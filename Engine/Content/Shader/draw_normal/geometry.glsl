#version 150
layout(triangles) in;

// Three lines will be generated: 6 vertices
layout(line_strip, max_vertices=6) out;

uniform mat4 modelview;

in vec3 g_normal[];

void main()
{
    int i;
    for(i=0; i<gl_in.length(); i++)
    {
        vec3 P = gl_in[i].gl_Position.xyz;
        vec3 N = g_normal[i];
        
        gl_Position = modelview * vec4(P, 1.0);
        EmitVertex();
        
        gl_Position = modelview * vec4(P + N*0.5, 1.0);
        EmitVertex();
        
        EndPrimitive();
    }
}
