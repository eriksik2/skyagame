#version 330

in vec3 vPosition;
in vec3 vNormal;

out vec3 g_normal;

void
main()
{
    gl_Position = vec4(vPosition, 1.0);
    g_normal = vNormal;
}
