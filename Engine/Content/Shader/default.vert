#version 330

in vec3 vPosition;
in vec3 vNormal;
in vec2 texcoord;
in vec3 vColor;

out vec2 f_texcoord;
out vec4 f_color;

uniform mat4 modelview;

void
main()
{
    gl_Position = modelview * vec4(vPosition, 1.0);
    f_texcoord = texcoord;
    f_color = vec4(vColor, 1.0);
}