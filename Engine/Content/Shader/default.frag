#version 330

in vec2 f_texcoord;
in vec4 f_color;
out vec4 outputColor;
uniform sampler2D maintexture;
void
main()
{
    vec2 flipped_texcoord = vec2(1.0 - f_texcoord.x, f_texcoord.y);
    //outputColor = f_color;
    outputColor = texture(maintexture, flipped_texcoord);
}
