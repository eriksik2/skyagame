#pragma once

#include <skya/ComponentManager.h>
#include <skya/GenerationalHeap.h>

namespace sk {

    class Game;

    class Entity {
        ComponentManager& m_manager;
    public:

        Entity(ComponentManager& manager) : m_manager(manager) {}

        ComponentManager& GetManager() { return m_manager; }
        const ComponentManager& GetManager() const { return m_manager; }
        Game& GetGame() { return m_manager.GetGame(); }
        const Game& GetGame() const { return m_manager.GetGame(); }

        template<class T>
        GenerationalHandle<T> Get() {
            return m_manager.template GetHeap<T>().find_if([=](const T& comp) {
                return &comp.GetEntity() == this;
            });
        }

        template<class T, class Pred>
        GenerationalHandle<T> GetComponentIf(Pred pred) {
            return m_manager.template GetHeap<T>().find_if([=](auto& comp) {
                return &comp.GetEntity() == this && pred(comp);
            });
        }

        template<class T>
        bool HasComponent() {
            return bool(Get<T>());
        }

        template<class T, class...Targs>
        GenerationalHandle<T> AddComponent(Targs&&...args) {
            return m_manager.template NewComponent<T>(this, std::forward<Targs>(args)...);
        }

        template<class T>
        void RemoveComponent(GenerationalIndex<T> index) {
            if(auto comp = m_manager.template GetHeap<T>()[index]) {
                if(&comp->GetEntity() == this) m_manager.RemoveComponent(index);
            }
        }
    };

    class Component {
        friend class ComponentManager;
        Entity* m_entity;
    public:
        virtual ~Component() {}

        Entity& GetEntity() { return *m_entity; }
        const Entity& GetEntity() const { return *m_entity; }
        Game& GetGame() { return m_entity->GetGame(); }
        const Game& GetGame() const { return m_entity->GetGame(); }
        ComponentManager& GetManager() { return m_entity->GetManager(); }
        const ComponentManager& GetManager() const { return m_entity->GetManager(); }

        template<class T>
        GenerationalHandle<T> Get() {
            return m_entity->template Get<T>();
        }

        template<class T, class Pred>
        GenerationalHandle<T> GetComponentIf(Pred pred) {
            return m_entity->template GetComponentIf<T>(pred);
        }

        template<class T>
        bool HasComponent() {
            return bool(Get<T>());
        }

        virtual void OnLoad() {}
        virtual void OnStart() {}
        virtual void OnUpdate(double) {}
        virtual void OnRender() {}
        virtual void OnEnd() {}
    };

}
