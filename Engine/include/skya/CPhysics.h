#pragma once
#include <skya/Component.h>
#include <glm/vec3.hpp>
namespace sk {
    class CPhysics : public Component {
    public:
        CPhysics();

        void OnUpdate(double dt);

        glm::vec3 GetVelocity() const;
        glm::vec3 GetAcceleration() const;

        void AddVelocity(glm::vec3 vel);
        void AddAcceleration(glm::vec3 accel);
        void ResetAcceleration();
        void AddForce(glm::vec3 force);

        float Mass = 0;
        glm::vec3 Drag = glm::vec3(0);
    private:
        glm::vec3 m_Velocity = glm::vec3(0);
        glm::vec3 m_Acceleration = glm::vec3(0);
    };
}

