#pragma once
#include <skya/Component.h>
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
namespace sk {
        class CTransform : public Component {
        public:

            static glm::vec3 WorldForward;
            static glm::vec3 WorldUp;
            static glm::vec3 WorldLeft;

            const glm::vec3& GetPosition() const;
            glm::vec3& GetPosition();

            const glm::vec3& GetScale() const;
            glm::vec3& GetScale();

            const glm::tquat<float>& GetRotation() const;
            glm::tquat<float>& GetRotation();

            const glm::mat4& GetMatrix();
            const glm::mat4& GetViewMatrix();

            glm::vec3 EulerRotation() const;
            glm::vec3 Forward() const;
            glm::vec3 Up() const;
            glm::vec3 Left() const;

        private:
            glm::vec3 m_Position = glm::vec3(0);
            glm::vec3 m_Scale = glm::vec3(1);
            glm::tquat<float> m_Rotation;
            glm::mat4 m_Matrix = glm::mat4(0);
            bool b_MatrixNeedUpdate = true;
            glm::mat4 m_ViewMatrix = glm::mat4(0);
            bool b_ViewMatrixNeedUpdate = true;
        };
}

