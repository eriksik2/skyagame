#pragma once

#include <stdlib.h>

#include <EASTL/vector.h>
#include <EASTL/functional.h>

#include <steam/steam_api.h>

#include <skya/Window.h>
#include <skya/GenerationalHeap.h>

namespace sk {
    class EntityBase;
    class CCamera;
    class Game {

        float time_left_over = 0;
    protected:
        Game();
    public:
        bool started = false;
        bool loaded = false;

        sk::GenerationalHandle<sk::CCamera> default_camera = nullptr;

        virtual ~Game() {}

        void Start();

        virtual void PreLoad() { }
        virtual void PostLoad() {}

        virtual void PreStart() {}
        virtual void PostStart() {}

        virtual void PreUpdate(double dt) {}
        virtual void PostUpdate(double dt) {}

        virtual void PreRender() {}
        virtual void PostRender() {}

        virtual void PreEnd() {}
        virtual void PostEnd() {}

        eastl::vector<eastl::function<void()>> OnLoad;
        eastl::vector<eastl::function<void()>> OnStart;
        eastl::vector<eastl::function<void(double)>> OnUpdate;
        eastl::vector<eastl::function<void()>> OnRender;
        eastl::vector<eastl::function<void()>> OnEnd;

        const float timestep = 1/60.f;

        Window window;

        template<class TDerived = Game>
        static TDerived Init(int appid) {
            if(SteamAPI_RestartAppIfNecessary(appid)) {
                printf("Launching through Steam...\n");
                exit(0);
            }
            if(!SteamAPI_Init()) {
                printf("\nFailed to initialize Steam API.\n");
            }
            return TDerived();
        }
    };

}
