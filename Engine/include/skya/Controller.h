#pragma once
#include <EASTL/vector.h>
#include <EASTL/span.h>

#include <steam/steam_api.h>

namespace sk {

    struct ControllerData {
        ControllerData(const char* n, uint64 h)
            : name(n), handle(h) {}
        const char* const name;
        const uint64 handle;
    };

    struct ControllerOriginData {
        ControllerOriginData() {}
        ControllerOriginData(EControllerActionOrigin ty);
        EControllerActionOrigin type;
        const char* text;
        const char* glyph;
    };

    class Controller {

        static class ControllerList {
            friend class Controller;
            int m_size = 0;
            uint64 m_array[STEAM_CONTROLLER_MAX_COUNT];
        public:
            int size() const { return m_size; }
            const uint64* begin() const { return m_array; }
            const uint64* end() const { return m_array + m_size; }
        } connected_controllers;

        static bool api_initialized;

        static eastl::vector<ControllerData> action_sets;
        static eastl::vector<ControllerData> digital_actions;
        static eastl::vector<ControllerData> analog_actions;

        static int GetDataIndex(eastl::span<const ControllerData> arr, const char* name);
        static int GetDataIndex(eastl::span<const ControllerData> arr, uint64 handle);
    public:
        static void Init();
        static void ForceSync();
        static uint64 GetActionSet(const char* name);
        static uint64 GetDigitalAction(const char* name);
        static uint64 GetAnalogAction(const char* name);

        static const ControllerList& GetConnectedControllers();

        Controller() {}
        Controller(uint64 controller_handle);

        uint64 GetDevice() const;
        void SetDevice(uint64 controller_handle);
        bool HasDevice() const;

        void ActivateActionSet(uint64 set_handle);
        void ActivateActionSet(const char* name);
        ControllerData GetCurrentActionSet() const;

        ControllerAnalogActionData_t GetAnalogActionData(uint64 action_handle) const;
        ControllerAnalogActionData_t GetAnalogActionData(const char* name) const;
        eastl::vector<ControllerOriginData> GetAnalogActionOrigins(uint64 action_handle) const;
        eastl::vector<ControllerOriginData> GetAnalogActionOrigins(const char* name) const;

        ControllerDigitalActionData_t GetDigitalActionData(uint64 action_handle) const;
        ControllerDigitalActionData_t GetDigitalActionData(const char* name) const;
        eastl::vector<ControllerOriginData> GetDigitalActionOrigins(uint64 action_handle) const;
        eastl::vector<ControllerOriginData> GetDigitalActionOrigins(const char* name) const;

        ControllerMotionData_t GetMotionData() const;

        void StopAnalogActionMomentum(uint64 action_handle);
        void StopAnalogActionMomentum(const char* name);

        void Pulse(ESteamControllerPad pad, unsigned short duration);
        void RepeatedPulse(ESteamControllerPad pad, unsigned short ontime, unsigned short offtime, unsigned short repeat);
        void Vibration(unsigned short speed_left, unsigned short speed_right);

        void SetLED(uint8 r, uint8 g, uint8 b);
        void ResetLED();

        bool ShowBindingPanel() const;

    private:
        int current_action_set_index = 0;
        ControllerHandle_t handle = 0;
    };
}
