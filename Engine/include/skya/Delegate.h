#pragma once
#include <EASTL/vector.h>


namespace sk {
    template<typename ReturnType, typename...ArgsType>
    class Delegate {
        using FuncType = ReturnType(*)(void*, ArgsType&&...);
        constexpr Delegate(FuncType func = nullptr, void* inst = nullptr)
            : m_Method(func), m_Class(inst) {}
    public:

        template<class...TArgsRef>
        void Invoke(TArgsRef&&...args) {
            (*m_Method)(m_Class, std::forward<ArgsType&&>(args)...);
        }

        template<class T, ReturnType(T::*Method)(ArgsType...)>
        constexpr static Delegate From(const T* owner) {
            return Delegate(MemberWrapper<T, Method>, (void*)owner);
        }

        template<ReturnType(*Method)(ArgsType...)>
        constexpr static Delegate From() {
            return Delegate(FreeWrapper<Method>);
        }

        template<class...TArgsRef>
        inline void operator ()(TArgsRef&&...args) { Invoke(std::forward<TArgsRef>(args)...); }

        inline bool operator== (const Delegate& other) const {
            return m_Class == other.m_Class && m_Method == other.m_Method;
        }

    private:
        FuncType m_Method;
        void* m_Class;


        template<class T, ReturnType(T::*Method)(ArgsType...), class...TArgsRef>
        static ReturnType MemberWrapper(void* owner, TArgsRef&&...args) {
            if(!owner) return;
            T* cast_owner = static_cast<T*>(owner);
            return (cast_owner->*Method)(std::forward<TArgsRef>(args)...);
        }

        template<ReturnType(*Method)(ArgsType...), class...TArgsRef>
        static ReturnType FreeWrapper(void* owner, TArgsRef&&...args) {
            return (*Method)(std::forward<TArgsRef>(args)...);
        }
    };

    template<class _T> struct make_delegate;
    template<class _Tr, class _Tc, class..._Tar>
    struct make_delegate<_Tr(_Tc::*)(_Tar...)> {
        template<_Tr(_Tc::*M)(_Tar...)>
        static auto from(_Tc* obj) {
            return Delegate<_Tr, _Tar...>::template From<_Tc, M>(obj);
        }
    };
#define DELEGATE(mem_fn_ptr, instance_ptr) make_delegate<decltype(mem_fn_ptr)>::template from<mem_fn_ptr>(instance_ptr)

    template<typename...ArgsType>
    using Action = Delegate<void, ArgsType...>;

    template<typename...ArgsType>
    class Event {
    public:
        using delegate_t = Delegate<void, ArgsType...>;

        template<class...TArgsRef>
        inline void operator()(TArgsRef&&...args) {
            Invoke(std::forward<TArgsRef>(args)...);
        }
        template<class...TArgsRef>
        void Invoke(TArgsRef&&...args) {
            for(auto& f : c_Handler) f.Invoke(std::forward<TArgsRef>(args)...);
        }

        Event& operator += (delegate_t func) {
            c_Handler.push_back(func);
            return *this;
        }
        Event& operator -= (delegate_t& func) {
            for(auto it = c_Handler.begin(), end = c_Handler.end(); it != end; ++it)
                if(*it == func) {
                    c_Handler.erase(it);
                    break;
                }
            return *this;
        }

    private:
        eastl::vector<delegate_t> c_Handler;
    };
}
