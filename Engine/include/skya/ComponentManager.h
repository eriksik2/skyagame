#pragma once

#include <skya/Game.h>

namespace sk {

    class Entity;

    class ComponentManager {
        Game& m_game;

    public:
        ComponentManager(Game& game) : m_game(game) {}

        Game& GetGame() { return m_game; }
        const Game& GetGame() const { return m_game; }

        template<class T>
        GenerationalHeap<T>& GetHeap() {
            static GenerationalHeap<T> heap(16);
            static int once = [=]() {
                m_game.OnLoad.push_back([=]() {
                    for(auto& comp : heap) comp.OnLoad();
                });
                m_game.OnStart.push_back([=]() {
                    for(auto& comp : heap) comp.OnStart();
                });
                m_game.OnUpdate.push_back([=](double ts) {
                    for(auto& comp : heap) comp.OnUpdate(ts);
                });
                m_game.OnRender.push_back([=]() {
                    for(auto& comp : heap) comp.OnRender();
                });
                m_game.OnEnd.push_back([=]() {
                    for(auto& comp : heap) comp.OnEnd();
                });
                return 0;
            }();
            return heap;
        }

        template<class T, class...Targs>
        GenerationalHandle<T> NewComponent(Entity* entity, Targs&&...args) {
            auto& heap = GetHeap<T>();
            auto ind = GetHeap<T>().emplace(std::forward<Targs>(args)...);
            T& item = *GetHeap<T>()[ind.index()];
            item.m_entity = entity;
            if(m_game.loaded) item.OnLoad();
            if(m_game.started) item.OnStart();
            return ind;
        }

        template<class T>
        void RemoveComponent(GenerationalIndex<T> index) {
            auto& heap = GetHeap<T>();
            T* item = heap[index];
            item->OnEnd();
            GetHeap<T>().erase(index);
        }
    };
}