#pragma once
#include <vector>
#include <ctime>
#include <type_traits>

#include <filesystem>
namespace fs = std::filesystem;

namespace sk {

    template<class T>
    class Loadable;

    template<class T>
    struct ResourceView {
        ResourceView(fs::path p, std::time_t t, T* d) : Path(p), Time(t), Data(d) {}
        fs::path    Path;
        std::time_t Time;
        const T*    Data;
    };
    template<class T>
    struct Resource {
        Resource(const Resource&) = delete;
        Resource(fs::path p, T* d) : Path(p), Time(std::time(0)), Data(d) {}
        Resource(Resource&& from) : Path(from.Path), Time(from.Time), Data(from.Data) {
            from.Data = nullptr;
        }
        ~Resource() { delete Data; }
        operator ResourceView<T>() const { return ResourceView<T>(Path, Time, Data); }
        fs::path    Path;
        std::time_t Time;
        T*          Data;
    };

    template<class T>
    class ResourceLoader {
        using errmethod_t = T*(*)(void);
        using defaultmethod_t = T*(*)(const char*);
        errmethod_t     ErrorMethod;
        defaultmethod_t DefaultMethod;
    public:
        ResourceLoader(defaultmethod_t def, errmethod_t err)
            : ErrorMethod(err)
            , DefaultMethod(def) {}

        /* Loads T using Method if not loaded already, then returns it */
        template<class Tm, Tm Method, typename ...A>
        T* Load(const char * path, A...args);

        /* Adds an already loaded T* to ResourceLoader loaded T's */
        T* Register(T* loaded, const char * path);

        /* Returns T if loaded, otherwise returns nullptr */
        T* Get(const char * path);

        T* LoadError();

        std::vector<Resource<T>> Registry;
        T* Error = nullptr;
    };


    template<class T>
    template<class Tm, Tm Method, typename...A>
    T* ResourceLoader<T>::Load(const char * path, A...args) {
        T* out = Get(path);
        if(out) return out;

        auto loaded = Method(path, std::forward<A>(args)...);

        return Register(loaded, path);
    }

    template<class T>
    T* ResourceLoader<T>::Register(T* loaded, const char * path) {
        T* out = Get(path);
        if(out) return out;

        auto fspath = fs::path(path).lexically_normal();
        if(loaded == nullptr) {
            printf("Failed to load: %s\n", fspath.string().c_str());
            return LoadError();
        }

        Registry.push_back(Resource<T>(fspath, loaded));

        printf("Loaded: %s\n", fspath.string().c_str());
        return Registry.back().Data;
    }

    template<class T>
    T* ResourceLoader<T>::Get(const char * path) {
        auto p = fs::path(path).lexically_normal();
        for(auto it = Registry.begin(); it != Registry.end(); ++it)
            if(it->Path == p) return it->Data;
        return nullptr;
    }

    template<class T>
    T* ResourceLoader<T>::LoadError() {
        if(!Error) {
            Error = (ErrorMethod)();
            if(Error == nullptr) {
                printf("Failed to load error file!!");
                return nullptr;
            }
        }
        return Error;
    }


    template<class Tobj>
    class Content {
        Content() = delete;
        Content(const Content&) = delete;
        Content(Content&&) = delete;

        static ResourceLoader<Tobj>* GetLoaderPointer() {
            static auto loader = new ResourceLoader<Tobj>(Default, Error);
            return loader;
        }
    public:
        static ResourceLoader<Tobj>& GetLoader() {
            return *GetLoaderPointer();
        }

        // remember to have a definition for these on all template instances
        using DefaultMethod = Tobj*(*)(const char*);
        using ErrorMethod = Tobj*(*)();
        static DefaultMethod Default;
        static ErrorMethod Error;

        /* Loads T using Method if not loaded already, then returns it */
        template<class Tm, Tm Method, typename...A>
        static Tobj* Load(A...args) {
            return GetLoader().template Load<Tm, Method>(std::forward<A>(args)...);
        }

        // for use in template parameter
#define SK_TYPE(value) decltype(value), value

        /* Loads T using default loading method if not loaded already, then returns it */
        static Tobj* Load(const char* path) {
            auto* obj = Default(path);
            return GetLoader().Register(obj, path);
            //return loader.template Load<decltype(&Tobj::FromFile), &Tobj::FromFile>(path);
        }

        template<class...Targs>
        static Tobj* Load(Tobj*(*method)(const char*, Targs...), const char* path, Targs...args) {
            auto& loader = GetLoader();
            return loader.template Register(method(path, std::forward<Targs>(args)...), path);
        }

        /* Returns std::vector of all loaded resources at time of call */
        static std::vector<ResourceView<Tobj>> ViewAll() {
            auto& reg = GetLoader().Registry;
            std::vector<ResourceView<Tobj>> view;
            view.reserve(reg.size());
            view.insert(view.end(), reg.begin(), reg.end());
            return view;
        }

        static ResourceView<Tobj> View(Tobj* obj) {
            for(auto& r : GetLoader().Registry) if(r.Data == obj)
                return ResourceView<Tobj>(r.Path, r.Time, r.Data);
            return ResourceView<Tobj>(fs::path(), std::time_t(0), nullptr);
        }

        /* Returns Tobj if loaded at path, otherwise returns nullptr */
        static Tobj* Get(const char* path) {
            return GetLoader().Get(path);
        }
    };
}
