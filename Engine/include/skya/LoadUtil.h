#pragma once
#include <skya/ResourceLoader.h>
#include <skya/grafx/Base.h>

namespace sk {
    sk::grafxbase::Texture2DHandle load_texture2d(eastl::string_view path);

    eastl::string read_file(eastl::string_view path);
    sk::grafxbase::ShaderHandle load_shader(eastl::string_view path, sk::grafxbase::ShaderType type);
    sk::grafxbase::ProgramHandle load_shader_program(eastl::string_view vertexpath, eastl::string_view fragmentpath);
}
