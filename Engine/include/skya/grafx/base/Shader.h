#pragma once
#include "Object.h"

#include <EASTL/string.h>

namespace sk::grafxbase {

    struct ShaderHandle : GrafxObject {};

    enum ShaderDataType {
        ShaderDataType_None = 0,
        ShaderDataType_Float, ShaderDataType_Float2, ShaderDataType_Float3, ShaderDataType_Float4,
        ShaderDataType_Float33, ShaderDataType_Float44,
        ShaderDataType_Int, ShaderDataType_Int2, ShaderDataType_Int3, ShaderDataType_Int4
    };

    enum ShaderType {
        ShaderType_Vertex,
        ShaderType_Fragment
    };
    
    void* get_backend_handle(ShaderHandle);
    
    ShaderHandle create_shader(ShaderType type);
    void delete_shader(ShaderHandle shader);

    void source_shader(ShaderHandle shader, eastl::string_view source);

    bool compile_shader(ShaderHandle shader, eastl::string* error_log = nullptr);
}