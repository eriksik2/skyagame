#pragma once
#include "Object.h"

#include <skya/grafx/base/Texture.h>

namespace sk::grafxbase {

    struct FramebufferHandle : GrafxObject {};
    extern FramebufferHandle DefaultFramebuffer;

    enum BlendFactor {
        BlendFactor_Zero,
        BlendFactor_One,
        BlendFactor_NewAlpha,
        BlendFactor_NewAlphaInverted
    };
    
    void* get_backend_handle(FramebufferHandle);
    
    FramebufferHandle create_framebuffer();
    void delete_framebuffer(FramebufferHandle fb);

    void bind_framebuffer_texture(FramebufferHandle fb, int location, Texture2DHandle tex, int mipmap_level = 0);
    void bind_framebuffer_depth_texture(FramebufferHandle fb, Texture2DHandle tex, int mipmap_level = 0);

    void set_framebuffer_blend(FramebufferHandle fb, BlendFactor newfactor, BlendFactor oldfactor);

    // clear specified output slots. slots has to be sorted.
    void clear_framebuffer(FramebufferHandle fb, glm::vec4 values, eastl::span<int> slots);
    // clear bound output slots
    void clear_framebuffer(FramebufferHandle fb, glm::vec4 values);// TODO : add set_framebuffer_clear_color _depth _stencil as framebuffer state instead of arguments 
    void clear_framebuffer_depth(FramebufferHandle fb, float values = 1.0f);
    void clear_framebuffer_stencil(FramebufferHandle fb, int values = 0);
}