#pragma once
#include "Object.h"

#include <skya/grafx/base/Framebuffer.h>
#include <skya/grafx/base/VertexArray.h>
#include <skya/grafx/base/Texture.h>
#include <skya/grafx/base/Shader.h>

#include <EASTL/string.h>
#include <EASTL/span.h>
#include <glm/glm.hpp>

namespace sk::grafxbase {

    struct ProgramHandle : GrafxObject {};

    /// ShaderData{type, name, location, count}
    /// type is the type of the uniform/attribute in the shader script.
    /// name is the name of the uniform/attribute in the shader script.
    /// location is the location to use when interfacing with the uniform/attribute be it with set_uniform_* or a vertex array.
    /// count is the array length of the uniform/attribute. count = 1 if it's not an array.
    struct ShaderData {
        ShaderDataType type;
        eastl::string name;
        int location;
        int count;
    };

    void* get_backend_handle(ProgramHandle);

    ProgramHandle create_program();
    void delete_program(ProgramHandle program);

    void bind_program_shader(ProgramHandle program, ShaderHandle shader);
    void unbind_program_shader(ProgramHandle program, ShaderHandle shader);

    /// link_program(prog, error)
    /// Returns false if linking failed. if error points to a string the string is filled with an implementation specific error message.
    /// Returns true otherwise.
    bool link_program(ProgramHandle program, eastl::string* error_out = nullptr);

    int program_attribute_count(ProgramHandle program);
    int program_uniform_count(ProgramHandle program);
    int program_shader_count(ProgramHandle program);

    /// program_*_info(program, index)
    /// Get info on uniform/attribute # index. 
    /// program has to have been created by create_program().
    /// index has to be in the range [0, program_*_count).
    /// Note that index doesn't necessarily correspond to the actual location of the uniform/attribute.
    ShaderData program_uniform_info(ProgramHandle program, int index);
    ShaderData program_attribute_info(ProgramHandle program, int index);

    void set_uniform_float1(ProgramHandle program, int location, float value);
    void set_uniform_float1v(ProgramHandle program, int location, eastl::span<float>);
    void set_uniform_float2(ProgramHandle program, int location, glm::vec2 value);
    void set_uniform_float2v(ProgramHandle program, int location, eastl::span<glm::vec2>);
    void set_uniform_float3(ProgramHandle program, int location, glm::vec3 value);
    void set_uniform_float3v(ProgramHandle program, int location, eastl::span<glm::vec3>);
    void set_uniform_float4(ProgramHandle program, int location, glm::vec4 value);
    void set_uniform_float4v(ProgramHandle program, int location, eastl::span<glm::vec4>);
    void set_uniform_float33(ProgramHandle program, int location, glm::mat3 value);
    void set_uniform_float33v(ProgramHandle program, int location, eastl::span<glm::mat3>);
    void set_uniform_float44(ProgramHandle program, int location, glm::mat4 value);
    void set_uniform_float44v(ProgramHandle program, int location, eastl::span<glm::mat4>);
    void set_uniform_int1(ProgramHandle program, int location, int value);
    void set_uniform_int1v(ProgramHandle program, int location, eastl::span<int>);
    void set_uniform_int2(ProgramHandle program, int location, glm::ivec2 value);
    void set_uniform_int2v(ProgramHandle program, int location, eastl::span<glm::ivec2>);
    void set_uniform_int3(ProgramHandle program, int location, glm::ivec3 value);
    void set_uniform_int3v(ProgramHandle program, int location, eastl::span<glm::ivec3>);
    void set_uniform_int4(ProgramHandle program, int location, glm::ivec4 value);
    void set_uniform_int4v(ProgramHandle program, int location, eastl::span<glm::ivec4>);

    /// set_uniform_texture(prog, loc, tex)
    /// sampler will use a specific texture tex.
    void set_uniform_texture(ProgramHandle program, int location, TextureHandle tex);

    /// set_uniform_sampler(prog, loc, unit)
    /// sampler will use the texture bound to unit at the time of rendering.
    void set_uniform_sampler(ProgramHandle program, int location, int unit);

    enum FaceCullingMode {
        FaceCullingMode_None,
        FaceCullingMode_CW,
        FaceCullingMode_CCW,
    };

    struct DrawCommandSettings {
        FaceCullingMode culling = FaceCullingMode_None;
        int first_index = 0;
        int count = 0;
    };

    void draw_triangles(VertexArrayHandle in, ProgramHandle program, FramebufferHandle out = DefaultFramebuffer, DrawCommandSettings settings = DrawCommandSettings());
}