#pragma once
#include "Object.h"

#include <EASTL/vector.h>
#include <EASTL/span.h>

namespace sk::grafxbase {

    struct RawBufferHandle : GrafxObject {};
    template<class T>
    struct BufferHandle : RawBufferHandle {};

    using BufferUsageHintFlag = int;
    enum BufferUsageHintFlag_ : BufferUsageHintFlag {
        BufferUsageHint_None = 0,              // Buffer will be used mainly only by the gpu and not read from or written to frequently.
        BufferUsageHint_ClientWrite = 1<<0,    // Buffer will be modified mainly by the cpu. As opposed to being modified mainly by the gpu.
        BufferUsageHint_ClientRead = 1<<1,     // Buffer will be read mainly by the cpu. As opposed to being read mainly by the gpu.
        BufferUsageHint_DynamicWrite = 1<<2,   // Buffer data will change frequently. As opposed to being modified only once or a few times.
        BufferUsageHint_DynamicRead = 1<<3,    // Buffer data will be read frequently. As opposed to being read only once or a few times.
        BufferUsageHint_Default = BufferUsageHint_ClientWrite | BufferUsageHint_DynamicRead
    };

    using BufferMapAccessFlag = int;
    enum BufferMapAccessFlag_ : BufferMapAccessFlag {
        BufferMapAccess_Read = 1<<0,
        BufferMapAccess_Write = 1<<1,
        BufferMapAccess_Default = BufferMapAccess_Read | BufferMapAccess_Write
    };

    void* get_backend_handle(RawBufferHandle);

    RawBufferHandle create_buffer_raw();
    void delete_buffer(RawBufferHandle buffer);

    int buffer_bytes(RawBufferHandle buffer);
    
    void allocate_buffer_raw(RawBufferHandle buffer, int byte_count, const void* data, BufferUsageHintFlag usage = BufferUsageHint_Default);

    void modify_buffer_raw(RawBufferHandle buffer, int byte_offset, int byte_count, const void* data);

    void copy_buffer_raw(RawBufferHandle from, RawBufferHandle to, int byte_offset_from, int byte_offset_to, int byte_count);

    void read_buffer_raw(RawBufferHandle buffer, int byte_offset, int byte_count, void* dest);

    void* map_buffer_raw(RawBufferHandle buffer, int byte_offset, int byte_count, BufferMapAccessFlag flags = BufferMapAccess_Default);
    bool unmap_buffer(RawBufferHandle buffer);

    template<class T>
    BufferHandle<T> create_buffer()
    { return {{create_buffer_raw()}}; }

    template<class T>
    int buffer_size(BufferHandle<T> buffer)
    { return buffer_bytes(buffer)/sizeof(T); }

    template<class T>
    void allocate_buffer(BufferHandle<T> buffer, eastl::span<const T> data, BufferUsageHintFlag usage = BufferUsageHint_Default)
    { allocate_buffer_raw(buffer, sizeof(T)*data.size(), data.data(), usage); }

    template<class T>
    void allocate_buffer(BufferHandle<T> buffer, eastl::span<T> data, BufferUsageHintFlag usage = BufferUsageHint_Default)
    { allocate_buffer(buffer, data, usage); }

    template<class T>
    void modify_buffer(BufferHandle<T> buffer, int offset, eastl::span<const T> data)
    { modify_buffer_raw(buffer, sizeof(T)*offset, sizeof(T)*data.size(), data.data()); }

    template<class T>
    void modify_buffer(BufferHandle<T> buffer, int offset, eastl::span<T> data)
    { modify_buffer(buffer, offset, data); }

    template<class T>
    void copy_buffer(BufferHandle<T> from, BufferHandle<T> to, int offset_from, int offset_to, int count)
    { copy_buffer_raw(from, to, sizeof(T)*offset_from, sizeof(T)*offset_to, sizeof(T)*count); }

    template<class T>
    eastl::vector<T> read_buffer(BufferHandle<T> buffer, int offset, int count){
        eastl::vector<T> out;
        out.set_capacity(count);
        read_buffer_raw(buffer, sizeof(T)*offset, sizeof(T)*count, out.data());
        return out;
    }

    template<class T>
    eastl::span<T> map_buffer(BufferHandle<T> buffer, int offset, int count, BufferMapAccessFlag flags = BufferMapAccess_Default){
        T* begin = (T*)map_buffer_raw(buffer, sizeof(T)*offset, sizeof(T)*count, flags);
        return eastl::span<T>(begin, begin+count);
    }

}