#pragma once
#include "Object.h"

#include <EASTL/string_view.h>
#include <EASTL/span.h>
#include <glm/glm.hpp>

namespace sk::grafxbase {

    struct TextureHandle : GrafxObject {};
    struct Texture1DHandle : TextureHandle {};
    struct Texture2DHandle : TextureHandle {};
    struct Texture3DHandle : TextureHandle {};
    struct TextureCubemapHandle : TextureHandle {};

    enum TextureScaleType {
        TextureScaleType_Nearest,
        TextureScaleType_Linear
    };

    enum TextureCubemapSide {
        TextureCubemapSide_PositiveX,
        TextureCubemapSide_NegativeX,
        TextureCubemapSide_PositiveY,
        TextureCubemapSide_NegativeY,
        TextureCubemapSide_PositiveZ,
        TextureCubemapSide_NegativeZ,
    };

    enum TextureFormatType {
        TextureFormatType_Float,    // supports bitdepth 16, 32
        TextureFormatType_Signed,   // supports bitdepth 8, 16, 32
        TextureFormatType_Unsigned, // supports bitdepth 8, 16, 32
        TextureFormatType_Depth,
    };

    enum Bitdepth {
        Bitdepth_8,
        Bitdepth_16,
        Bitdepth_32,
    };

    struct TextureFormat {
        TextureFormatType type = TextureFormatType_Float;
        int components = 3; //[1, 4]
        Bitdepth bitdepth = Bitdepth_32;
    };

    extern const TextureFormat TextureFormat_Depth;
    
    extern const TextureFormat TextureFormat_RG32;
    extern const TextureFormat TextureFormat_RGB32;
    extern const TextureFormat TextureFormat_RGBA32;
    extern const TextureFormat TextureFormat_RG16;
    extern const TextureFormat TextureFormat_RGB16;
    extern const TextureFormat TextureFormat_RGBA16;
    extern const TextureFormat TextureFormat_RG8;
    extern const TextureFormat TextureFormat_RGB8;
    extern const TextureFormat TextureFormat_RGBA8;

    void* get_backend_handle(TextureHandle);

    Texture2DHandle load_texture_2D(eastl::string_view path);

    Texture1DHandle create_texture_1D();
    Texture2DHandle create_texture_2D();
    Texture3DHandle create_texture_3D();
    TextureCubemapHandle create_texture_cubemap();
    void delete_texture(TextureHandle tex);

    void texture_unit(TextureHandle tex, int unit);

    void set_texture_mag_filter(TextureHandle tex, TextureScaleType type);
    void set_texture_min_filter(TextureHandle tex, TextureScaleType type);
    void set_texture_minmag_filter(TextureHandle tex, TextureScaleType type);

    void build_texture_mipmaps(TextureHandle tex);

    void allocate_texture(Texture1DHandle tex, TextureFormat internal_format, int width, int mipmap_levels = 0);
    //void modify_texture_raw(Texture1DHandle tex, int level, int x, int width, TextureChannel channel, void* data);
    void modify_texture(Texture1DHandle tex, int level, int x, int width, eastl::span<int> data);
    void modify_texture(Texture1DHandle tex, int level, int x, int width, eastl::span<glm::ivec3> data);
    void modify_texture(Texture1DHandle tex, int level, int x, int width, eastl::span<glm::ivec4> data);

    void allocate_texture(Texture2DHandle tex, TextureFormat internal_format, glm::ivec2 size, int mipmap_levels = 0);
    //void modify_texture_raw(Texture2DHandle tex, int level, glm::ivec2 pos, glm::ivec2 size, TextureChannel channel, void* data);
    void modify_texture(Texture2DHandle tex, int level, glm::ivec2 pos, glm::ivec2 size, eastl::span<int> data);
    void modify_texture(Texture2DHandle tex, int level, glm::ivec2 pos, glm::ivec2 size, eastl::span<glm::ivec3> data);
    void modify_texture(Texture2DHandle tex, int level, glm::ivec2 pos, glm::ivec2 size, eastl::span<glm::ivec4> data);

    void allocate_texture(Texture3DHandle tex, TextureFormat internal_format, glm::ivec3 size, int mipmap_levels = 0);
    //void modify_texture_raw(Texture3DHandle tex, int level, glm::ivec3 pos, glm::ivec3 size, TextureChannel channel, void* data);
    void modify_texture(Texture3DHandle tex, int level, glm::ivec3 pos, glm::ivec3 size, eastl::span<int> data);
    void modify_texture(Texture3DHandle tex, int level, glm::ivec3 pos, glm::ivec3 size, eastl::span<glm::ivec3> data);
    void modify_texture(Texture3DHandle tex, int level, glm::ivec3 pos, glm::ivec3 size, eastl::span<glm::ivec4> data);

    void allocate_texture(TextureCubemapHandle tex, TextureFormat internal_format, int size, int mipmap_levels = 0);
    //void modify_texture_raw(TextureCubemapHandle tex, TextureCubemapSide side, int level, glm::ivec2 pos, glm::ivec2 size, TextureChannel channel, void* data);
    void modify_texture(TextureCubemapHandle tex, TextureCubemapSide side, int level, glm::ivec2 pos, glm::ivec2 size, eastl::span<int> data);
    void modify_texture(TextureCubemapHandle tex, TextureCubemapSide side, int level, glm::ivec2 pos, glm::ivec2 size, eastl::span<glm::ivec3> data);
    void modify_texture(TextureCubemapHandle tex, TextureCubemapSide side, int level, glm::ivec2 pos, glm::ivec2 size, eastl::span<glm::ivec4> data);

}