#pragma once
#include "Object.h"

#include <skya/grafx/base/Shader.h>
#include <skya/grafx/base/Buffer.h>

namespace sk::grafxbase {

    struct VertexArrayHandle : GrafxObject {};

    struct VertexAttributeFormat {
        ShaderDataType type;
        int byte_stride;
        int byte_offset;
    };

    void* get_backend_handle(VertexArrayHandle);

    VertexArrayHandle create_vertex_array();
    void delete_vertex_array(VertexArrayHandle array);

    void enable_vertex_attribute(VertexArrayHandle array, int location);
    void disable_vertex_attribute(VertexArrayHandle array, int location);

    void bind_vertex_attribute_buffer(VertexArrayHandle array, int location, RawBufferHandle buffer, VertexAttributeFormat format);

    void bind_vertex_index_buffer(VertexArrayHandle array, BufferHandle<unsigned int> buffer);
    void bind_vertex_index_buffer(VertexArrayHandle array, BufferHandle<int> buffer);

    int vertex_index_buffer_size(VertexArrayHandle array);
}