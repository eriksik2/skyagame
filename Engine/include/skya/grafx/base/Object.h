#pragma once
namespace sk::grafxbase {
    struct GrafxObject {
        void* internal_handle = nullptr;
        constexpr operator bool() const { return internal_handle; }
    };
}