#pragma once

#include <EASTL/shared_ptr.h>

namespace sk {
    template<class T>
    using handle = eastl::shared_ptr<T>;
}