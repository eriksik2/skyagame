#pragma once
#include <skya/ResourceLoader.h>
#include <skya/grafx/Base.h>

#include <EASTL/span.h>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

namespace sk {

    class MeshObject {
    public:
        struct Vertex {
            glm::vec3 Position = glm::vec3(0);
            glm::vec3 Normal = glm::vec3(0);
            glm::vec2 UV = glm::vec2(0);
            glm::vec3 Color = glm::vec3(0);
            Vertex() {}
            Vertex(glm::vec3 pos) : Position(pos) {}
            Vertex(float x, float y, float z) : Position(x, y, z) {}
        };

        int IndiceLength;

        MeshObject();
        MeshObject(eastl::span<const Vertex> verts, eastl::span<const int> indices);

        grafxbase::BufferHandle<Vertex> GetVertices();
        grafxbase::BufferHandle<int> GetIndices();

        void RewriteData(eastl::span<const Vertex> verts, eastl::span<const int> indices);

        void Draw(sk::grafxbase::ProgramHandle program, sk::grafxbase::FramebufferHandle fb);

        void PipePositionToAttribute(int attribute);
        void PipeNormalToAttribute(int attribute);
        void PipeUVToAttribute(int attribute);
        void PipeColorToAttribute(int attribute);

        static MeshObject* LoadError();
        static MeshObject* FromFile(const char* path);
        static MeshObject* GenPlane(const char*, float xlen = 1, float zlen = 1, int side = 3);

        grafxbase::VertexArrayHandle VAO;
        grafxbase::BufferHandle<int> Indices;
        grafxbase::BufferHandle<Vertex> Verts;
    };

}
