#pragma once

#include <skya/Enums.h>

#include <imgui.h>
#include <skya/imgui/Widgets.h>

#define IMGUI_IMPL_OPENGL_LOADER_GL3W
#include <skya/imgui/imgui_impl_opengl3.h>
#include <skya/imgui/imgui_impl_glfw.h>

namespace sk {
    class imgui {
    public:
        imgui(GLFWwindow* window, ImGuiConfigFlags = ImGuiConfigFlags_None/*= ImGuiConfigFlags_DockingEnable*/);
        ~imgui();

        bool is_mouse_captured() const;
        bool is_keyboard_captured() const;

        void start_frame() const;
        void render() const;
    };

}
