#pragma once

#include <imgui.h>

using ImGuiTimelineFlags = int;
enum ImGuiTimelineFlags_ {
    ImGuiTimelineFlags_None = 0,
    ImGuiTimelineFlags_NoHorizontalMove = 1<<0,
    ImGuiTimelineFlags_NoVerticalMove = 1<<1,
    ImGuiTimelineFlags_NoResize = 1<<2,
    ImGuiTimelineFlags_NoZoomWithMouse = 1<<3,
    ImGuiTimelineFlags_NoDragWithMouse = 1<<4,
    ImGuiTimelineFlags_AutoTracks = 1<<5, // internal
    ImGuiTimelineFlags_NoMove = ImGuiTimelineFlags_NoHorizontalMove | ImGuiTimelineFlags_NoVerticalMove,
};

namespace ImGui {
    /// Timeline
    void BeginTimeline(const char* label, ImVec2 size = {0, 0}, int tracks = 0, const char* indicator_format = "%.3f");
    void EndTimeline();
    void SetTimelineZoom(float start, float end, int ms = 0);

    void TimelineItem(const char* label, float* start, float* end, int* track, ImGuiTimelineFlags flags = ImGuiTimelineFlags_None);
    inline void TimelineItem(const char* label, float* start, float* end, float track = 0, ImGuiTimelineFlags flags = ImGuiTimelineFlags_None)
    { int t = track; TimelineItem(label, start, end, &t, flags | ImGuiTimelineFlags_NoHorizontalMove | ImGuiTimelineFlags_NoResize); }
    inline void TimelineItem(const char* label, float start, float end, float track = 0, ImGuiTimelineFlags flags = ImGuiTimelineFlags_None)
    { int t = track; TimelineItem(label, &start, &end, &t, flags | ImGuiTimelineFlags_NoMove | ImGuiTimelineFlags_NoResize); }
}