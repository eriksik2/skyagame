#pragma once
#include <skya/Component.h>
#include <skya/Physics.h>

#include <glm/vec3.hpp>
#include <vector>

namespace sk {
    namespace component {
        template<class Ent>
        class PhysHandler :
            public Component<PhysHandler, Ent> {
        public:
            void OnUpdate(double dt);

        private:
            //std::vector<std::weak_ptr<Physics>> c_Objects;
        };
    }
}
