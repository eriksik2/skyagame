#pragma once
#include <glm/glm.hpp>
namespace sk {
    struct Ray {
        Ray() {}
        Ray(glm::vec3 origin, glm::vec3 dir) :Origin(origin), Dir(dir) {}
        glm::vec3 Origin = glm::vec3(0);
        glm::vec3 Dir = glm::vec3(0);
        glm::vec3 Inverse();

        glm::vec3 Normal(glm::vec3 p) { return glm::vec3(0); }
    };
    struct AABB {
        AABB() {}
        AABB(glm::vec3 min, glm::vec3 max) :Min(min), Max(max) {
            for(int i = 0; i < 3; ++i)
                if(Max[i] < Min[i])
                {
                    auto tmp = Max[i];
                    Max[i] = Min[i];
                    Min[i] = tmp;
                }
        }
        glm::vec3 Min = glm::vec3(0);
        glm::vec3 Max = glm::vec3(0);

        glm::vec3 Normal(glm::vec3 p);
        glm::vec3 Perimeter(glm::vec3 p);
    };
    struct Sphere {
        Sphere() {}
        Sphere(glm::vec3 origin, float radius) :Origin(origin), Radius(radius) {}
        glm::vec3 Origin = glm::vec3(0);
        float Radius = 0;

        glm::vec3 Normal(glm::vec3 p);
        glm::vec3 Perimeter(glm::vec3 p);
    };
    struct Cylinder {
        Cylinder() {}
        Cylinder(glm::vec3 origin, float radius, float hheight) :Origin(origin), Radius(radius), HalfHeight(hheight) {}
        glm::vec3 Origin = glm::vec3(0);
        float Radius = 0;
        float HalfHeight = 0;

        glm::vec3 Normal(glm::vec3 p);
        glm::vec3 Perimeter(glm::vec3 p);
    };
    // math is wrong in FlatCylinder
    struct FlatCylinder {
        FlatCylinder() {}
        FlatCylinder(glm::vec3 origin, float radius, float hheight) :Origin(origin), Radius(radius), HalfHeight(hheight) {}
        glm::vec3 Origin = glm::vec3(0);
        float Radius = 0;
        float HalfHeight = 0;
        
        glm::vec3 Normal(glm::vec3 p);
        glm::vec3 Perimeter(glm::vec3 p);
    };

    /* Check if two shapes intersect */
    bool Intersection(Ray ray, AABB box);
    bool Intersection(AABB box1, AABB box2);
    bool Intersection(AABB box, Sphere sphere);
    bool Intersection(AABB box, Cylinder cylinder);
    bool Intersection(AABB box, FlatCylinder cylinder);
    bool Intersection(Sphere sphere1, Sphere sphere2);

    /* Get the intersection normal */
    glm::vec3 IntersectionNormal(AABB box1, AABB box2);

    /* Smallest distance between two shapes SQUARED */
    float Distance2(Sphere sphere1, Sphere sphere2);
    float Distance2(Sphere sphere1, glm::vec3 point);
    float Distance2(AABB box, Sphere sphere);
    float Distance2(AABB box, glm::vec3 point);
    float Distance2(AABB box, Cylinder cylinder);
    float Distance2(AABB box, FlatCylinder cylinder);
    float Distance2(Cylinder cylinder, glm::vec3 point);
    float Distance2(glm::vec3 point1, glm::vec3 point2);

    /* Smallest distance between two shapes, slower than Distance2 */
    float Distance(Sphere sphere1, Sphere sphere2);
    float Distance(AABB box, Sphere sphere);
    float Distance(AABB box, glm::vec3 point);
    float Distance(AABB box, Cylinder cylinder);
    float Distance(AABB box, FlatCylinder cylinder);
    float Distance(glm::vec3 point1, glm::vec3 point2);
}
