#pragma once
#include <EASTL/vector.h>
#include <EASTL/algorithm.h>
#include <new>

#include <stddef.h>
#include <assert.h>

namespace sk {

    template<class T>
    class GenerationalEntry {
        char m_entry[sizeof(T)];
        size_t m_generation;
        bool m_alive;
    public:
        GenerationalEntry() : m_generation(0), m_alive(true) {
            new(m_entry) T();
        }

        GenerationalEntry(const T& item) : m_generation(0), m_alive(true) {
            new(m_entry) T(item);
        }
        GenerationalEntry(T&& item) : m_generation(0), m_alive(true) {
            new(m_entry) T(std::move(item));
        }

        GenerationalEntry(const GenerationalEntry& from) : m_generation(from.m_generation), m_alive(from.m_alive) {
            if(m_alive) new(m_entry) T(from.value());
        }
        GenerationalEntry(GenerationalEntry&& from) : m_generation(from.m_generation), m_alive(from.m_alive) {
            if(m_alive) new(m_entry) T(std::move(from.value()));
        }

        ~GenerationalEntry() {
            if(m_alive) static_cast<T*>(static_cast<void*>(m_entry))->~T();
        }

        const T& value() const { return *static_cast<const T*>(static_cast<const void*>(m_entry)); }
        T& value() { return *static_cast<T*>(static_cast<void*>(m_entry)); }

        size_t generation() const { return m_generation; }
        bool alive() const { return m_alive; }

        void destroy() {
            assert(m_alive == true);
            m_alive = false;
            ++m_generation;
            static_cast<T*>(static_cast<void*>(m_entry))->~T();
        }
        template<class...Targs>
        void construct(Targs&&...args) {
            assert(m_alive == false);
            m_alive = true;
            new(m_entry) T(std::forward<Targs>(args)...);
        }
    };

    template<class> class GenerationalHeap;

    template<class T>
    class GenerationalIndex {
        friend GenerationalHeap<T>;
        size_t generation;
        size_t index;
    public:
        GenerationalIndex() : generation(0), index(0) {}
        GenerationalIndex(size_t gen, size_t ind) : generation(gen), index(ind) {}
    };

    template<class T>
    class GenerationalHandle {
        const GenerationalHeap<T>* m_heap;
        GenerationalIndex<T> m_index;
    public:
        GenerationalHandle(const GenerationalHeap<T>& heap, GenerationalIndex<T> index) : m_heap(&heap), m_index(index) {}
        GenerationalHandle(nullptr_t) : m_heap(nullptr), m_index() {}

        GenerationalIndex<T> index() const {
            return m_index;
        }
        explicit operator GenerationalIndex<T>() const {
            return m_index;
        }

        bool has_value() const {
            if(m_heap == nullptr) return false;
            return m_heap->contains(m_index);
        }
        operator bool() const {
            return has_value();
        }

        T& value() {
            return *(*const_cast<GenerationalHeap<T>*>(m_heap))[m_index]; // dumb
        }
        const T& value() const {
            return *(*m_heap)[m_index];
        }

        T& operator*() { return value(); }
        const T& operator*() const { return value(); }

        T* operator->() { return &value(); }
        const T* operator->() const { return &value(); }
    };

    template<class T>
    class GenerationalIterator {
        friend GenerationalHeap<T>;
        const GenerationalHeap<T>* m_heap;
        size_t m_index;

    public:
        GenerationalIterator(const GenerationalHeap<T>* heap, size_t index = 0) : m_heap(heap), m_index(index) {}

        bool operator==(const GenerationalIterator& other) const {
            return m_heap == other.m_heap && m_index == other.m_index;
        }
        bool operator!=(const GenerationalIterator& other) const {
            return m_heap != other.m_heap || m_index != other.m_index;
        }
        GenerationalIterator& operator++() {
            auto size = m_heap->vector.size();
            do ++m_index;
            while(m_index < size && !m_heap->vector[m_index].alive());
            return *this;
        }

        T& operator*() {
            return const_cast<GenerationalHeap<T>*>(m_heap)->vector[m_index].value();
        }
        const T& operator*() const {
            return m_heap->vector[m_index].value();
        }

    };

    template<class T>
    class GenerationalHeap {
        friend GenerationalIterator<T>;

        using IndexType = GenerationalIndex<T>;
        using HandleType = GenerationalHandle<T>;

        eastl::vector<GenerationalEntry<T>> vector;

        auto find_unused() const {
            return eastl::find_if(vector.begin(), vector.end(), [](const GenerationalEntry<T>& entry) {
                return !entry.alive();
            });
        }
        auto find_unused() {
            return eastl::find_if(vector.begin(), vector.end(), [](const GenerationalEntry<T>& entry) {
                return !entry.alive();
            });
        }
        HandleType make_handle(typename eastl::vector<GenerationalEntry<T>>::iterator it) {
            assert(it >= vector.begin()); // make sure we dont cast a negative value to size_t
            return HandleType(*this, IndexType{ it->generation(), static_cast<size_t>(it - vector.begin()) });
        }
        const HandleType make_handle(typename eastl::vector<GenerationalEntry<T>>::const_iterator it) const {
            assert(it >= vector.begin()); // make sure we dont cast a negative value to size_t
            return HandleType(*this, IndexType{ it->generation(), static_cast<size_t>(it - vector.begin()) });
        }
    public:

        GenerationalHeap() {}

        GenerationalHeap(size_t capacity) {
            vector.reserve(capacity);
        }

        const GenerationalIterator<T> begin() const { return GenerationalIterator<T>(this); }
        GenerationalIterator<T> begin() { return GenerationalIterator<T>(this); }

        const GenerationalIterator<T> end() const { return GenerationalIterator<T>(this, vector.size()); }
        GenerationalIterator<T> end() { return GenerationalIterator<T>(this, vector.size()); }

        bool contains(IndexType index) const {
            assert(index.index < vector.size());
            const GenerationalEntry<T>& item = vector[index.index];
            return item.generation() == index.generation;
        }

        bool contains(const T& item) const {
            return static_cast<bool>(find(item));
        }

        HandleType find(const T& item) {
            auto it = eastl::find(vector.begin(), vector.end(), item);
            if(it == vector.end()) return nullptr;
            if(!it->alive()) return nullptr;
            return make_handle(it);
        }

        const HandleType find(const T& item) const {
            auto it = eastl::find(vector.begin(), vector.end(), item);
            if(it == vector.end()) return nullptr;
            if(!it->alive()) return nullptr;
            return make_handle(it);
        }

        template<class UnaryPred>
        HandleType find_if(UnaryPred pred) {
            auto it = eastl::find_if(vector.begin(), vector.end(), [pred](const GenerationalEntry<T>& entry) {
                if(!entry.alive()) return false;
                return pred(entry.value());
            });
            if(it == vector.end()) return nullptr;
            return make_handle(it);
        }

        template<class UnaryPred>
        const HandleType find_if(UnaryPred pred) const {
            auto it = eastl::find_if(vector.begin(), vector.end(), [pred](const GenerationalEntry<T>& entry) {
                if(!entry.alive()) return false;
                return pred(entry.value());
            });
            if(it == vector.end()) return nullptr;
            return make_handle(it);
        }

        HandleType insert(const T& item) {
            auto it = find_unused();
            if(it == vector.end()) {
                if(vector.size() == vector.capacity())
                    vector.set_capacity(vector.capacity()*1.5f);
                vector.push_back(item);
                it = vector.end() - 1; // this is needed
            }
            else it->construct(item);
            return make_handle(it);
        }

        template<class...Targs>
        HandleType emplace(Targs&&...args) {
            auto it = find_unused();
            if(it == vector.end()) {
                if(vector.size() == vector.capacity())
                    vector.set_capacity(vector.capacity()*1.5f);
                vector.emplace_back(T(std::forward<Targs>(args)...));
                it = vector.end() - 1; // this is needed
            }
            else it->construct(eastl::forward<Targs>(args)...);
            return make_handle(it);
        }

        void erase(IndexType index) {
            assert(index.index < vector.size());
            GenerationalEntry<T>& item = vector[index.index];
            item.destroy();
        }

        void erase(GenerationalIterator<T> it) {
            GenerationalEntry<T>& item = vector[it.m_index];
            item.destroy();
        }

        void erase(const T& item) {
            if(auto opt = find(item)) erase(opt.value());
        }

        T* operator[](IndexType index) {
            assert(index.index < vector.size());
            GenerationalEntry<T>& item = vector[index.index];
            if(item.generation() == index.generation) return &item.value();
            else return nullptr;
        }

        const T* operator[](IndexType index) const {
            assert(index.index < vector.size());
            GenerationalEntry<T>& item = vector[index.index];
            if(item.generation() == index.generation) return &item.value();
            else return nullptr;
        }
    };

}