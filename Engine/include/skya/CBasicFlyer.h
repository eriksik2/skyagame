#pragma once
#include <skya/Component.h>
#include <skya/Types.h>

namespace sk {
    class CBasicFlyer : public Component {
    public:
        void OnStart();

        void OnKeyEvent(KeyEventArgs e);
        void OnMouseButtonEvent(MouseButtonEventArgs e);
        void OnMouseMoveEvent(CursorPosEventArgs e);

        void OnUpdate(double dt);

        float m_AccelSpeed = .1f;
    private:

        bool b_focused = false;

        int fwd = 0;
        int left = 0;

        float mousex = 0;
        float mousey = 0;

        float rotx = 0;
        float roty = 0;
    };
}

