#pragma once

#include <chrono>
#include <regex>

#include <EASTL/string.h>
#include <EASTL/string_view.h>
#include <EASTL/map.h>
#include <EASTL/vector.h>

namespace sk::perf {
    using namespace std::chrono;

    time_point<steady_clock, microseconds> now();

    struct Timespan {
        Timespan() = default;
        inline Timespan(time_point<steady_clock, microseconds> start, time_point<steady_clock, microseconds> end)
            : m_start(start), m_end(end) {}
        time_point<steady_clock, microseconds> start_time() const;
        time_point<steady_clock, microseconds> end_time() const;
        microseconds duration() const;
    protected:
        time_point<steady_clock, microseconds> m_start;
        time_point<steady_clock, microseconds> m_end;
    };

    struct Timer : Timespan {
        void start();
        void end();
    };


    

    struct Profiler {
        friend struct ScopeTimer;
        struct ResultData {
            eastl::string_view function;
            Timespan span;
            int calldepth;
        };

        Profiler();

        void start_frame();
        void end_frame();
        void draw_imgui();

    private:
        unsigned frameno = 0;
        Timer frametime;
        int calldepth;
        eastl::vector<ResultData> results;

        bool do_capture = true;
    };
    extern Profiler MainProfiler;

    struct ScopeTimer {
        ScopeTimer(eastl::string_view name, Profiler& profiler = MainProfiler);
        ~ScopeTimer();
    private:
        eastl::string_view m_name;
        Profiler& m_profiler;
        int m_index;
        Timer m_timer;
    };

    inline eastl::string_view util_format_func_name(eastl::string_view str) {
        static std::regex rule(" *(virtual)? *[a-zA-Z0-9]+ *(__cdecl)? *");
        std::cmatch match;
        if(!std::regex_search(str.data(), match, rule)) return str;
        return eastl::string_view{match[0].second};
    }
}
#if defined(__FUNCSIG__)
#define SKYA_FUNCTION_NAME_RAW __FUNCSIG__
#elif defined(__PRETTY_FUNCTION__)
#define SKYA_FUNCTION_NAME_RAW __PRETTY_FUNCTION__
#elif defined(__FUNCTION__)
#define SKYA_FUNCTION_NAME_RAW __FUNCTION__
#else
#define SKYA_FUNCTION_NAME_RAW __func__
#endif
#define SKYA_DEFINE_FUNCTION_NAME() static eastl::string_view SKYA_FUNCTION_NAME{sk::perf::util_format_func_name(SKYA_FUNCTION_NAME_RAW)}

//#define SKYA_HIDDEN(NAME) SKYA_CAT(___skya__, SKYA_CAT(NAME, __LINE__))
#define SKYA_HIDDEN(NAME) ___skya__ ## NAME ## __ ## __LINE__
#define SKYA_UNNAMED SKYA_HIDDEN(unnamed)

#define SKYA_PROFILE_SCOPE(NAME) sk::perf::ScopeTimer SKYA_HIDDEN(timer)(NAME)
#define SKYA_PROFILE_FUNCTION() SKYA_DEFINE_FUNCTION_NAME(); SKYA_PROFILE_SCOPE(SKYA_FUNCTION_NAME)