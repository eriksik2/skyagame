#pragma once
#include <skya/Component.h>

#include <skya/grafx/Base.h>
#include <skya/MeshObject.h>

namespace sk {
        class CMesh : public sk::Component {
        public:
            ~CMesh();
            void OnLoad();
            void OnRender();

            void SetFramebuffer(sk::grafxbase::FramebufferHandle fb);
            void SetMesh(sk::MeshObject* mesh);
            void SetTexture(sk::grafxbase::TextureHandle tex);
            void SetShader(sk::grafxbase::ProgramHandle program);


        private:
            sk::grafxbase::FramebufferHandle Framebuffer;
            sk::grafxbase::ProgramHandle Shader;
            sk::grafxbase::TextureHandle Texture;
            sk::MeshObject*    m_Mesh = nullptr;
            sk::GenerationalHandle<sk::CCamera> m_camera = nullptr;

        };
}


