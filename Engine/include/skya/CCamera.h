#pragma once
#include <skya/Component.h>
#include <skya/Window.h>

#include <glm/fwd.hpp>

namespace sk {
        class CCamera : public sk::Component {
        public:
            glm::mat4 GetProjectionMatrix() const;

            void OnStart();

            float VerticalFov = 1.2f;
            float AspectRatio = 1;
            float NearZ = 0.3f;
            float FarZ = 1000;
        private:
            void OnFramebufferSizeEvent(FramebufferSizeEventArgs e);

        };
}

