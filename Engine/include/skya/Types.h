#pragma once
#include <skya/Enums.h>

namespace sk {

    struct KeyEventArgs {
        Keyboard::Key key;
        int scancode;
        Keyboard::Action action;
        Keyboard::Modifier mods;
    };
    struct CharEventArgs {
        wchar_t codepoint;
    };
    struct CursorPosEventArgs {
        double X;
        double Y;
    };
    struct CursorEnterEventArgs {
        bool entered;
    };
    struct MouseButtonEventArgs {
        Mouse::Button button;
        Mouse::Action action;
        int mods;
    };

    struct ScrollEventArgs {
        double xoffset, yoffset;
    };

    struct FramebufferSizeEventArgs {
        int width;
        int height;
    };

    struct WindowSizeEventArgs {
        int width;
        int height;
    };
}
