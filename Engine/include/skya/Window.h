#pragma once
#include <skya/Delegate.h>
#include <skya/Types.h>
#include <skya/imgui/imgui.h>

#include <glm/vec2.hpp>

struct GLFWwindow;
namespace sk {

    enum Window_CursorMode {
        CursorMode_Normal,      // Makes the cursor visible and behaving normally.
        CursorMode_Hidden,      // Makes the cursor invisible when it is over the content area of the window but does not restrict the cursor from leaving.
        CursorMode_Disabled     // Hides and grabs the cursor, providing virtual and unlimited cursor movement.
    };

    class Window {
    public:
        Window();

        Event<KeyEventArgs> OnKeyCallback;
        Event<CharEventArgs> OnCharCallback;
        Event<CursorPosEventArgs> OnCursorPosCallback;
        Event<CursorEnterEventArgs> OnCursorEnterCallback;
        Event<MouseButtonEventArgs> OnMouseButtonCallback;
        Event<ScrollEventArgs> OnScrollCallback;

        Event<FramebufferSizeEventArgs> OnFramebufferSizeCallback;
        Event<WindowSizeEventArgs> OnWindowSizeCallback;

        void StartGuiFrame() const;
        void RenderGuiFrame() const;

        void SwapBuffers();

        int ShouldClose() const;
        glm::ivec2 GetFramebufferSize() const;
        glm::ivec2 GetWindowSize() const;

        void SetWindowSize(int, int);

        void SetCursorMode(Window_CursorMode);
        Window_CursorMode GetCursorMode() const;
        //void SetStickyKeys(bool); glfwSetInputMode(window, GLFW_STICKY_KEYS, ...)
        //void SetStickyMouseButtons(bool); glfwSetInputMode(window, GLFW_STICKY_MOUSE_BUTTONS, ...)
        //void SetLockKeyMods(bool); glfwSetInputMode(window, GLFW_LOCK_KEY_MODS, ...)
        //void SetRawMouseMotion(bool); glfwSetInputMode(window, GLFW_RAW_MOUSE_MOTION, ...)
    private:
        GLFWwindow* const m_glfw_window;
        imgui m_gui;

        Window_CursorMode m_cursor_mode = CursorMode_Normal;

        int m_FramebufferHeight, m_FramebufferWidth;
        int m_WindowHeight, m_WindowWidth;

        static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
        static void CharCallback(GLFWwindow* window, unsigned int codepoint);
        static void CursorPosCallback(GLFWwindow* window, double xpos, double ypos);
        static void CursorEnterCallback(GLFWwindow* window, int entered);
        static void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
        static void ScrollCallback(GLFWwindow* window, double xoffset, double yoffset);
        static void FramebufferSizeCallback(GLFWwindow* window, int width, int height);
        static void WindowSizeCallback(GLFWwindow* window, int width, int height);
        // TODO: ScrollCallback, JoystickCallback, DropCallback, etc All of them
    };
}
