#include "skya/Profiling.h"
#include <skya/Game.h>

#include <steam/steam_api.h>

#include <skya/grafx/Base.h>
#include <GLFW/glfw3.h>

#include <stdint.h>
void* __cdecl operator new[](size_t ,size_t ,size_t size, const char* name, int flags, unsigned debugFlags, const char* file, int line) {
	return new uint8_t[size];
}
void* __cdecl operator new[](size_t size, const char* name, int flags, unsigned debugFlags, const char* file, int line) {
	return new uint8_t[size];
}

using namespace sk;

Game::Game() {

#ifdef _WIN32
    //glFrontFace(GL_CW);
#endif

}

void Game::Start() {
    PreLoad();
    for(auto f : OnLoad) f();
    loaded = true;
    PostLoad();

    PreStart();
    for(auto f : OnStart) f();
    started = true;
    PostStart();

    window.SetWindowSize(1280, 720);
    do {
        auto dt = float(glfwGetTime());
        glfwSetTime(0);
        if(dt > 0.5) dt = 0.5;
        time_left_over += dt;

        {
            SKYA_PROFILE_SCOPE("Game Update");

            while(time_left_over >= timestep) {
                PreUpdate(timestep);
                for(auto f : OnUpdate) f(timestep);
                PostUpdate(timestep);
                time_left_over -= timestep;
            }
        }

        window.StartGuiFrame();
        {
            SKYA_PROFILE_SCOPE("Game Draw");

            PreRender();
            for(auto f : OnRender) f();
            PostRender();
        }
        sk::perf::MainProfiler.end_frame();
        sk::perf::MainProfiler.draw_imgui();
        sk::perf::MainProfiler.start_frame();

        window.RenderGuiFrame();

        {   SKYA_PROFILE_SCOPE("Buffer Swap");
            grafxbase::flush_command_queue();
            window.SwapBuffers();
        }

        //SteamAPI_RunCallbacks();
        {   SKYA_PROFILE_SCOPE("glfwPollEvents");
            glfwPollEvents();
        }
    } while(!window.ShouldClose());
    PreEnd();
    for(auto f : OnEnd) f();
    PostEnd();
}

