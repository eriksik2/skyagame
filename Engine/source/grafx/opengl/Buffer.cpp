#include "Buffer_internal.h"

#include "State.h"
using namespace sk::grafxbase;

GLenum ConvertBufferUsageHintFlag(BufferUsageHintFlag flag) {
    if(flag & BufferUsageHint_ClientWrite  && flag & BufferUsageHint_ClientRead)  flag &= ~BufferUsageHint_ClientRead;
    if(flag & BufferUsageHint_DynamicWrite && flag & BufferUsageHint_DynamicRead) flag &= ~BufferUsageHint_DynamicRead;
    switch (flag) {
        case BufferUsageHint_DynamicWrite | BufferUsageHint_ClientWrite: return GL_DYNAMIC_DRAW;
        case BufferUsageHint_DynamicWrite | BufferUsageHint_ClientRead: return GL_DYNAMIC_READ;
        case BufferUsageHint_DynamicWrite: return GL_DYNAMIC_COPY;
        case BufferUsageHint_DynamicRead | BufferUsageHint_ClientWrite: return GL_STATIC_DRAW;
        case BufferUsageHint_DynamicRead | BufferUsageHint_ClientRead: return GL_STATIC_READ;
        case BufferUsageHint_DynamicRead: return GL_STATIC_COPY;
        case BufferUsageHint_ClientWrite: return GL_STREAM_DRAW;
        case BufferUsageHint_ClientRead: return GL_STREAM_READ;
        default: return GL_STREAM_COPY;
    }
}

GLbitfield ConvertBufferMapAccessFlag(BufferMapAccessFlag flag) {
    GLbitfield out = 0;
    if(flag & BufferMapAccess_Read) out |= GL_MAP_READ_BIT;
    if(flag & BufferMapAccess_Write) out |= GL_MAP_WRITE_BIT;
    return out;
}


namespace sk::grafxbase {

void* get_backend_handle(RawBufferHandle buffer) {
    return gl_handle_ptr(obj(buffer));
}

RawBufferHandle create_buffer_raw(){
    BufferObject* out = new BufferObject;
    glGenBuffers(1, &out->gl_handle);
    auto orig = buffer_binding(GL_ARRAY_BUFFER);
    glBindBuffer(GL_ARRAY_BUFFER, gl_handle(out));
    glBindBuffer(GL_ARRAY_BUFFER, gl_handle(orig));
    return {{out}};
}

void delete_buffer(RawBufferHandle buffer){
    auto buf = obj(buffer)->gl_handle;
    glDeleteBuffers(1, &buf);
    delete obj(buffer);
    // TODO :  If a buffer object that is currently bound is deleted, the binding reverts to 0
}


int buffer_bytes(RawBufferHandle buffer) {
    return obj(buffer)->byte_count;
}

void allocate_buffer_raw(RawBufferHandle buffer, int byte_count, const void* data, BufferUsageHintFlag usage) {
    bind_buffer(GL_ARRAY_BUFFER, obj(buffer));
    glBufferData(GL_ARRAY_BUFFER, byte_count, data, ConvertBufferUsageHintFlag(usage));
    obj(buffer)->byte_count = byte_count;
}

void modify_buffer_raw(RawBufferHandle buffer, int byte_offset, int byte_count, const void* data){
    bind_buffer(GL_COPY_WRITE_BUFFER, obj(buffer));
    glBufferSubData(GL_COPY_WRITE_BUFFER, byte_offset, byte_count, data);
}

void copy_buffer_raw(RawBufferHandle from, RawBufferHandle to, int byte_offset_from, int byte_offset_to, int byte_count){
    bind_buffer(GL_COPY_READ_BUFFER, obj(from));
    bind_buffer(GL_COPY_WRITE_BUFFER, obj(to));
    glCopyBufferSubData(GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER, byte_offset_from, byte_offset_to, byte_count);
}

void read_buffer_raw(RawBufferHandle buffer, int byte_offset, int byte_count, void* dest){
    bind_buffer(GL_COPY_READ_BUFFER, obj(buffer));
    glGetBufferSubData(GL_COPY_READ_BUFFER, byte_offset, byte_count, dest);
}

void* map_buffer_raw(RawBufferHandle buffer, int byte_offset, int byte_count, BufferMapAccessFlag flags){
    bind_buffer(GL_COPY_READ_BUFFER, obj(buffer));
    return glMapBufferRange(GL_COPY_READ_BUFFER, byte_offset, byte_count, ConvertBufferMapAccessFlag(flags));
}
bool unmap_buffer(RawBufferHandle buffer){
    bind_buffer(GL_COPY_READ_BUFFER, obj(buffer));
    return glUnmapBuffer(GL_COPY_READ_BUFFER);
}
}
