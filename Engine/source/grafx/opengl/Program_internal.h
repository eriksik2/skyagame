#pragma once
#include <skya/grafx/base/Program.h>
#include "internal.h"

#include "Texture_internal.h"

#include <EASTL/map.h>

namespace sk::grafxbase {
    struct ProgramObject : GLObject {
        eastl::map<int, TextureObject*> sampler_texture_map;
        eastl::map<int, int> sampler_unit_map;
    };

    constexpr auto obj(ProgramHandle p) {
        return (ProgramObject*)p.internal_handle;
    }
}
