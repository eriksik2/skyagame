#include "Shader_internal.h"

#include <cassert>

using namespace sk::grafxbase;

namespace sk::grafxbase{

void* get_backend_handle(ShaderHandle h) {
    return gl_handle_ptr(obj(h));
}

GLenum convertShaderType(ShaderType type){
    switch (type) {
        case ShaderType_Vertex: return GL_VERTEX_SHADER;
        case ShaderType_Fragment: return GL_FRAGMENT_SHADER;
    }
    assert(false);
    return 0;
}

ShaderHandle create_shader(ShaderType type){
    auto out = new ShaderObject;
    out->gl_handle = glCreateShader(convertShaderType(type));
    return ShaderHandle{{out}};
}
void delete_shader(ShaderHandle shader){
    glDeleteShader(obj(shader)->gl_handle);
    delete obj(shader);
}

void source_shader(ShaderHandle shader, eastl::string_view source){
    int length = source.length();
    const char* data = source.data();
    glShaderSource(obj(shader)->gl_handle, 1, &data, &length);
}


bool compile_shader(ShaderHandle shader, eastl::string* error_log){
    glCompileShader(obj(shader)->gl_handle);
    int compile_status;
    glGetShaderiv(obj(shader)->gl_handle, GL_COMPILE_STATUS, &compile_status);
    if(compile_status) return true;
    if(error_log) {
        GLint logLength;
        glGetShaderiv(obj(shader)->gl_handle, GL_INFO_LOG_LENGTH, &logLength);
        char* log = (char*)malloc(logLength*sizeof(char));
        glGetShaderInfoLog(obj(shader)->gl_handle, logLength, nullptr, log);
        *error_log = eastl::string(log);
        free(log);
    }
    return false;
}
}
