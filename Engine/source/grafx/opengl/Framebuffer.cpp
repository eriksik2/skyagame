#include "Framebuffer_internal.h"

#include "Texture_internal.h"
#include "State.h"

#include <EASTL/algorithm.h>
#include <EASTL/vector.h>

using namespace sk::grafxbase;

namespace sk::grafxbase{

    FramebufferObject DefaultFramebufferObject;
    FramebufferHandle DefaultFramebuffer = {{&DefaultFramebufferObject}};

    void* get_backend_handle(FramebufferHandle h) {
        return gl_handle_ptr(obj(h));
    }

    FramebufferHandle create_framebuffer(){
        auto out = new FramebufferObject;
        glGenFramebuffers(1, &out->gl_handle);
        return FramebufferHandle{{out}};
    }

    void delete_framebuffer(FramebufferHandle fb){
        glDeleteFramebuffers(1, &obj(fb)->gl_handle);
        delete obj(fb);
    }

    void bind_framebuffer_depth_texture(FramebufferHandle fb, Texture2DHandle tex, int mipmap_level){
        auto object = obj(fb);
        bind_framebuffer(GL_DRAW_FRAMEBUFFER, object);
        glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, obj(tex)->type, obj(tex)->gl_handle, mipmap_level);
    }

    void internal_bind_framebuffer_texture(FramebufferHandle fb, GLenum location, Texture2DHandle tex, int mipmap_level){
        auto object = obj(fb);
        auto [it, did_insert] = object->active_output_slots.insert(location);
        if(did_insert) object->need_flush = true;

        bind_framebuffer(GL_DRAW_FRAMEBUFFER, object);
        glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + location, obj(tex)->type, obj(tex)->gl_handle, mipmap_level);
    }

    void bind_framebuffer_texture(FramebufferHandle fb, int location, Texture2DHandle tex, int mipmap_level)
    { internal_bind_framebuffer_texture(fb,location, tex, mipmap_level); }

    void set_framebuffer_blend(FramebufferHandle fb, BlendFactor newfactor, BlendFactor oldfactor){
        auto object = obj(fb);
        if(newfactor == object->m_newfactor && oldfactor == object->m_oldfactor) return;
        object->m_newfactor = newfactor;
        object->m_oldfactor = oldfactor;
    }

    void clear_framebuffer(FramebufferHandle fb, glm::vec4 values, eastl::span<int> slots){
        if(slots.empty()) return;
        eastl::vector<unsigned> filled;
        filled.set_capacity(*(slots.end() - 1));
        for(int i : slots){
            while(i > filled.size()) filled.push_back(GL_NONE);
            filled.push_back(GL_COLOR_ATTACHMENT0 + i);
        }
        auto object = obj(fb);

        bind_framebuffer(GL_DRAW_FRAMEBUFFER, object);
        glDrawBuffers(filled.size(), filled.data());
        glClearColor(values.r, values.g, values.b, values.a);
        glClear(GL_COLOR_BUFFER_BIT);
        object->need_flush = true;
    }

    void clear_framebuffer(FramebufferHandle fb, glm::vec4 values){
        bind_framebuffer(GL_DRAW_FRAMEBUFFER, obj(fb));
        glClearColor(values.r, values.g, values.b, values.a);
        internal_flush_framebuffer(fb);
        glClear(GL_COLOR_BUFFER_BIT);
    }

    void clear_framebuffer_depth(FramebufferHandle fb, float value){
        bind_framebuffer(GL_DRAW_FRAMEBUFFER, obj(fb));
        glClearDepth(value);
        glClear(GL_DEPTH_BUFFER_BIT);
    }

    void clear_framebuffer_stencil(FramebufferHandle fb, int value){
        bind_framebuffer(GL_DRAW_FRAMEBUFFER, obj(fb));
        glClearStencil(value);
        glClear(GL_STENCIL_BUFFER_BIT);
    }



    GLenum ConvertBlendFactor(BlendFactor blend){
        switch (blend) {
        case BlendFactor_Zero: return GL_ZERO;
        case BlendFactor_One: return GL_ONE;
        case BlendFactor_NewAlpha: return GL_SRC_ALPHA;
        case BlendFactor_NewAlphaInverted: return GL_ONE_MINUS_SRC_ALPHA;
        }
    }

    void internal_flush_framebuffer(FramebufferHandle fb){
        auto object = obj(fb);

        glBlendFunc(ConvertBlendFactor(object->m_newfactor), ConvertBlendFactor(object->m_oldfactor));

        //bind_framebuffer(GL_DRAW_FRAMEBUFFER, object);
        //auto v = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);//GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT
        //printf("%d\n", v);  

        if(!object->need_flush) return;

        // construct list (of active draw buffers) that can be passed to opengl
        int max = *eastl::max_element(object->active_output_slots.begin(), object->active_output_slots.end());
        eastl::vector<unsigned int> slots;
        slots.resize(max + 1, GL_NONE); // if max is 2 then slots needs space for 0, 1, 2
        for(auto active : object->active_output_slots){
            slots[active] = GL_COLOR_ATTACHMENT0 + active;
        }

        bind_framebuffer(GL_DRAW_FRAMEBUFFER, object);
        glDrawBuffers(slots.size(), slots.data());

        object->need_flush = false;
    }
}
