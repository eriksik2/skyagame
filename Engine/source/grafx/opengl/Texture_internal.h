#pragma once
#include <skya/grafx/base/Texture.h>
#include "internal.h"

namespace sk::grafxbase {
    struct TextureObject : GLObject {
        GLuint type;
        TextureFormat format;
    };

    constexpr auto obj(TextureHandle p) {
        return (TextureObject*)p.internal_handle;
    }
}

