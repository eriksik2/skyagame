#include "Texture_internal.h"

#include "State.h"
#include "skya/grafx/base/Texture.h"

#include <SOIL.h>

using namespace sk::grafxbase;

namespace sk::grafxbase {

    const TextureFormat TextureFormat_Depth = { TextureFormatType_Depth, 1, Bitdepth_16 };
    const TextureFormat TextureFormat_RG32 = { TextureFormatType_Float, 2, Bitdepth_32 };
    const TextureFormat TextureFormat_RGB32 = { TextureFormatType_Float, 3, Bitdepth_32 };
    const TextureFormat TextureFormat_RGBA32 = { TextureFormatType_Float, 4, Bitdepth_32 };
    const TextureFormat TextureFormat_RG16 = { TextureFormatType_Float, 2, Bitdepth_16 };
    const TextureFormat TextureFormat_RGB16 = { TextureFormatType_Float, 3, Bitdepth_16 };
    const TextureFormat TextureFormat_RGBA16 = { TextureFormatType_Float, 4, Bitdepth_16 };
    const TextureFormat TextureFormat_RG8 = { TextureFormatType_Float, 2, Bitdepth_8 };
    const TextureFormat TextureFormat_RGB8 = { TextureFormatType_Float, 3, Bitdepth_8 };
    const TextureFormat TextureFormat_RGBA8 = { TextureFormatType_Float, 4, Bitdepth_8 };

void* get_backend_handle(TextureHandle h) {
    return gl_handle_ptr(obj(h));
}

Texture2DHandle load_texture_2D(eastl::string_view path) {
    GLuint tex = SOIL_load_OGL_texture(path.data(),
		SOIL_LOAD_AUTO,
		SOIL_CREATE_NEW_ID,
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_NTSC_SAFE_RGB
	);
    if(!tex) return {};
    TextureObject* out = new TextureObject;
    out->type = GL_TEXTURE_2D;
    out->gl_handle = tex;
    return {{{out}}};
}


TextureObject* create_texture(GLenum type){
    TextureObject* out = new TextureObject;
    out->type = type;
    glGenTextures(1, &out->gl_handle);
    auto orig = texture_binding(type);
    bind_texture(type, out);
    bind_texture(type, orig);
    return out;
}
Texture1DHandle create_texture_1D()
{ return {{{create_texture(GL_TEXTURE_1D)}}}; }
Texture2DHandle create_texture_2D()
{ return {{{create_texture(GL_TEXTURE_2D)}}}; }
Texture3DHandle create_texture_3D()
{ return {{{create_texture(GL_TEXTURE_3D)}}}; }
TextureCubemapHandle create_texture_cubemap()
{ return {{{create_texture(GL_TEXTURE_CUBE_MAP)}}}; }

void delete_texture(TextureHandle tex){
    auto t = obj(tex)->gl_handle;
    glDeleteTextures(1, &t);
    if(texture_binding(obj(tex)->type) == obj(tex)) bind_texture(obj(tex)->type, 0);
    delete obj(tex);
}

void texture_unit(TextureHandle tex, int unit){
    bind_texture(obj(tex)->type, obj(tex), unit);
}


GLenum ConvertTextureScaleType(TextureScaleType type) {
    switch (type) {
    case TextureScaleType_Nearest: return GL_NEAREST;
    case TextureScaleType_Linear: return GL_LINEAR;
    }
}

void set_texture_mag_filter(TextureHandle tex, TextureScaleType type){
    bind_texture(obj(tex)->type, obj(tex));
    glTexParameteri(obj(tex)->type, GL_TEXTURE_MAG_FILTER, ConvertTextureScaleType(type));
}

void set_texture_min_filter(TextureHandle tex, TextureScaleType type){
    bind_texture(obj(tex)->type, obj(tex));
    glTexParameteri(obj(tex)->type, GL_TEXTURE_MIN_FILTER, ConvertTextureScaleType(type));
}
void set_texture_minmag_filter(TextureHandle tex, TextureScaleType type){
    set_texture_mag_filter(tex, type);
    set_texture_min_filter(tex, type);
}


GLenum ConvertTextureCubemapSide(TextureCubemapSide side) {
    switch (side) {
    case TextureCubemapSide_PositiveX: return GL_TEXTURE_CUBE_MAP_POSITIVE_X;
    case TextureCubemapSide_NegativeX: return GL_TEXTURE_CUBE_MAP_NEGATIVE_X;
    case TextureCubemapSide_PositiveY: return GL_TEXTURE_CUBE_MAP_POSITIVE_Y;
    case TextureCubemapSide_NegativeY: return GL_TEXTURE_CUBE_MAP_NEGATIVE_Y;
    case TextureCubemapSide_PositiveZ: return GL_TEXTURE_CUBE_MAP_POSITIVE_Z;
    case TextureCubemapSide_NegativeZ: return GL_TEXTURE_CUBE_MAP_NEGATIVE_Z;
    }
}

GLenum toInternalFormat(TextureFormat format){
    switch (format.type) {
    break;case TextureFormatType_Depth:
        switch (format.bitdepth) {
        break;case Bitdepth_8:case Bitdepth_16: return GL_DEPTH_COMPONENT16;
        break;case Bitdepth_32: return GL_DEPTH_COMPONENT32F;
        }
    break;case TextureFormatType_Float: 
        switch (format.bitdepth) {
        break;case Bitdepth_8:case Bitdepth_16:
            switch (format.components) {
            break;case 1: return GL_R16F;
            break;case 2: return GL_RG16F;
            break;case 3: return GL_RGB16F;
            break;case 4: return GL_RGBA16F;
            }
        break;case Bitdepth_32:
            switch (format.components) {
            break;case 1: return GL_R32F;
            break;case 2: return GL_RG32F;
            break;case 3: return GL_RGB32F;
            break;case 4: return GL_RGBA32F;
            }
        }
    break;case TextureFormatType_Signed:
        switch (format.bitdepth) {
        break;case Bitdepth_8:
            switch (format.components) {
            break;case 1: return GL_R8I;
            break;case 2: return GL_RG8I;
            break;case 3: return GL_RGB8I;
            break;case 4: return GL_RGBA8I;
            }
        break;case Bitdepth_16:
            switch (format.components) {
            break;case 1: return GL_R16I;
            break;case 2: return GL_RG16I;
            break;case 3: return GL_RGB16I;
            break;case 4: return GL_RGBA16I;
            }
        break;case Bitdepth_32:
            switch (format.components) {
            break;case 1: return GL_R32I;
            break;case 2: return GL_RG32I;
            break;case 3: return GL_RGB32I;
            break;case 4: return GL_RGBA32I;
            }
        }
    break;case TextureFormatType_Unsigned:
        switch (format.bitdepth) {
        break;case Bitdepth_8:
            switch (format.components) {
            break;case 1: return GL_R8UI;
            break;case 2: return GL_RG8UI;
            break;case 3: return GL_RGB8UI;
            break;case 4: return GL_RGBA8UI;
            }
        break;case Bitdepth_16:
            switch (format.components) {
            break;case 1: return GL_R16UI;
            break;case 2: return GL_RG16UI;
            break;case 3: return GL_RGB16UI;
            break;case 4: return GL_RGBA16UI;
            }
        break;case Bitdepth_32:
            switch (format.components) {
            break;case 1: return GL_R32UI;
            break;case 2: return GL_RG32UI;
            break;case 3: return GL_RGB32UI;
            break;case 4: return GL_RGBA32UI;
            }
        }
    }
    assert(0);
    return GL_RGBA;
}
GLenum toPixelTransferFormat(TextureFormat format, int given_components = 4){
    switch (format.type) {
    break;case TextureFormatType_Depth: return GL_DEPTH_COMPONENT;
    break;case TextureFormatType_Float: 
        switch (eastl::min(format.components, given_components)) {
        break;case 1: return GL_RED;
        break;case 2: return GL_RG;
        break;case 3: return GL_RGB;
        break;case 4: return GL_RGBA;
        }
    break;case TextureFormatType_Signed:case TextureFormatType_Unsigned:
        switch (eastl::min(format.components, given_components)) {
        break;case 1: return GL_RED_INTEGER;
        break;case 2: return GL_RG_INTEGER;
        break;case 3: return GL_RGB_INTEGER;
        break;case 4: return GL_RGBA_INTEGER;
        }
    }
    assert(0);
    return GL_RGBA;
}



void allocate_texture_internal(TextureObject* tex, TextureFormat format, glm::ivec3 size, int mipmap_levels){
    bind_texture(tex->type, tex);
    tex->format = format;
    GLenum internal_format = toInternalFormat(tex->format);
    GLenum transfer_format = toPixelTransferFormat(tex->format);
    switch (tex->type) {
    case GL_TEXTURE_1D: return glTexImage1D(tex->type, 0, internal_format, size.x, 0, transfer_format, GL_INT, nullptr);
    case GL_TEXTURE_2D: return glTexImage2D(tex->type, 0, internal_format, size.x, size.y, 0, transfer_format, GL_INT, nullptr);
    case GL_TEXTURE_3D: return glTexImage3D(tex->type, 0, internal_format, size.x, size.y, size.z, 0, transfer_format, GL_INT, nullptr);
    case GL_TEXTURE_CUBE_MAP: 
        GLenum side[] = {GL_TEXTURE_CUBE_MAP_POSITIVE_X, GL_TEXTURE_CUBE_MAP_NEGATIVE_X, GL_TEXTURE_CUBE_MAP_POSITIVE_Y, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, GL_TEXTURE_CUBE_MAP_POSITIVE_Z, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z};
        for(int i = 0; i < sizeof(side); ++i)
            glTexImage2D(side[i], 0, internal_format, size.x, size.x, 0, transfer_format, GL_INT, nullptr);
        return;
    }
}

void modify_texture_internal(TextureObject* tex, TextureCubemapSide side, int level, glm::ivec3 pos, glm::ivec3 size, int input_components, GLenum input_type, void* data){
    bind_texture(tex->type, tex);
    GLenum transfer_format = toPixelTransferFormat(tex->format, input_components);
    switch (tex->type) {
    case GL_TEXTURE_1D: return glTexSubImage1D(tex->type, level, pos.x, size.x, transfer_format, input_type, data);
    case GL_TEXTURE_2D: return glTexSubImage2D(tex->type, level, pos.x, pos.y, size.x, size.y, transfer_format, input_type, data);
    case GL_TEXTURE_3D: return glTexSubImage3D(tex->type, level, pos.x, pos.y, pos.z, size.x, size.y, size.z, transfer_format, input_type, data);
    case GL_TEXTURE_CUBE_MAP: 
        GLenum type = ConvertTextureCubemapSide(side);
        return glTexSubImage2D(type, level, pos.x, pos.y, size.x, size.y, transfer_format, input_type, data);
    }
}

void build_texture_mipmaps(TextureHandle tex){
    bind_texture(obj(tex)->type, obj(tex));
    glGenerateMipmap(obj(tex)->type);
}


void allocate_texture(Texture1DHandle tex, TextureFormat internal_format, int width, int mipmap_levels)
{ allocate_texture_internal(obj(tex), internal_format, glm::ivec3{width}, mipmap_levels); }

//void modify_texture_raw(Texture1DHandle tex, int level, int x, int width, TextureChannel channel, void* data)
//{ modify_texture_internal(obj(tex), TextureCubemapSide(0), level, glm::ivec3{x}, glm::ivec3{width}, channel, GL_FLOAT, data); }

void modify_texture(Texture1DHandle tex, int level, int x, int width, eastl::span<int> data)
{ modify_texture_internal(obj(tex), TextureCubemapSide(0), level, glm::ivec3{x}, glm::ivec3{width}, 1, GL_INT, data.data()); }
void modify_texture(Texture1DHandle tex, int level, int x, int width, eastl::span<glm::ivec3> data)
{ modify_texture_internal(obj(tex), TextureCubemapSide(0), level, glm::ivec3{x}, glm::ivec3{width}, 3, GL_INT, data.data()); }
void modify_texture(Texture1DHandle tex, int level, int x, int width, eastl::span<glm::ivec4> data)
{ modify_texture_internal(obj(tex), TextureCubemapSide(0), level, glm::ivec3{x}, glm::ivec3{width}, 4, GL_INT, data.data()); }


void allocate_texture(Texture2DHandle tex, TextureFormat internal_format, glm::ivec2 size, int mipmap_levels)
{ allocate_texture_internal(obj(tex), internal_format, glm::ivec3{size, 0}, mipmap_levels); }

//void modify_texture_raw(Texture2DHandle tex, int level, glm::ivec2 pos, glm::ivec2 size, TextureChannel channel, void* data)
//{ modify_texture_internal(obj(tex), TextureCubemapSide(0), level, glm::ivec3{pos, 0}, glm::ivec3{size, 0}, channel, GL_FLOAT, data); }

void modify_texture(Texture2DHandle tex, int level, glm::ivec2 pos, glm::ivec2 size, eastl::span<int> data)
{ modify_texture_internal(obj(tex), TextureCubemapSide(0), level, glm::ivec3{pos, 0}, glm::ivec3{size, 0}, 1, GL_INT, data.data()); }
void modify_texture(Texture2DHandle tex, int level, glm::ivec2 pos, glm::ivec2 size, eastl::span<glm::ivec3> data)
{ modify_texture_internal(obj(tex), TextureCubemapSide(0), level, glm::ivec3{pos, 0}, glm::ivec3{size, 0}, 3, GL_INT, data.data()); }
void modify_texture(Texture2DHandle tex, int level, glm::ivec2 pos, glm::ivec2 size, eastl::span<glm::ivec4> data)
{ modify_texture_internal(obj(tex), TextureCubemapSide(0), level, glm::ivec3{pos, 0}, glm::ivec3{size, 0}, 4, GL_INT, data.data()); }

void allocate_texture(Texture3DHandle tex, TextureFormat internal_format, glm::ivec3 size, int mipmap_levels)
{ allocate_texture_internal(obj(tex), internal_format, size, mipmap_levels); }

//void modify_texture_raw(Texture3DHandle tex, int level, glm::ivec3 pos, glm::ivec3 size, TextureChannel channel, void* data)
//{ modify_texture_internal(obj(tex), TextureCubemapSide(0), level, pos, size, channel, GL_INT, data); }

void modify_texture(Texture3DHandle tex, int level, glm::ivec3 pos, glm::ivec3 size, eastl::span<int> data)
{ modify_texture_internal(obj(tex), TextureCubemapSide(0), level, pos, size, 1, GL_INT, data.data()); }
void modify_texture(Texture3DHandle tex, int level, glm::ivec3 pos, glm::ivec3 size, eastl::span<glm::ivec3> data)
{ modify_texture_internal(obj(tex), TextureCubemapSide(0), level, pos, size, 3, GL_INT, data.data()); }
void modify_texture(Texture3DHandle tex, int level, glm::ivec3 pos, glm::ivec3 size, eastl::span<glm::ivec4> data)
{ modify_texture_internal(obj(tex), TextureCubemapSide(0), level, pos, size, 4, GL_INT, data.data()); }

void allocate_texture(TextureCubemapHandle tex, TextureFormat internal_format, int size, int mipmap_levels)
{ allocate_texture_internal(obj(tex), internal_format, glm::ivec3{size}, mipmap_levels); }

//void modify_texture_raw(TextureCubemapHandle tex, TextureCubemapSide side, int level, glm::ivec2 pos, glm::ivec2 size, TextureChannel channel, void* data)
//{ modify_texture_internal(obj(tex), side, level, glm::ivec3{pos, 0}, glm::ivec3{size, 0}, channel, GL_FLOAT, data); }

void modify_texture(TextureCubemapHandle tex, TextureCubemapSide side, int level, glm::ivec2 pos, glm::ivec2 size, eastl::span<int> data)
{ modify_texture_internal(obj(tex), side, level, glm::ivec3{pos, 0}, glm::ivec3{size, 0}, 1, GL_INT, data.data()); }
void modify_texture(TextureCubemapHandle tex, TextureCubemapSide side, int level, glm::ivec2 pos, glm::ivec2 size, eastl::span<glm::ivec3> data)
{ modify_texture_internal(obj(tex), side, level, glm::ivec3{pos, 0}, glm::ivec3{size, 0}, 3, GL_INT, data.data()); }
void modify_texture(TextureCubemapHandle tex, TextureCubemapSide side, int level, glm::ivec2 pos, glm::ivec2 size, eastl::span<glm::ivec4> data)
{ modify_texture_internal(obj(tex), side, level, glm::ivec3{pos, 0}, glm::ivec3{size, 0}, 4, GL_INT, data.data()); }
}
