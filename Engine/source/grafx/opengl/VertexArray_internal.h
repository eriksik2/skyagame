#pragma once
#include <skya/grafx/base/VertexArray.h>
#include "internal.h"

#include "Buffer_internal.h"

using namespace sk::grafxbase;

namespace sk::grafxbase {
    struct VertexArrayObject : GLObject {
        BufferObject* element_array_binding = 0;
    };

    constexpr auto obj(VertexArrayHandle p) {
        return (VertexArrayObject*)p.internal_handle;
    }
}