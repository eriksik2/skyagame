#pragma once
#include <GL/gl3w.h>
#include <cassert>

namespace sk::grafxbase {
    struct GLObject {
        unsigned gl_handle;
    };

    constexpr GLuint gl_handle(GLObject* object) { return object? object->gl_handle : 0; }
    constexpr void* gl_handle_ptr(GLObject* object) { return object? (void*)static_cast<unsigned long long>(object->gl_handle) : nullptr; }

}

