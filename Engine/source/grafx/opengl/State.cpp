#include "State.h"

#include "Buffer_internal.h"
#include "Framebuffer_internal.h"
#include "Program_internal.h"
#include "Shader_internal.h"
#include "Texture_internal.h"
#include "VertexArray_internal.h"

#include <EASTL/vector.h>

using namespace sk::grafxbase;

struct TextureUnitState {
    TextureObject* texture_1D_binding = 0;
    TextureObject* texture_2D_binding = 0;
    TextureObject* texture_3D_binding = 0;
    TextureObject* texture_1D_array_binding = 0;
    TextureObject* texture_2D_array_binding = 0;
    TextureObject* texture_rectangle_binding = 0;
    TextureObject* texture_cube_map_binding = 0;
    TextureObject* texture_cube_map_array_binding = 0;
    TextureObject* texture_buffer_binding = 0;
    TextureObject* texture_2D_multisample_binding = 0;
    TextureObject* texture_2D_multisample_array_binding = 0;
};

struct GLState {
    void* throwaway;

    VertexArrayObject* vertex_array_binding = 0;
    ProgramObject* program_binding = 0;

    BufferObject* array_buffer_binding = 0;
    BufferObject* atomic_counter_buffer_binding = 0;
    BufferObject* copy_read_buffer_binding = 0;
    BufferObject* copy_write_buffer_binding = 0;
    BufferObject* dispatch_indirect_buffer_binding = 0;
    BufferObject* draw_indirect_buffer_binding = 0;
    BufferObject* pixel_pack_buffer_binding = 0;
    BufferObject* pixel_unpack_buffer_binding = 0;
    BufferObject* query_buffer_binding = 0;
    BufferObject* shader_storage_buffer_binding = 0;
    BufferObject* transform_feedback_buffer_binding = 0;
    BufferObject* uniform_buffer_binding = 0;

    FramebufferObject* draw_framebuffer_binding = 0;
    FramebufferObject* read_framebuffer_binding = 0;

    GLuint active_texture_unit = 0;
    eastl::vector<TextureUnitState> texture_units;
};

GLState initial_state(){
    GLState state;
    {
        int max_texture_units;
        glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &max_texture_units);
        state.texture_units.resize(max_texture_units);
    }

    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);

    return state;
}

GLState& get_state() {
    static GLState state = initial_state();
    return state;
}

namespace sk::grafxbase {

template<class T, class...Targ>
bool state_bind(T object, T&(*call)(Targ...), Targ...args){
    T& bind = call(args...);
    if(bind == object) return false;
    bind = object;
    return true;
}

void flush_command_queue(){
    glFlush();
}


VertexArrayObject*& vertex_array_binding() {
    auto& state = get_state();
    return state.vertex_array_binding;
}
void bind_vertex_array(VertexArrayObject* object) {
    if(state_bind(object, &vertex_array_binding))
        glBindVertexArray(gl_handle(object));
}


ProgramObject*& program_binding() {
    auto& state = get_state();
    return state.program_binding;
}
void bind_program(ProgramObject* object) {
    if(state_bind(object, &program_binding))
        glUseProgram(gl_handle(object));
}


BufferObject*& buffer_binding(GLenum target) {
    auto& state = get_state();
    switch(target) {
    case GL_ARRAY_BUFFER: 
    return state.array_buffer_binding;
    case GL_ATOMIC_COUNTER_BUFFER: return state.atomic_counter_buffer_binding;
    case GL_COPY_READ_BUFFER: return state.copy_read_buffer_binding;
    case GL_COPY_WRITE_BUFFER: return state.copy_write_buffer_binding;
    case GL_DISPATCH_INDIRECT_BUFFER: return state.dispatch_indirect_buffer_binding;
    case GL_DRAW_INDIRECT_BUFFER: return state.draw_indirect_buffer_binding;
    case GL_PIXEL_PACK_BUFFER: return state.pixel_pack_buffer_binding;
    case GL_PIXEL_UNPACK_BUFFER: return state.pixel_unpack_buffer_binding;
    case GL_QUERY_BUFFER: return state.query_buffer_binding;
    case GL_SHADER_STORAGE_BUFFER: return state.shader_storage_buffer_binding;
    case GL_TRANSFORM_FEEDBACK_BUFFER: return state.transform_feedback_buffer_binding;
    case GL_UNIFORM_BUFFER: return state.uniform_buffer_binding;
    }
    assert(false && "Invalid target");
}
void bind_buffer(GLenum target, BufferObject* object) {
    if(state_bind(object, &buffer_binding, target))
        glBindBuffer(target, gl_handle(object));
}

TextureObject*& texture_binding(GLenum target, int unit) {
    auto& state = get_state().texture_units[unit];
    switch(target) {
    case GL_TEXTURE_1D: return state.texture_1D_binding;
    case GL_TEXTURE_2D: return state.texture_2D_binding;
    case GL_TEXTURE_3D: return state.texture_3D_binding;
    case GL_TEXTURE_1D_ARRAY: return state.texture_1D_array_binding;
    case GL_TEXTURE_2D_ARRAY: return state.texture_2D_array_binding;
    case GL_TEXTURE_RECTANGLE: return state.texture_rectangle_binding;
    case GL_TEXTURE_CUBE_MAP: return state.texture_cube_map_binding;
    case GL_TEXTURE_CUBE_MAP_ARRAY: return state.texture_cube_map_array_binding;
    case GL_TEXTURE_BUFFER: return state.texture_buffer_binding;
    case GL_TEXTURE_2D_MULTISAMPLE: return state.texture_2D_multisample_binding;
    case GL_TEXTURE_2D_MULTISAMPLE_ARRAY: return state.texture_2D_multisample_array_binding;
    }
    assert(false && "Invalid target");
}

void set_active_texture_unit(int unit) {
    auto& state = get_state();
    if(state.active_texture_unit == unit) return;
    state.active_texture_unit = unit;
    glActiveTexture(GL_TEXTURE0 + unit);
}
void bind_texture(GLenum target, TextureObject* object, int unit) {
    set_active_texture_unit(unit);
    if(state_bind(object, &texture_binding, target, unit))
        glBindTexture(target, gl_handle(object));
}


FramebufferObject*& framebuffer_binding(GLenum target) {
    auto& state = get_state();
    switch(target) {
    case GL_DRAW_FRAMEBUFFER: return state.draw_framebuffer_binding;
    case GL_READ_FRAMEBUFFER: return state.read_framebuffer_binding;
    }
    assert(false && "Invalid target");
}
void bind_framebuffer(GLenum target, FramebufferObject* object) {
    if(target == GL_FRAMEBUFFER) {
        auto*& draw_bind = framebuffer_binding(GL_DRAW_FRAMEBUFFER);
        auto*& read_bind = framebuffer_binding(GL_READ_FRAMEBUFFER);
        if(draw_bind == object && read_bind == object) return;
        draw_bind = object;
        read_bind = object;
    } else {
        auto*& bind = framebuffer_binding(target);
        if(bind == object) return;
        bind = object;
    }
    glBindFramebuffer(target, gl_handle(object));
}


}
