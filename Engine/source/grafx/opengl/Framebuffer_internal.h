#pragma once
#include <skya/grafx/base/Framebuffer.h>
#include "internal.h"

#include "Texture_internal.h"

#include <EASTL/set.h>

namespace sk::grafxbase {
    struct FramebufferObject : GLObject {
        eastl::set<int> active_output_slots;
        bool need_flush = false;

        // blending
        BlendFactor m_newfactor = BlendFactor_One;
        BlendFactor m_oldfactor = BlendFactor_Zero;
    };

    // flush framebuffer before using it in a draw command
    void internal_flush_framebuffer(FramebufferHandle fb);

    constexpr auto obj(FramebufferHandle p) {
        return (FramebufferObject*)p.internal_handle;
    }
}
