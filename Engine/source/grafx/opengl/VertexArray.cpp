#include "VertexArray_internal.h"

#include "State.h"

#include <EASTL/utility.h>

using namespace sk::grafxbase;

namespace sk::grafxbase {

void* get_backend_handle(VertexArrayHandle h) {
    return gl_handle_ptr(obj(h));
}

VertexArrayHandle create_vertex_array(){
    VertexArrayObject* out = new VertexArrayObject;
    glGenVertexArrays(1, &out->gl_handle);
    return VertexArrayHandle{{out}};
}
void delete_vertex_array(VertexArrayHandle array){
    glDeleteVertexArrays(1, &obj(array)->gl_handle);
    delete obj(array);
}

void enable_vertex_attribute(VertexArrayHandle array, int index){
    bind_vertex_array(obj(array));
    glEnableVertexAttribArray(index);
}
void disable_vertex_attribute(VertexArrayHandle array, int index){
    bind_vertex_array(obj(array));
    glDisableVertexAttribArray(index);
}

eastl::pair<int, int> split_ShaderDataType(GLenum type){
    switch(type){
        case ShaderDataType_Float: return eastl::make_pair(GL_FLOAT, 1);
        case ShaderDataType_Float2: return eastl::make_pair(GL_FLOAT, 2);
        case ShaderDataType_Float3: return eastl::make_pair(GL_FLOAT, 3);
        case ShaderDataType_Float4: return eastl::make_pair(GL_FLOAT, 4);
        case ShaderDataType_Int: return eastl::make_pair(GL_INT, 1);
        case ShaderDataType_Int2: return eastl::make_pair(GL_INT, 2);
        case ShaderDataType_Int3: return eastl::make_pair(GL_INT, 3);
        case ShaderDataType_Int4: return eastl::make_pair(GL_INT, 4);
    }
    return eastl::make_pair(GL_NONE, 1);
}

void bind_vertex_attribute_buffer(VertexArrayHandle array, int index, RawBufferHandle buffer, VertexAttributeFormat format){
    bind_vertex_array(obj(array));
    bind_buffer(GL_ARRAY_BUFFER, obj(buffer));
    auto [type, size] = split_ShaderDataType(format.type);
    glVertexAttribPointer(index, size, type, false, format.byte_stride, (void*)(long long)format.byte_offset);
}

void bind_index_buffer_internal(VertexArrayHandle array, RawBufferHandle buffer){
    if(obj(array)->element_array_binding == obj(buffer)) return;
    obj(array)->element_array_binding = obj(buffer);
    bind_vertex_array(obj(array));
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, obj(buffer)->gl_handle);
}
void bind_vertex_index_buffer(VertexArrayHandle array, BufferHandle<int> buffer)
{ bind_index_buffer_internal(array, buffer); }
void bind_vertex_index_buffer(VertexArrayHandle array, BufferHandle<unsigned int> buffer)
{ bind_index_buffer_internal(array, buffer); }

int vertex_index_buffer_size(VertexArrayHandle array){
    if(obj(array)->element_array_binding)
        return buffer_bytes({{obj(array)->element_array_binding}})/sizeof(int);
    return 0;
}
}
