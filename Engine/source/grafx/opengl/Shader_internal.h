#pragma once
#include <skya/grafx/base/Shader.h>
#include "internal.h"

namespace sk::grafxbase {
    struct ShaderObject : GLObject {
    };

    constexpr auto obj(ShaderHandle p) {
        return (ShaderObject*)p.internal_handle;
    }
}
