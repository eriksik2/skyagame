#pragma once
#include <skya/grafx/base/Buffer.h>
#include "internal.h"

namespace sk::grafxbase {
    struct BufferObject : GLObject {
        int byte_count = 0;
    };

    constexpr auto obj(RawBufferHandle p) {
        return (BufferObject*)p.internal_handle;
    }
}
