#include "Program_internal.h"
#include "Shader_internal.h"
#include "VertexArray_internal.h"

#include "State.h"

using namespace sk::grafxbase;

//// Private

// Simple wrapper around glGetProgramiv to return value.
int getProgramiv(ProgramHandle program, GLenum pname) {
    int out; glGetProgramiv(obj(program)->gl_handle, pname, &out);
    return out;
}

// Simple wrapper around glGetProgramInfoLog to return log as a string
eastl::string getProgramInfoLog(ProgramHandle program){
    int logLength = getProgramiv(program, GL_INFO_LOG_LENGTH);

    char* log = (char*)malloc(logLength*sizeof(char));
    glGetProgramInfoLog(obj(program)->gl_handle, logLength, nullptr, log);
    eastl::string out(log);
    free(log);
    return out;
}

ShaderDataType glType_to_skType(GLenum type){
    switch(type){
        case GL_FLOAT: return ShaderDataType_Float;
        case GL_FLOAT_VEC2: return ShaderDataType_Float2;
        case GL_FLOAT_VEC3: return ShaderDataType_Float3;
        case GL_FLOAT_VEC4: return ShaderDataType_Float4;
        case GL_FLOAT_MAT3: return ShaderDataType_Float33;
        case GL_FLOAT_MAT4: return ShaderDataType_Float44;
        case GL_INT: return ShaderDataType_Int;
        case GL_INT_VEC2: return ShaderDataType_Int2;
        case GL_INT_VEC3: return ShaderDataType_Int3;
        case GL_INT_VEC4: return ShaderDataType_Int4;
    }
    return ShaderDataType_None;
}

//// Public

namespace sk::grafxbase {

void* get_backend_handle(ProgramHandle h) {
    return gl_handle_ptr(obj(h));
}

ProgramHandle create_program() {
    ProgramObject* out = new ProgramObject;
    out->gl_handle = glCreateProgram();
    return ProgramHandle{{out}};
}
void delete_program(ProgramHandle program){
    glDeleteProgram(obj(program)->gl_handle);
    delete obj(program);
}

void bind_program_shader(ProgramHandle program, ShaderHandle shader){
    glAttachShader(obj(program)->gl_handle, obj(shader)->gl_handle);
}

void unbind_program_shader(ProgramHandle program, ShaderHandle shader){
    glDetachShader(obj(program)->gl_handle, obj(shader)->gl_handle);
}

bool link_program(ProgramHandle program, eastl::string *output){
    glLinkProgram(obj(program)->gl_handle);
    int linked;
    glGetProgramiv(obj(program)->gl_handle, GL_LINK_STATUS, &linked);
    if(linked) return true; // success
    if(output) *output = getProgramInfoLog((program));
    return false;
}


int program_attribute_count(ProgramHandle program){ return getProgramiv(program, GL_ACTIVE_ATTRIBUTES); }
int program_uniform_count(ProgramHandle program){ return getProgramiv(program, GL_ACTIVE_UNIFORMS); }
int program_shader_count(ProgramHandle program){ return getProgramiv(program, GL_ATTACHED_SHADERS); }

ShaderData program_uniform_info(ProgramHandle program, int index){
    int buffer_length = getProgramiv(program, GL_ACTIVE_UNIFORM_MAX_LENGTH);
    char* buffer = (char*)malloc(buffer_length*sizeof(char));
    GLint count;
    GLenum type;
    glGetActiveUniform(obj(program)->gl_handle, index, buffer_length, 0, &count, &type, buffer);
    auto location = glGetUniformLocation(obj(program)->gl_handle, buffer);
    return ShaderData{
        glType_to_skType(type), eastl::string(buffer), location, count
    };
}
ShaderData program_attribute_info(ProgramHandle program, int index){
    int buffer_length = getProgramiv((program), GL_ACTIVE_ATTRIBUTE_MAX_LENGTH);
    char* buffer = (char*)malloc(buffer_length*sizeof(char));
    GLint count;
    GLenum type;
    glGetActiveAttrib(obj(program)->gl_handle, index, buffer_length, 0, &count, &type, buffer);
    auto location = glGetAttribLocation(obj(program)->gl_handle, buffer);
    return ShaderData{
        glType_to_skType(type), eastl::string(buffer), location, count
    };
}

void set_uniform_float1(ProgramHandle program, int location, float value){
    bind_program(obj(program));
    glUniform1fv(location, 1, &value);
}
void set_uniform_float1v(ProgramHandle program, int location, eastl::span<float> value){
    bind_program(obj(program));
    glUniform1fv(location, value.size(), (const float*)value.data());
}

void set_uniform_float2(ProgramHandle program, int location, glm::vec2 value){
    bind_program(obj(program));
    glUniform2fv(location, 1, (const float*)&value);
}
void set_uniform_float2v(ProgramHandle program, int location, eastl::span<glm::vec2> value){
    bind_program(obj(program));
    glUniform2fv(location, value.size(), (const float*)value.data());
}

void set_uniform_float3(ProgramHandle program, int location, glm::vec3 value){
    bind_program(obj(program));
    glUniform3fv(location, 1, (const float*)&value);
}
void set_uniform_float3v(ProgramHandle program, int location, eastl::span<glm::vec3> value){
    bind_program(obj(program));
    glUniform3fv(location, value.size(), (const float*)value.data());
}

void set_uniform_float4(ProgramHandle program, int location, glm::vec4 value){
    bind_program(obj(program));
    glUniform4fv(location, 1, (const float*)&value);
}
void set_uniform_float4v(ProgramHandle program, int location, eastl::span<glm::vec4> value){
    bind_program(obj(program));
    glUniform4fv(location, value.size(), (const float*)value.data());
}

void set_uniform_float33(ProgramHandle program, int location, glm::mat3 value){
    bind_program(obj(program));
    glUniformMatrix3fv(location, 1, false, (const float*)&value);
}
void set_uniform_float33v(ProgramHandle program, int location, eastl::span<glm::mat3> value){
    bind_program(obj(program));
    glUniformMatrix3fv(location, value.size(), false, (const float*)value.data());
}

void set_uniform_float44(ProgramHandle program, int location, glm::mat4 value){
    bind_program(obj(program));
    glUniformMatrix4fv(location, 1, false, (const float*)&value);
}
void set_uniform_float44v(ProgramHandle program, int location, eastl::span<glm::mat4> value){
    bind_program(obj(program));
    glUniformMatrix4fv(location, value.size(), false, (const float*)value.data());
}

void set_uniform_int1(ProgramHandle program, int location, int value){
    bind_program(obj(program));
    glUniform1iv(location, 1, (const int*)&value);
}
void set_uniform_int1v(ProgramHandle program, int location, eastl::span<int> value){
    bind_program(obj(program));
    glUniform1iv(location, value.size(), (const int*)value.data());
}

void set_uniform_int2(ProgramHandle program, int location, glm::ivec2 value){
    bind_program(obj(program));
    glUniform2iv(location, 1, (const int*)&value);
}
void set_uniform_int2v(ProgramHandle program, int location, eastl::span<glm::ivec2> value){
    bind_program(obj(program));
    glUniform2iv(location, value.size(), (const int*)value.data());
}

void set_uniform_int3(ProgramHandle program, int location, glm::ivec3 value){
    bind_program(obj(program));
    glUniform3iv(location, 1, (const int*)&value);
}
void set_uniform_int3v(ProgramHandle program, int location, eastl::span<glm::ivec3> value){
    bind_program(obj(program));
    glUniform3iv(location, value.size(), (const int*)value.data());
}

void set_uniform_int4(ProgramHandle program, int location, glm::ivec4 value){
    bind_program(obj(program));
    glUniform4iv(location, 1, (const int*)&value);
}
void set_uniform_int4v(ProgramHandle program, int location, eastl::span<glm::ivec4> value){
    bind_program(obj(program));
    glUniform4iv(location, value.size(), (const int*)value.data());
}

void set_uniform_texture(ProgramHandle program, int location, TextureHandle tex){
    obj(program)->sampler_texture_map[location] = obj(tex);
}

void internal_set_uniform_sampler(ProgramHandle program, int location, int unit){
    auto sampler_unit = obj(program)->sampler_unit_map.find(location);
    if(sampler_unit == obj(program)->sampler_unit_map.end() || sampler_unit->second != unit){
        bind_program(obj(program));
        glUniform1iv(location, 1, &unit);
        obj(program)->sampler_unit_map[location] = unit;
    }
}
void set_uniform_sampler(ProgramHandle program, int location, int unit){
    obj(program)->sampler_texture_map.erase(location);
    internal_set_uniform_sampler(program, location, unit);
}

void draw_triangles(VertexArrayHandle vao, ProgramHandle program, FramebufferHandle out, DrawCommandSettings settings){
    bind_program(obj(program));

    eastl::map<GLuint, int> type_unit_map;
    for(auto [sampler, texture] : obj(program)->sampler_texture_map) {
        auto unit = type_unit_map[texture->type]++;
        bind_texture(texture->type, texture, unit);
        internal_set_uniform_sampler(program, sampler, unit);
    }

    if(obj(out)) internal_flush_framebuffer(out);
    bind_framebuffer(GL_DRAW_FRAMEBUFFER, obj(out));

    bind_vertex_array(obj(vao));

    if(settings.count == 0) settings.count = vertex_index_buffer_size(vao);
    switch (settings.culling) {
    break;case FaceCullingMode_CW:
        glFrontFace(GL_CW);
        glEnable(GL_CULL_FACE);
    break;case FaceCullingMode_CCW:
        glFrontFace(GL_CCW);
        glEnable(GL_CULL_FACE);
    break;case FaceCullingMode_None:
        glDisable(GL_CULL_FACE);
    }
    glDrawElements(GL_TRIANGLES, settings.count, GL_UNSIGNED_INT, (const void*)(long long)settings.first_index);
}

}