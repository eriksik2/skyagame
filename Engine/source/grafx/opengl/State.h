#pragma once

#include "Buffer_internal.h"
#include "Framebuffer_internal.h"
#include "Program_internal.h"
#include "Shader_internal.h"
#include "Texture_internal.h"
#include "VertexArray_internal.h"

namespace sk::grafxbase {
    VertexArrayObject*& vertex_array_binding();
    void bind_vertex_array(VertexArrayObject* array);

    ProgramObject*& program_binding();
    void bind_program(ProgramObject* program);

    BufferObject*& buffer_binding(unsigned int target);
    void bind_buffer(unsigned int target, BufferObject* buffer);

    TextureObject*& texture_binding(unsigned int target, int unit = 0);
    void bind_texture(unsigned int target, TextureObject* tex, int unit = 0);

    FramebufferObject*& framebuffer_binding(unsigned int target);
    void bind_framebuffer(unsigned int target, FramebufferObject* fb);
}

