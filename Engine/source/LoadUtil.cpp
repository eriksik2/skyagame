#include <skya/LoadUtil.h>

#include <SOIL.h>

#include <stdlib.h>
#include <stdio.h>


sk::grafxbase::Texture2DHandle load_texture2d(eastl::string_view path) {
    auto tex = sk::grafxbase::load_texture_2D(path);
    sk::grafxbase::set_texture_minmag_filter(tex, sk::grafxbase::TextureScaleType_Nearest);
    return tex;
    /*int width, height, channels;
    unsigned char *img = SOIL_load_image(path.data(), &width, &height, &channels, SOIL_LOAD_AUTO);
    if(img == 0)
    {
        printf("Couldn't load texture %s: %s\n", path.data(), SOIL_last_result());
        return {};
    }
    sk::grafxbase::TextureFormat format;
    sk::grafxbase::TextureChannel input_format = sk::grafxbase::TextureChannel_Red;
    switch(channels)
    {
    case 1:
        //format = GL_LUMINANCE;
        //break;
    case 2:
        assert(false && "not implemented");
        //format = GL_LUMINANCE_ALPHA;
        break;
    case 3:
        format = sk::grafxbase::TextureFormat_RGB;
        input_format = sk::grafxbase::TextureChannel_RGB;
        break;
    case 4:
        format = sk::grafxbase::TextureFormat_RGB;
        input_format = sk::grafxbase::TextureChannel_RGBA;
        break;
    }
    

    auto texture = sk::grafxbase::create_texture_2D();

    sk::grafxbase::allocate_texture(texture, format, {width, height});
    sk::grafxbase::modify_texture_raw(texture, 0, {0, 0}, {width, height}, input_format, img);
    sk::grafxbase::build_texture_mipmaps(texture);
    sk::grafxbase::set_texture_mag_filter(texture, sk::grafxbase::TextureScaleType_Nearest);

    SOIL_free_image_data(img);

    return texture;*/
}


sk::grafxbase::Texture2DHandle* load_adapter(const char* path){
    auto tex = load_texture2d(path);
    if(!tex) return nullptr;
    auto store = new sk::grafxbase::Texture2DHandle(tex);
    return store;
}
sk::grafxbase::Texture2DHandle* load_adapter_error() {
    return load_adapter("Content/Engine/err.png");
}

template<> sk::Content<sk::grafxbase::Texture2DHandle>::DefaultMethod sk::Content<sk::grafxbase::Texture2DHandle>::Default = &load_adapter;
template<> sk::Content<sk::grafxbase::Texture2DHandle>::ErrorMethod sk::Content<sk::grafxbase::Texture2DHandle>::Error = &load_adapter_error;

eastl::string sk::read_file(eastl::string_view path){
        FILE* file = fopen(path.data(), "r");
        if(!file) return nullptr;

        fseek(file, 0L, SEEK_END);
        long size = ftell(file);
        rewind(file);

        char* buffer = (char*)calloc(size, sizeof(char));
        char* line = (char*)calloc(size, sizeof(char));
        while(fgets(line, size, file) != nullptr) strcat(buffer, line);
        free(line);
        return {buffer};
    }
    sk::grafxbase::ShaderHandle sk::load_shader(eastl::string_view path, sk::grafxbase::ShaderType type){
        auto shader = sk::grafxbase::create_shader(type);
        sk::grafxbase::source_shader(shader, read_file(path));
        eastl::string err;
        if(!sk::grafxbase::compile_shader(shader, &err)){
            printf("failed to compile shader %s: %s", path.data(), err.data());
            return {};
        }
        return shader;
    }
    sk::grafxbase::ProgramHandle sk::load_shader_program(eastl::string_view vertexpath, eastl::string_view fragmentpath){
        auto prog = sk::grafxbase::create_program();

        auto vert = sk::load_shader(vertexpath, sk::grafxbase::ShaderType_Vertex);
        if(!vert) return {};

        auto frag = sk::load_shader(fragmentpath, sk::grafxbase::ShaderType_Fragment);
        if(!frag) {
            sk::grafxbase::delete_shader(vert);
            return {};
        }
        sk::grafxbase::bind_program_shader(prog, vert);
        sk::grafxbase::bind_program_shader(prog, frag);
        
        sk::grafxbase::link_program(prog);
        eastl::string err;
        if(!sk::grafxbase::link_program(prog, &err)){
            printf("failed to link shader program: %s", err.data());
            sk::grafxbase::delete_program(prog);
            prog = {};
        }
        sk::grafxbase::delete_shader(frag);
        sk::grafxbase::delete_shader(vert);
        return prog;
    }