#include "EASTL/string_view.h"
#include <skya/Profiling.h>

#include <skya/imgui/imgui.h>
#include <string>

namespace sk::perf {
    Profiler MainProfiler{};

    time_point<steady_clock, microseconds> now() {
        return time_point_cast<microseconds>(steady_clock::now());
    }

    
    time_point<steady_clock, microseconds> Timespan::start_time() const { return m_start; }
    time_point<steady_clock, microseconds> Timespan::end_time() const { return m_end; }
    microseconds Timespan::duration() const {
        auto start = start_time();
        auto end = end_time();
        if(end < start) end = now();
        return duration_cast<microseconds>(end - start);
    }

    void Timer::start() { m_start = now(); }
    void Timer::end() { m_end = now(); }



    ScopeTimer::ScopeTimer(eastl::string_view name, Profiler& profiler) : m_name(name), m_profiler(profiler) {
        m_timer.start();
        if(!m_profiler.do_capture) return;
        m_index = m_profiler.results.size();
        m_profiler.results.push_back({m_name, Timespan(), m_profiler.calldepth++});
    }
    ScopeTimer::~ScopeTimer() {
        if(!m_profiler.do_capture) return;
        m_profiler.calldepth--;
        m_timer.end();
        m_profiler.results[m_index].span = m_timer;
    }

    Profiler::Profiler(){
        Profiler::start_frame();
    }
    void Profiler::start_frame() {
        if(!do_capture) return;
        int size = results.size();
        results.clear();
        results.set_capacity(size + 50);
        static eastl::string current_frame;
        current_frame = "Frame " + eastl::string(std::to_string(frameno++).data());
        results.push_back({current_frame, Timespan(), 0});
        calldepth = 1;
        frametime.start();
    }
    void Profiler::end_frame() {
        if(!do_capture) return;
        frametime.end();
        results[0].span = frametime;
    }

    void Profiler::draw_imgui() {
        using namespace ImGui;

        Begin("Performance");

        static float a = 0, b = 2; static int c = 0;
        BeginTimeline("timeline0");
            TimelineItem("name.data()", &a, &b, &c);
            TimelineItem("_Stuck", -.9, -.1, 3);
        EndTimeline();

        float frametime_ms = frametime.duration().count()*0.001f;
        Text("Frametime: %.3f ms", frametime_ms);

        bool active_capture_changed = Checkbox("Active capture", &do_capture);

        BeginTimeline("timeline");
        if(active_capture_changed)
            ;//SetTimelineZoom(0, frametime_ms);

        for(auto& res : results){
            eastl::string_view name = res.function;
            float start_ms = (res.span.start_time() - frametime.start_time()).count()*0.001f;
            float end_ms = (res.span.end_time() - frametime.start_time()).count()*0.001f;
            float duration_ms = res.span.duration().count()*0.001f;
            if(duration_ms == 0) continue;

            TimelineItem(name.data(), start_ms, end_ms, res.calldepth);

            bool item_hover = IsItemHovered();
            bool item_dbclick = item_hover && IsMouseDoubleClicked(0);
            if(item_dbclick){
                SetTimelineZoom(start_ms, end_ms, 150);
            } else if(item_hover) {
                PushStyleColor(ImGuiCol_PopupBg, {255, 255, 255, 255});
                PushStyleColor(ImGuiCol_Text, {0, 0, 0, 255});
                BeginTooltip();
                float time_per = duration_ms/frametime_ms*100.f;
                Text("%s", name.data());
                Text("    Duration:   %.3f ms (%.2f%%)", duration_ms, time_per);
                Text("    Called:        %.3f ms into frame", start_ms);
                Text("    Returned:  %.3f ms into frame", end_ms);
                EndTooltip();
                PopStyleColor(2);
            }
        }

        EndTimeline();
        End();
    }
}