#include <skya/CMesh.h>

#include <skya/MeshObject.h>
#include <skya/Game.h>
#include <skya/CCamera.h>
#include <skya/CTransform.h>

#include <skya/LoadUtil.h>


sk::CMesh::~CMesh() {
    SetShader({});
}


void sk::CMesh::OnLoad() {
    if(!m_camera.has_value()) m_camera = GetGame().default_camera;
    if(!Texture) SetTexture(*sk::Content<sk::grafxbase::Texture2DHandle>::Load("Content/Texture/opentksquare.png"));
    if(!Shader) {
        // should not create a new ProgramObject for every CMesh
        auto program = sk::load_shader_program("Content/Shader/default.vert", "Content/Shader/default.frag");
        SetShader(program);
    }
    if(m_Mesh == nullptr) SetMesh(sk::Content<sk::MeshObject>::Load("Content/Mesh/cow.obj"));
}

void sk::CMesh::OnRender() {
    if(!m_camera.has_value()) return;
    auto proj = m_camera->GetProjectionMatrix();
    auto view = m_camera->template Get<sk::CTransform>()->GetViewMatrix();
    auto modl = this->template Get<sk::CTransform>()->GetMatrix();
    auto final_matrix = proj * view * modl;

    for(int i = 0; i < sk::grafxbase::program_uniform_count(Shader); ++i){
        auto uniform = sk::grafxbase::program_uniform_info(Shader, i);
        
        if(uniform.name == "modelview") sk::grafxbase::set_uniform_float44(Shader, uniform.location, final_matrix);
    }

    m_Mesh->Draw(Shader, Framebuffer);
}

void sk::CMesh::SetFramebuffer(sk::grafxbase::FramebufferHandle fb){
    Framebuffer = fb;
}


void sk::CMesh::SetMesh(sk::MeshObject * mesh) {
    m_Mesh = mesh;
    auto attributes = sk::grafxbase::program_attribute_count(Shader);
    for(int i = 0; i < attributes; ++i){
        auto attr = sk::grafxbase::program_attribute_info(Shader, i);
        
        sk::grafxbase::enable_vertex_attribute(m_Mesh->VAO, i);
        if(attr.name == "vPosition") m_Mesh->PipePositionToAttribute(attr.location);
        else if(attr.name == "vNormal") m_Mesh->PipeNormalToAttribute(attr.location);
        else if(attr.name == "texcoord") m_Mesh->PipeUVToAttribute(attr.location);
        else if(attr.name == "vColor") m_Mesh->PipeColorToAttribute(attr.location);
    }
}

void sk::CMesh::SetTexture(sk::grafxbase::TextureHandle tex) {
    Texture = tex;
    if(Shader) for(int i = 0; i < sk::grafxbase::program_uniform_count(Shader); ++i){
        auto uniform = sk::grafxbase::program_uniform_info(Shader, i);
        
        if(uniform.name == "maintexture") sk::grafxbase::set_uniform_texture(Shader, uniform.location, Texture);
    }
}

void sk::CMesh::SetShader(sk::grafxbase::ProgramHandle program) {
    if(Shader) sk::grafxbase::delete_program(Shader);
    Shader = program;
    if(Shader) for(int i = 0; i < sk::grafxbase::program_uniform_count(Shader); ++i){
        auto uniform = sk::grafxbase::program_uniform_info(Shader, i);
        
        if(uniform.name == "maintexture") sk::grafxbase::set_uniform_texture(Shader, uniform.location, Texture);
    }
}
