#include <skya/CCamera.h>

#include <skya/Delegate.h>

#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>

glm::mat4 sk::CCamera::GetProjectionMatrix() const {
    return glm::perspective(VerticalFov, AspectRatio, NearZ, FarZ);
}

void sk::CCamera::OnStart() {
    GetGame().window.OnFramebufferSizeCallback += DELEGATE(&CCamera::OnFramebufferSizeEvent, this);
}

void sk::CCamera::OnFramebufferSizeEvent(sk::FramebufferSizeEventArgs e) {
    AspectRatio = (float)e.width / (float)e.height;
}
