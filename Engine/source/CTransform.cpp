#include "glm/gtx/quaternion.hpp"
#include <skya/CTransform.h>

#include <glm/gtc/matrix_transform.hpp>


glm::vec3 sk::CTransform::WorldForward = glm::vec3(0, 0, 1);
glm::vec3 sk::CTransform::WorldUp = glm::vec3(0, 1, 0);
glm::vec3 sk::CTransform::WorldLeft = glm::vec3(1, 0, 0);


const glm::vec3 & sk::CTransform::GetPosition() const {
    return m_Position;
}

glm::vec3 & sk::CTransform::GetPosition() {
    b_ViewMatrixNeedUpdate = true;
    b_MatrixNeedUpdate = true;
    return m_Position;
}

const glm::vec3 & sk::CTransform::GetScale() const {
    return m_Scale;
}

glm::vec3 & sk::CTransform::GetScale() {
    b_MatrixNeedUpdate = true;
    return m_Scale;
}

const glm::tquat<float>& sk::CTransform::GetRotation() const {
    return m_Rotation;
}

glm::tquat<float>& sk::CTransform::GetRotation() {
    b_ViewMatrixNeedUpdate = true;
    b_MatrixNeedUpdate = true;
    return m_Rotation;
}

const glm::mat4& sk::CTransform::GetMatrix() {
    m_Matrix = glm::translate(glm::mat4(1), m_Position) * glm::toMat4(m_Rotation) * glm::scale(glm::mat4(1), m_Scale);
    if(b_MatrixNeedUpdate)
    {
        b_MatrixNeedUpdate = false;
    }
    return m_Matrix;
}

const glm::mat4& sk::CTransform::GetViewMatrix() {
    m_ViewMatrix = glm::lookAt(m_Position, m_Position + Forward(), WorldUp);
    if(b_ViewMatrixNeedUpdate)
    {
        b_ViewMatrixNeedUpdate = false;
    }
    return m_ViewMatrix;
}

glm::vec3 sk::CTransform::EulerRotation() const {
    return glm::eulerAngles(m_Rotation);
}

glm::vec3 sk::CTransform::Forward() const {
    if(m_Rotation == decltype(m_Rotation)()) return WorldForward;
    return glm::inverse(m_Rotation) * WorldForward;
    //auto euler = EulerRotation();
    //return glm::vec3((float)(sin(euler.x) * cos(euler.y)), (float)sin(euler.y), (float)(cos(euler.x) * cos(euler.y)));
}

glm::vec3 sk::CTransform::Up() const {
    return glm::inverse(m_Rotation) * WorldUp;
}

glm::vec3 sk::CTransform::Left() const {
    return glm::inverse(m_Rotation) * WorldLeft;
}
