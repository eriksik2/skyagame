#include <skya/imgui/Widgets.h>

#include <imgui.h>

#include <fmt/printf.h>

#include <EASTL/map.h>
#include <EASTL/vector.h>
#include <EASTL/algorithm.h>


#include <chrono>
using namespace std::chrono;
template<class T>
struct Lerpable {
    constexpr Lerpable(T v) : m_target(v), m_old(v) {}
    template<class D>
    void lerp(T v, D duration) {
        m_old = T(*this);
        m_target = v;
        m_lerp_duration = duration_cast<microseconds>(duration);
        m_set_time = time_point_cast<microseconds>(steady_clock::now());
    }
    Lerpable& operator=(T v) {
        m_target = v;
        m_lerp_duration = microseconds::zero();
        return *this;
    }
    operator T() {
        float lerp = get_elapsed();
        constexpr auto func = [](float l) { return pow(l, 2); };
        lerp = (1-func(1-lerp))*lerp + func(lerp)*(1-lerp);
        return m_old*(1-lerp) + m_target*lerp;
    }
    Lerpable& operator+=(T b) { *this = T(*this) + b; return *this; }
    Lerpable& operator-=(T b) { *this = T(*this) - b; return *this; }
    Lerpable& operator*=(T b) { *this = T(*this) * b; return *this; }
    Lerpable& operator/=(T b) { *this = T(*this) / b; return *this; }

private:
    float get_elapsed(){
        if(m_lerp_duration == microseconds::zero()) return 1;
        auto time_now = time_point_cast<microseconds>(steady_clock::now());
        auto time_target = m_set_time + m_lerp_duration;
        if(time_now >= time_target) {
            m_lerp_duration = microseconds::zero();
            return 1;
        }
        return (time_now - m_set_time).count()/(float)m_lerp_duration.count();
    }
    T m_target;
    T m_old;
    time_point<steady_clock, microseconds> m_set_time;
    microseconds m_lerp_duration;
};

template<class T> T operator+(Lerpable<T> a, T b) { return T(a) + b; }
template<class T> T operator+(T a, Lerpable<T> b) { return a + T(b); }
template<class T> T operator-(Lerpable<T> a, T b) { return T(a) - b; }
template<class T> T operator-(T a, Lerpable<T> b) { return a - T(b); }
template<class T> T operator*(Lerpable<T> a, T b) { return T(a) * b; }
template<class T> T operator*(T a, Lerpable<T> b) { return a * T(b); }
template<class T> T operator/(Lerpable<T> a, T b) { return T(a) / b; }
template<class T> T operator/(T a, Lerpable<T> b) { return a / T(b); }


struct TimelineState {
    ImGuiTimelineFlags flags = ImGuiTimelineFlags_None;
    ImGuiID ID = 0;
    int header_height = 0;
    int tracks = 0;
    int auto_track_counter = 0;
    Lerpable<float> x_offset = 0;// px
    Lerpable<float> zoom_scale = 100;// 1ms == 100px
    ImVec2 min_bound = {0,0};
    ImVec2 max_bound = {0,0};

    static constexpr auto col_border = IM_COL32(85, 85, 85, 255);
    static constexpr auto col_item_bg = IM_COL32(0, 0, 0, 255);
    static constexpr auto col_item_hover_bg = IM_COL32(45, 45, 45, 255);
    static constexpr auto col_header_bg = IM_COL32(45, 45, 45, 255);
    static constexpr auto col_interval_indicator = IM_COL32(55, 55, 55, 255);
    static constexpr auto col_interval_indicator_text = IM_COL32(115, 115, 115, 255);
    static constexpr auto col_mouse_indicator = IM_COL32(115, 115, 115, 255);
    static constexpr auto col_mouse_indicator_text = IM_COL32(205, 205, 205, 255);
    static constexpr auto col_auxiliary_indicator_text = IM_COL32(205, 205, 205, 255);

    void ZoomToRange(float min, float max) {
        zoom_scale = (max_bound.x - min_bound.x)/(max - min);
        x_offset = min*zoom_scale;
    }

    void SmoothZoomToRange(float min, float max, int ms) {
        float new_zoom_scale = (max_bound.x - min_bound.x)/(max - min);
        zoom_scale.lerp(new_zoom_scale, milliseconds(ms));
        x_offset.lerp(min*new_zoom_scale, milliseconds(ms));
    }

    float LocalToTimepoint(float px) const { return (px + x_offset)/zoom_scale; }
    float GlobalToTimepoint(float gpx) const { return LocalToTimepoint(gpx - min_bound.x); }
    int TimepointToLocal(float x) const { return x*zoom_scale - x_offset; }
    int TimepointToGlobal(float x) const { return TimepointToLocal(x) + min_bound.x; }

    float ClampLocal(float x) const { return eastl::min(max_bound.x - min_bound.x, eastl::max(0, x)); }

    float TrackHeight() const { return 50; }
    float ContentHeight() const { return TrackHeight()*tracks + header_height; }

    int LocalToTrack(float py) const { return eastl::min(eastl::max(0, tracks-1), eastl::max(0, int((py - header_height)/TrackHeight()))); }
    int GlobalToTrack(float gpy) const { return LocalToTrack(gpy - min_bound.y); }

};
static eastl::map<ImGuiID, TimelineState> g_TimelineStateMap;
static eastl::vector<ImGuiID> g_TimelineID;
static unsigned g_TimelineItemID = 0;

namespace ImGui {

    void SetTimelineZoom(float start, float end, int ms){
        ImGuiID ID = g_TimelineID.back();
        TimelineState& state = g_TimelineStateMap[ID];
        if(ms) state.SmoothZoomToRange(start, end, ms);
        else state.ZoomToRange(start, end);
    }


    void DrawTimelineIndicator(const TimelineState& state, float xpx, ImU32 color, bool draw_text, const char* str){
        auto min = state.min_bound;
        auto max = state.max_bound;
        int draw_x = xpx;
        auto& draw = *GetWindowDrawList();
        if(draw_x > 0 && min.x + draw_x < max.x) draw.AddLine({min.x + draw_x, min.y}, {min.x + draw_x, max.y}, color, 1);
        if(!draw_text) return;
        auto text_width = CalcTextSize(str).x/* + padding*/;
        if(draw_x + text_width <= 0 || min.x + draw_x - text_width >= max.x) return;
        if(min.x + draw_x + text_width > max.x) SetCursorScreenPos({min.x + draw_x - text_width, min.y});
        else SetCursorScreenPos({min.x + draw_x, min.y});
        TextUnformatted(str);
    }

    void TimelineBehaviour(TimelineState& state){
        auto mouse_gpos = GetMousePos(); // global position
        auto mouse_lpos = ImVec2{mouse_gpos.x - state.min_bound.x, mouse_gpos.y - state.min_bound.y}; // local position

        if((state.flags & ImGuiTimelineFlags_AutoTracks) == state.flags){
            state.tracks = state.auto_track_counter;
            state.auto_track_counter = 1;
        }

        g_TimelineItemID = 0;

        if(IsWindowHovered()){
            if(IsMouseDragging(0)){
                state.x_offset -= GetMouseDragDelta().x;
                ResetMouseDragDelta();
            }

            float scrolldelta = GetIO().MouseWheel;
            if(scrolldelta != 0.0f){
                const float factor = 1.1f;
                float new_scale = state.zoom_scale;
                if(scrolldelta > 0) new_scale *= factor;
                else if(scrolldelta < 0) new_scale *= 1/factor;
                
                float delta = new_scale / state.zoom_scale;
                float anchor = mouse_lpos.x;
                state.x_offset = (state.x_offset + anchor)*delta - anchor;
                state.zoom_scale = new_scale;
            }
        }
    }

    void BeginTimeline(const char* label, ImVec2 size_arg, int tracks, const char* indicator_format){
        IM_ASSERT(label);
        ImGui::PushID(label);
        ImGuiID ID = ImGui::GetID("");
        g_TimelineID.push_back(ID);
        TimelineState& state = g_TimelineStateMap[ID];

        state.header_height = GetTextLineHeightWithSpacing() + 5;
        if(tracks == 0){
            if((state.flags & ImGuiTimelineFlags_AutoTracks) == 0) state.tracks = 1;
            state.flags |= ImGuiTimelineFlags_AutoTracks;
        }
        else {
            state.flags &=~ ImGuiTimelineFlags_AutoTracks;
            state.tracks = tracks;
        }

        PushStyleVar(ImGuiStyleVar_WindowPadding, {0, 0});
        ImGui::BeginChild("timeline", {size_arg.x, state.ContentHeight()}, true, ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoMove);
        auto win_size = GetWindowSize();//wind->WorkRect.GetSize();
        auto win_gpos = GetWindowPos(); // global position / min bound
        auto win_gmax = ImVec2{win_gpos.x + win_size.x, win_gpos.y + win_size.y}; // max bound
        state.min_bound = win_gpos;
        state.max_bound = win_gmax;
        auto mouse_gpos = GetMousePos(); // global position
        auto mouse_lpos = ImVec2{mouse_gpos.x - win_gpos.x, mouse_gpos.y - win_gpos.y}; // local position

        bool timeline_hover = IsWindowHovered();

        auto& draw = *GetWindowDrawList();
        
        draw.AddRectFilled(win_gpos, {win_gmax.x, win_gpos.y + state.header_height}, TimelineState::col_header_bg);
        draw.AddRect(win_gpos, {win_gmax.x, win_gpos.y + state.header_height}, TimelineState::col_border);

        for(int i = 1; i < state.tracks; ++i){
            float y = win_gpos.y + state.header_height + state.TrackHeight()*i;
            draw.AddLine({win_gpos.x, y}, {win_gmax.x, y}, TimelineState::col_border);
        }

        PushStyleColor(ImGuiCol_Text, TimelineState::col_interval_indicator_text);
        float interval_px = 0.001f*state.zoom_scale;
        while(interval_px < 75) interval_px*=5;
        if(interval_px > 1.f) for(float x = -fmod(state.x_offset, interval_px); x < win_size.x; x += interval_px){
            auto str = fmt::sprintf(indicator_format, state.LocalToTimepoint(x));
            auto text_width = CalcTextSize(str.c_str()).x/*+ padding*/;
            bool draw_text = x < win_size.x - text_width*2 && x > text_width;
            if(timeline_hover) draw_text = draw_text && (x < mouse_lpos.x - text_width || x > mouse_lpos.x + text_width);
            DrawTimelineIndicator(state, x, TimelineState::col_interval_indicator, draw_text, str.c_str());
        }
        auto start_str = fmt::sprintf(indicator_format, state.LocalToTimepoint(0));
        float start_str_width = CalcTextSize(start_str.c_str()).x/*+ padding*/;
        DrawTimelineIndicator(state, 0, TimelineState::col_mouse_indicator, !timeline_hover || mouse_lpos.x > start_str_width, start_str.c_str());
        auto end_str = fmt::sprintf(indicator_format, state.LocalToTimepoint(win_size.x));
        float end_str_width = CalcTextSize(end_str.c_str()).x/*+ padding*/;
        DrawTimelineIndicator(state, win_size.x, TimelineState::col_mouse_indicator, !timeline_hover || mouse_lpos.x < win_size.x - end_str_width*2, end_str.c_str());

        if(timeline_hover) {
            PushStyleColor(ImGuiCol_Text, TimelineState::col_mouse_indicator_text);
            auto mouse_str = fmt::sprintf(indicator_format, state.LocalToTimepoint(mouse_lpos.x));
            DrawTimelineIndicator(state, mouse_lpos.x, TimelineState::col_mouse_indicator, true, mouse_str.c_str());
            PopStyleColor();
        }

        PopStyleColor();

        TimelineBehaviour(state);
    }

    void TimelineItemBehaviour(TimelineState& state, float* start, float* end, int* track, ImVec2 item_min, ImVec2 item_max, ImGuiTimelineFlags flags) {
        float edge_grab = 7.5f;
        static float start_grab_offset = 0, end_grab_offset = 0; // Used when mouse dragging items
        auto mouse_gpos = GetMousePos();
        float mouse_lpos = state.GlobalToTimepoint(mouse_gpos.x);


        PushID(++g_TimelineItemID);

        bool enable_resizing = (flags & ImGuiTimelineFlags_NoResize) == 0;
        bool active_left = false, active_right = false;
        if(enable_resizing) {
            SetCursorScreenPos(item_min);
            InvisibleButton("left", {edge_grab, item_max.y - item_min.y});
            bool hover_left = IsItemHovered();
            active_left = IsItemActive();

            SetCursorScreenPos({item_max.x - edge_grab, item_min.y});
            InvisibleButton("right", {edge_grab, item_max.y - item_min.y});
            bool hover_right = IsItemHovered();
            active_right = IsItemActive();
            
            if(hover_left || hover_right) SetMouseCursor(ImGuiMouseCursor_ResizeEW);
            else  if(active_left || active_right){
                SetMouseCursor(ImGuiMouseCursor_None);
                float min_width = edge_grab;
                float* point = active_left ? start : end;
                auto str = fmt::sprintf("%.3f", mouse_lpos);
                DrawTimelineIndicator(state, mouse_gpos.x - state.min_bound.x, TimelineState::col_mouse_indicator, true, str.c_str());
                if((active_left ? *end - mouse_lpos : mouse_lpos - *start) >= min_width/state.zoom_scale)
                    *point = mouse_lpos;
            }
        }

        bool enable_move_x = (flags & ImGuiTimelineFlags_NoHorizontalMove) == 0;
        bool enable_move_y = (flags & ImGuiTimelineFlags_NoVerticalMove) == 0;
        
        SetCursorScreenPos({item_min.x + edge_grab, item_min.y});
        float mid_width = item_max.x - item_min.x - edge_grab*2;
        bool hover_mid, clicked_mid, active_mid; hover_mid = clicked_mid = active_mid = false;
        if(mid_width > 0 && (enable_move_x || enable_move_y)){
            InvisibleButton("mid", {item_max.x - item_min.x - edge_grab*2, item_max.y - item_min.y});
            hover_mid = IsItemHovered();
            clicked_mid = IsItemClicked();
            active_mid = IsItemActive();
        }
        if(hover_mid && (enable_move_x || enable_move_y)) SetMouseCursor(ImGuiMouseCursor_Hand);
        if(clicked_mid && enable_move_x) {
            start_grab_offset = *start - mouse_lpos;
            end_grab_offset = *end - mouse_lpos;
        }
        if(active_mid){
            if(enable_move_y) *track = state.GlobalToTrack(mouse_gpos.y);
            if(enable_move_x) {
                float new_start = mouse_lpos + start_grab_offset;
                float new_end = mouse_lpos + end_grab_offset;
                auto str_start = fmt::sprintf("%.3f", new_start);
                auto str_end = fmt::sprintf("%.3f", new_end);
                DrawTimelineIndicator(state, state.ClampLocal(state.TimepointToLocal(new_start)), TimelineState::col_mouse_indicator, true, str_start.c_str());
                DrawTimelineIndicator(state, state.ClampLocal(state.TimepointToLocal(new_end)), TimelineState::col_mouse_indicator, true, str_end.c_str());
                *start = new_start;
                *end = new_end;
            }
        }

        bool auto_tracks = (state.flags & ImGuiTimelineFlags_AutoTracks) == state.flags;
        if(auto_tracks && *track + 1 >= state.auto_track_counter) state.auto_track_counter = *track + 1
            + (active_mid && enable_move_y ? 1 : 0);

        SetCursorScreenPos(item_min);
        if(!(active_right || active_left || active_mid || clicked_mid))
            InvisibleButton("", {item_max.x - item_min.x, item_max.y - item_min.y});

        PopID();
    }

    void TimelineItem(const char* label, float* start, float* end, int* track, ImGuiTimelineFlags flags){
        int dummy=0;
        if(track==nullptr) {
            flags |= ImGuiTimelineFlags_NoVerticalMove;
            track = &dummy;
        }
        ImGuiID ID = g_TimelineID.back();
        TimelineState& state = g_TimelineStateMap[ID];
        auto& draw = *GetWindowDrawList();

        auto& style = GetStyle();
        const auto padding = style.FramePadding.x;

        auto min_bound = ImVec2{(float)state.TimepointToGlobal(*start), state.min_bound.y + state.header_height + *track*state.TrackHeight()};
        auto max_bound = ImVec2{(float)state.TimepointToGlobal(*end), min_bound.y + state.TrackHeight()};
        if(max_bound.y - min_bound.y <= 0.0f) return;
        if(max_bound.x - min_bound.x <= 0.0f) return;
        bool item_hover = false;//timeline_hover && IsMouseHoveringRect(min_bound, max_bound, true);
        draw.AddRectFilled(min_bound, max_bound, item_hover ? TimelineState::col_item_hover_bg : TimelineState::col_item_bg);
        draw.AddRect(min_bound, max_bound, TimelineState::col_border);
        PushClipRect({min_bound.x + 1, min_bound.y + 1}, {max_bound.x - 1, max_bound.y - 1}, true);
        if(min_bound.x < state.min_bound.x) { // outside timeline
            float text_width = CalcTextSize(label).x + padding;
            float text_x = state.min_bound.x + padding;
            if(text_x + text_width > max_bound.x) text_x = max_bound.x - text_width;
            SetCursorScreenPos({text_x, min_bound.y});
        } else {
            SetCursorScreenPos({min_bound.x + padding, min_bound.y});
        }
        TextUnformatted(label);
        PopClipRect();
        
        TimelineItemBehaviour(state, start, end, track, min_bound, max_bound, flags);
    }

    void EndTimeline(){
        ImGuiID ID = g_TimelineID.back();
        g_TimelineID.pop_back();
        TimelineState& state = g_TimelineStateMap[ID];

        


        ImGui::EndChild();
        ImGui::PopID();
        ImGui::PopStyleVar();
    }
}