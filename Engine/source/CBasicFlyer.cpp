#include <skya/CBasicFlyer.h>
#include <skya/Game.h>
#include <skya/Delegate.h>
#include <skya/Controller.h>
#include <skya/Window.h>
#include <skya/Enums.h>
#include <skya/CTransform.h>
#include <skya/CPhysics.h>

#include <glm/glm.hpp>
#define _USE_MATH_DEFINES
#include <math.h>


void sk::CBasicFlyer::OnStart() {
    GetGame().window.OnKeyCallback += DELEGATE(&CBasicFlyer::OnKeyEvent, this);
    GetGame().window.OnMouseButtonCallback += DELEGATE(&CBasicFlyer::OnMouseButtonEvent, this);
    GetGame().window.OnCursorPosCallback += DELEGATE(&CBasicFlyer::OnMouseMoveEvent, this);
}



void sk::CBasicFlyer::OnKeyEvent(sk::KeyEventArgs e) {
    if(e.action == Keyboard::Action::PRESS)
        switch(e.key)
        {
        case Keyboard::Key::W: fwd = 1; break;
        case Keyboard::Key::A: left = 1; break;
        case Keyboard::Key::S: fwd = -1; break;
        case Keyboard::Key::D: left = -1; break;
        default: break;
        }
    else if(e.action == Keyboard::Action::RELEASE)
        switch(e.key)
        {
        case Keyboard::Key::W: fwd = 0; break;
        case Keyboard::Key::A: left = 0; break;
        case Keyboard::Key::S: fwd = 0; break;
        case Keyboard::Key::D: left = 0; break;
        case Keyboard::Key::ESCAPE:
            GetGame().window.SetCursorMode(CursorMode_Normal);
            b_focused = false;
            break;
        default: break;
        }
}

void sk::CBasicFlyer::OnMouseButtonEvent(sk::MouseButtonEventArgs e) {
    if(e.action == Mouse::Action::PRESS && e.button == Mouse::Button::LEFT)
    {
        b_focused = true;
        GetGame().window.SetCursorMode(CursorMode_Disabled);
    }
}

void sk::CBasicFlyer::OnMouseMoveEvent(sk::CursorPosEventArgs e) {
    if(b_focused)
    {
        rotx += (float(e.X) - mousex)*.01f;
        roty += (float(e.Y) - mousey)*.01f;
    }
    mousex = float(e.X);
    mousey = float(e.Y);
}

void sk::CBasicFlyer::OnUpdate(double dt) {
    if(!this->template HasComponent<sk::CTransform>()) return;
    if(!this->template HasComponent<sk::CPhysics>()) return;
    //auto&& controller = this->GetGame().controller;
    //auto cont_device = false;
    //float movex = 0, movey = 0;
    //
    //cont_device = controller.HasDevice();
    //
    //if(cont_device)
    //{
    //    controller.ActivateActionSet("flying");
    //    auto look = controller.GetAnalogActionData("look");
    //    rotx += look.x*.01f;
    //    roty += look.y*.01f;
    //
    //    auto move = controller.GetAnalogActionData("move");
    //    movex = -move.x;
    //    movey = move.y;
    //}
    //else controller.SetDevice(*Controller::GetConnectedControllers().begin());

    if(roty > float(M_PI_2) - .001f)
    {
        //if(cont_device) controller.StopAnalogActionMomentum("look");
        roty = float(M_PI_2) - .001f;
    }
    else if(roty < -float(M_PI_2) + .001f)
    {
        //if(cont_device) controller.StopAnalogActionMomentum("look");
        roty = -float(M_PI_2) + .001f;
    }

    sk::CTransform& transform = *this->template Get<sk::CTransform>();

    transform.GetRotation() = glm::angleAxis(rotx, transform.WorldUp);
    transform.GetRotation() *= glm::angleAxis(-roty, transform.Left());

    auto fwdaccel = transform.Forward()*(float)(fwd);
    auto leftaccel = transform.Left()*(float)(left);

    this->template Get<sk::CPhysics>()->AddAcceleration((fwdaccel + leftaccel) * m_AccelSpeed);
}
