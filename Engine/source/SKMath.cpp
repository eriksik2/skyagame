#include <skya/SKMath.h>

#include <math.h>

#include <EASTL/numeric_limits.h>
#include <EASTL/algorithm.h>


#include <glm/vec3.hpp>
#include <glm/gtx/norm.hpp>

using namespace sk;

glm::vec3 Ray::Inverse() {
    if(eastl::numeric_limits<float>::is_iec559) return glm::vec3(1)/Dir;
    else return glm::vec3(1/(Dir.x + .0000001f), 1/(Dir.y + .0000001f), 1/(Dir.z + .0000001f));
}

glm::vec3 AABB::Normal(glm::vec3 p) {
    glm::vec3 v;
    for(int i = 0; i < 3; ++i)
    {
        //v[i] = cen[i] > p[i] ? -1.0f : 1.0f;
        v[i] = (p[i] > Max[i]) ? 1.0f : ((p[i] < Min[i]) ? -1.0f : 0);
    }
    return v;
}
glm::vec3 AABB::Perimeter(glm::vec3 p) {
    glm::vec3 v;
    for(int i = 0; i < 3; ++i)
    {
        //if(p[i] < cen[i]) v[i] = Min[i];
        //if(p[i] > cen[i]) v[i] = Max[i];
        if(p[i] < Min[i]) p[i] = Min[i];
        if(p[i] > Max[i]) p[i] = Max[i];
    }
    return p;
}

bool sk::Intersection(Ray ray, AABB box) {
    glm::vec3 inv = ray.Inverse();

    float vmin = (box.Min.x - ray.Origin.x)*inv.x;
    float vmax = (box.Max.x - ray.Origin.x)*inv.x;

    float tmin = eastl::min(vmin, vmax);
    float tmax = eastl::max(vmin, vmax);

    vmin = (box.Min.y - ray.Origin.y)*inv.y;
    vmax = (box.Max.y - ray.Origin.y)*inv.y;

    tmin = eastl::max(tmin, eastl::min(vmin, vmax));
    tmax = eastl::min(tmax, eastl::max(vmin, vmax));

    vmin = (box.Min.z - ray.Origin.z)*inv.z;
    vmax = (box.Max.z - ray.Origin.z)*inv.z;

    tmin = eastl::max(tmin, eastl::min(vmin, vmax));
    tmax = eastl::min(tmax, eastl::max(vmin, vmax));

    return tmax > tmin;
}


bool sk::Intersection(AABB box1, AABB box2) {
    bool t[3];
    for(int i = 0; i < 3; ++i)
        t[i] = box1.Max[i] > box2.Min[i] && box1.Min[i] < box2.Max[i];
    return t[0] && t[1] && t[2];
}

bool sk::Intersection(AABB box, Sphere sphere) {
    return Distance(box, sphere) < 0;
}

bool sk::Intersection(AABB box, Cylinder cylinder) {
    return Distance(box, cylinder) < 0;
}

bool sk::Intersection(AABB box, FlatCylinder cylinder) {
    return Distance(box, cylinder) < 0;
}

bool sk::Intersection(Sphere sphere1, Sphere sphere2) {
    return Distance(sphere1, sphere2) < 0;
}

glm::vec3 sk::IntersectionNormal(AABB box1, AABB box2) {
    return box1.Normal(box2.Perimeter((box1.Min + box1.Max) / 2.f));
}


float sk::Distance2(Sphere sphere1, Sphere sphere2) {
    return Distance2(sphere1.Origin, sphere2.Origin) - sphere1.Radius*sphere1.Radius - sphere2.Radius*sphere2.Radius;
}

float sk::Distance2(Sphere sphere1, glm::vec3 point) {
    return Distance2(sphere1.Origin, point) - sphere1.Radius*sphere1.Radius;
}

float sk::Distance2(AABB box, Sphere sphere) {
    return Distance2(box, sphere.Origin) - sphere.Radius*sphere.Radius;
}

float sk::Distance2(AABB box, glm::vec3 point) {
    float dist2 = 0;
    for(int i = 0; i < 3; ++i)
    {
        if(point[i] < box.Min[i]) dist2 += std::pow(box.Min[i] - point[i], 2);
        else if(point[i] > box.Max[i]) dist2 += std::pow(box.Max[i] - point[i], 2);
        else {
            auto diff = eastl::min(std::abs(box.Min[i] - point[i]), std::abs(box.Max[i] - point[i]));
            dist2 -= std::pow(diff, 2);
        }
    }
    return Distance2(box.Perimeter(point), point);
}

float sk::Distance2(AABB box, Cylinder cylinder) {
    Sphere sphere(cylinder.Origin, cylinder.Radius);
    if(box.Min.y > cylinder.Origin.y + cylinder.HalfHeight) sphere.Origin.y += cylinder.HalfHeight;
    else if(box.Max.y < cylinder.Origin.y - cylinder.HalfHeight) sphere.Origin.y -= cylinder.HalfHeight;
    else sphere.Origin.y = box.Max.y;
    return Distance2(box, sphere);
}

float sk::Distance2(AABB box, FlatCylinder cylinder) {
    if(box.Min.y > cylinder.Origin.y + cylinder.HalfHeight) return Distance2(box.Min, cylinder.Perimeter(box.Min));
    else if(box.Max.y < cylinder.Origin.y - cylinder.HalfHeight) return Distance2(box.Max, cylinder.Perimeter(box.Max));
    else return Distance2(box, Sphere(glm::vec3{cylinder.Origin.x, box.Max.y, cylinder.Origin.z}, cylinder.Radius*(2/3)));
}

float sk::Distance2(Cylinder cylinder, glm::vec3 point) {
    Sphere sphere(cylinder.Origin, cylinder.Radius);
    sphere.Origin.y = eastl::clamp(point.y, cylinder.Origin.y - cylinder.HalfHeight, cylinder.Origin.y + cylinder.HalfHeight);
    return Distance2(sphere, point);
}

float sk::Distance2(glm::vec3 point1, glm::vec3 point2) {
    float dist2 = 0;
    for(int i = 0; i < 3; ++i)
        dist2 += std::pow(point1[i] - point2[i], 2);
    return dist2;
}

float sk::Distance(Sphere sphere1, Sphere sphere2) {
    return Distance(sphere1.Origin, sphere2.Origin) - sphere1.Radius - sphere2.Radius;
}

float sk::Distance(AABB box, Sphere sphere) {
    return std::sqrt(Distance2(box, sphere.Origin)) - sphere.Radius;
}

float sk::Distance(AABB box, glm::vec3 point) {
    return std::sqrt(Distance2(box, point));
}

float sk::Distance(AABB box, Cylinder cylinder) {
    Sphere sphere(cylinder.Origin, cylinder.Radius);
    if(box.Min.y > cylinder.Origin.y + cylinder.HalfHeight) sphere.Origin.y += cylinder.HalfHeight;
    else if(box.Max.y < cylinder.Origin.y - cylinder.HalfHeight) sphere.Origin.y -= cylinder.HalfHeight;
    else sphere.Origin.y = box.Max.y;
    return Distance(box, sphere);
}

float sk::Distance(AABB box, FlatCylinder cylinder) {
    if(box.Min.y > cylinder.Origin.y + cylinder.HalfHeight) return Distance(box.Min, cylinder.Perimeter(box.Min));
    else if(box.Max.y < cylinder.Origin.y - cylinder.HalfHeight) return Distance(box.Max, cylinder.Perimeter(box.Max));
    else return Distance(box, Sphere(glm::vec3{cylinder.Origin.x, box.Max.y, cylinder.Origin.z}, cylinder.Radius*(2/3)));
}

float sk::Distance(glm::vec3 point1, glm::vec3 point2) {
    return std::sqrt(Distance2(point1, point2));
}

glm::vec3 sk::Sphere::Normal(glm::vec3 p) {
    auto diff = p - Origin;
    if(glm::length2(diff) <= 0) return glm::vec3(0);
    return glm::normalize(diff);
}

glm::vec3 sk::Sphere::Perimeter(glm::vec3 p) {
    auto diff = p - Origin;
    auto length = glm::length(diff);
    if(length <= 0) return Origin;
    if(length > Radius) length = Radius;
    return Origin + glm::normalize(diff)*length;
}

glm::vec3 sk::Cylinder::Normal(glm::vec3 p) {
    Sphere sphere(Origin, Radius);
    if(p.y > Origin.y + HalfHeight) sphere.Origin.y += HalfHeight;
    else if(p.y < Origin.y - HalfHeight) sphere.Origin.y -= HalfHeight;
    else sphere.Origin.y = p.y;
    return sphere.Normal(p);
}

glm::vec3 sk::Cylinder::Perimeter(glm::vec3 p) {
    Sphere sphere(Origin, Radius);
    if(p.y > Origin.y + HalfHeight) sphere.Origin.y += HalfHeight;
    else if(p.y < Origin.y - HalfHeight) sphere.Origin.y -= HalfHeight;
    else sphere.Origin.y = p.y;
    return sphere.Perimeter(p);
}

glm::vec3 sk::FlatCylinder::Perimeter(glm::vec3 p) {
    Sphere sphere(Origin, Radius);
    if(p.y > Origin.y + HalfHeight) p.y = sphere.Origin.y + HalfHeight;
    else if(p.y < Origin.y - HalfHeight) p.y = sphere.Origin.y - HalfHeight;
    else sphere.Origin.y = p.y;
    auto pe = sphere.Perimeter(p);
    return pe;
}

glm::vec3 sk::FlatCylinder::Normal(glm::vec3 p) {
    if(p.y > Origin.y + HalfHeight) return glm::vec3{0, 1, 0};
    else if(p.y < Origin.y - HalfHeight) return glm::vec3{0, -1, 0};
    else return Sphere(glm::vec3{Origin.x, p.y, Origin.z}, Radius).Normal(p);
}
