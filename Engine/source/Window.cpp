#include <skya/Window.h>

#include <skya/imgui/imgui_impl_glfw.h>

#include <GL/gl3w.h>
#include <GLFW/glfw3.h>

#include <stdio.h>

using namespace sk;

GLFWwindow* init_glfw(){
    if(!glfwInit())
    {
        printf("Couldn't initialize glfw.\n");
        return nullptr;
    }
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_SAMPLES, 1);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#if _DEBUG
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
#endif

    auto* window = glfwCreateWindow(1, 1, "", nullptr, nullptr);
    if(!window)
    {
        printf("Couldn't create OpenGL context.\n");
        glfwTerminate();
        return nullptr;
    }
    glfwMakeContextCurrent(window);

    if(gl3wInit() != 0)
    {
        printf("Couldn't initialize glew.\n");
        glfwTerminate();
        return nullptr;
    }
    return window;
}

Window::Window() : m_glfw_window(init_glfw()), m_gui(m_glfw_window) {

    auto* window = m_glfw_window;

    if(window == nullptr) printf("Couldn't create Window.\n");
    glfwSetWindowUserPointer(window, this);
    glfwSetKeyCallback(window, &KeyCallback);
    glfwSetCharCallback(window, &CharCallback);
    glfwSetCursorPosCallback(window, &CursorPosCallback);
    glfwSetCursorEnterCallback(window, &CursorEnterCallback);
    glfwSetMouseButtonCallback(window, &MouseButtonCallback);
    glfwSetScrollCallback(window, &ScrollCallback);
    glfwSetFramebufferSizeCallback(window, &FramebufferSizeCallback);
    glfwSetWindowSizeCallback(window, &WindowSizeCallback);

    glfwSwapInterval(0);

    glfwGetWindowSize(window, &m_WindowWidth, &m_WindowHeight);
    glfwGetFramebufferSize(window, &m_FramebufferWidth, &m_FramebufferHeight);
}

void Window::StartGuiFrame() const {
    m_gui.start_frame();
}
void Window::RenderGuiFrame() const{
    m_gui.render();
}

void Window::SwapBuffers() {
    glfwSwapBuffers(m_glfw_window);
}

int Window::ShouldClose() const {
    return glfwWindowShouldClose(m_glfw_window);
}

glm::ivec2 sk::Window::GetFramebufferSize() const {
    return glm::ivec2(m_FramebufferWidth, m_FramebufferHeight);
}

glm::ivec2 sk::Window::GetWindowSize() const {
    return glm::ivec2(m_WindowWidth, m_WindowHeight);
}

void Window::SetWindowSize(int x, int y) {
    glfwSetWindowSize(m_glfw_window, x, y);
}

void Window::SetCursorMode(Window_CursorMode mode) {
    if(m_cursor_mode == mode) return;
    m_cursor_mode = mode;
    
    int glfwmode;
    switch (mode) {
    break;case CursorMode_Normal: glfwmode = GLFW_CURSOR_NORMAL;
    break;case CursorMode_Hidden: glfwmode = GLFW_CURSOR_HIDDEN;
    break;case CursorMode_Disabled: glfwmode = GLFW_CURSOR_DISABLED;
    }
    glfwSetInputMode(m_glfw_window, GLFW_CURSOR, glfwmode);
}
Window_CursorMode Window::GetCursorMode() const {
    return m_cursor_mode;
}

void Window::KeyCallback(GLFWwindow * window, int key, int scancode, int action, int mods) {
    ImGui_ImplGlfw_KeyCallback(window, key, scancode, action, mods);

    auto win = (Window*)glfwGetWindowUserPointer(window);
    KeyEventArgs e{ (Keyboard::Key)key, scancode, (Keyboard::Action)action, (Keyboard::Modifier)mods };

    if(win->m_gui.is_keyboard_captured()) return;

    win->OnKeyCallback(e);
}

void Window::CharCallback(GLFWwindow * window, unsigned int codepoint) {
    ImGui_ImplGlfw_CharCallback(window, codepoint);

    auto win = (Window*)glfwGetWindowUserPointer(window);

    if(win->m_gui.is_keyboard_captured()) return;

    CharEventArgs e{ (wchar_t)codepoint };
    win->OnCharCallback(e);
}

void Window::CursorPosCallback(GLFWwindow * window, double xpos, double ypos) {
    auto win = (Window*)glfwGetWindowUserPointer(window);

    CursorPosEventArgs e{ xpos, ypos };
    win->OnCursorPosCallback(e);
}

void Window::CursorEnterCallback(GLFWwindow * window, int entered) {
    auto win = (Window*)glfwGetWindowUserPointer(window);
    CursorEnterEventArgs e{ (bool)entered };
    win->OnCursorEnterCallback(e);
}

void Window::MouseButtonCallback(GLFWwindow * window, int button, int action, int mods) {
    ImGui_ImplGlfw_MouseButtonCallback(window, button, action, mods);

    auto win = (Window*)glfwGetWindowUserPointer(window);
    MouseButtonEventArgs e{ (Mouse::Button)button, (Mouse::Action)action, mods };

    if(win->m_gui.is_mouse_captured()) return;

    win->OnMouseButtonCallback(e);
}

void sk::Window::ScrollCallback(GLFWwindow * window, double xoffset, double yoffset) {
    ImGui_ImplGlfw_ScrollCallback(window, xoffset, yoffset);

    auto win = (Window*)glfwGetWindowUserPointer(window);

    ScrollEventArgs e{ xoffset, yoffset };
    win->OnScrollCallback(e);
}

void sk::Window::FramebufferSizeCallback(GLFWwindow * window, int width, int height) {
    glViewport(0, 0, width, height);
    auto win = (Window*)glfwGetWindowUserPointer(window);
    win->m_FramebufferHeight = height;
    win->m_FramebufferWidth = width;

    FramebufferSizeEventArgs e{ width, height };
    win->OnFramebufferSizeCallback(e);
}

void sk::Window::WindowSizeCallback(GLFWwindow * window, int width, int height) {
    auto win = (Window*)glfwGetWindowUserPointer(window);
    win->m_WindowHeight = height;
    win->m_WindowWidth = width;

    WindowSizeEventArgs e{ width, height };
    win->OnWindowSizeCallback(e);
}
