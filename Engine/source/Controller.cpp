#include <skya/Controller.h>

#include <EASTL/array.h>

#include <string.h>

sk::ControllerOriginData::ControllerOriginData(EControllerActionOrigin ty)
    : type(ty)
    , text(SteamController()->GetStringForActionOrigin(ty))
    , glyph(SteamController()->GetGlyphForActionOrigin(ty)) {}

sk::Controller::ControllerList sk::Controller::connected_controllers = sk::Controller::ControllerList();

bool sk::Controller::api_initialized = false;

eastl::vector<sk::ControllerData> sk::Controller::action_sets = eastl::vector<sk::ControllerData>();
eastl::vector<sk::ControllerData> sk::Controller::digital_actions = eastl::vector<sk::ControllerData>();
eastl::vector<sk::ControllerData> sk::Controller::analog_actions = eastl::vector<sk::ControllerData>();


int sk::Controller::GetDataIndex(eastl::span<const ControllerData> arr, const char *name) {
    for(int i = 0; i < arr.size(); ++i) if(strcmp(arr[i].name, name) == 0)
        return i;
    return arr.size();
}
int sk::Controller::GetDataIndex(eastl::span<const ControllerData> arr, uint64 handle) {
    for(int i = 0; i < arr.size(); ++i) if(arr[i].handle == handle)
        return i;
    return arr.size();
}

uint64 sk::Controller::GetActionSet(const char *name) {
    for(auto&& data : action_sets) if(strcmp(data.name, name) == 0) return data.handle;
    auto handle = SteamController()->GetActionSetHandle(name);
    if(handle) action_sets.emplace_back(name, handle);
    return handle;
}

uint64 sk::Controller::GetDigitalAction(const char *name) {
    for(auto&& data : digital_actions) if(strcmp(data.name, name) == 0) return data.handle;
    auto handle = SteamController()->GetDigitalActionHandle(name);
    if(handle) digital_actions.emplace_back(name, handle);
    return handle;
}

uint64 sk::Controller::GetAnalogAction(const char *name) {
    for(auto&& data : analog_actions) if(strcmp(data.name, name) == 0) return data.handle;
    auto handle = SteamController()->GetAnalogActionHandle(name);
    if(handle) analog_actions.emplace_back(name, handle);
    return handle;
}

void sk::Controller::Init() {
    if(api_initialized) return;
    SteamController()->Init();
    api_initialized = true;
}

void sk::Controller::ForceSync() {
    SteamController()->RunFrame();
}

const sk::Controller::ControllerList& sk::Controller::GetConnectedControllers() {
    connected_controllers.m_size = SteamController()->GetConnectedControllers(connected_controllers.m_array);
    return connected_controllers;
}

// instance

sk::Controller::Controller(uint64 controller_handle) {
    SetDevice(controller_handle);
}

uint64 sk::Controller::GetDevice() const {
    return handle;
}

void sk::Controller::SetDevice(uint64 controller_handle) {
    handle = controller_handle;
}

bool sk::Controller::HasDevice() const {
    for(auto&& controller : GetConnectedControllers())
    {
        if(controller == handle) return true;
    }
    return false;
}

void sk::Controller::ActivateActionSet(uint64 set_handle) {
    if(action_sets.size() && action_sets[current_action_set_index].handle == set_handle) return;
    int index = GetDataIndex(action_sets, set_handle);
    if(index == action_sets.size()) return;
    SteamController()->ActivateActionSet(handle, action_sets[current_action_set_index = index].handle);
}
void sk::Controller::ActivateActionSet(const char *name) {
    ActivateActionSet(GetActionSet(name));
}

sk::ControllerData sk::Controller::GetCurrentActionSet() const {
    if(!action_sets.size()) return ControllerData("", 0);
    return action_sets[current_action_set_index];
}

ControllerAnalogActionData_t sk::Controller::GetAnalogActionData(uint64 action_handle) const {
    return SteamController()->GetAnalogActionData(handle, action_handle);
}
ControllerAnalogActionData_t sk::Controller::GetAnalogActionData(const char *name) const {
    return GetAnalogActionData(GetAnalogAction(name));
}
eastl::vector<sk::ControllerOriginData> sk::Controller::GetAnalogActionOrigins(uint64 action_handle) const {
    if(!action_sets.size()) return eastl::vector<ControllerOriginData>();
    auto set_handle = action_sets[current_action_set_index].handle;
    eastl::array<EControllerActionOrigin, STEAM_CONTROLLER_MAX_ORIGINS> types;
    SteamController()->GetAnalogActionOrigins(handle, set_handle, action_handle, types.data());
    return eastl::vector<ControllerOriginData>(types.begin(), types.end());
}
eastl::vector<sk::ControllerOriginData> sk::Controller::GetAnalogActionOrigins(const char *name) const {
    return GetAnalogActionOrigins(GetAnalogAction(name));
}

ControllerDigitalActionData_t sk::Controller::GetDigitalActionData(uint64 action_handle) const {
    return SteamController()->GetDigitalActionData(handle, action_handle);
}
ControllerDigitalActionData_t sk::Controller::GetDigitalActionData(const char *name) const {
    return GetDigitalActionData(GetDigitalAction(name));
}

eastl::vector<sk::ControllerOriginData> sk::Controller::GetDigitalActionOrigins(uint64 action_handle) const {
    if(!action_sets.size()) return eastl::vector<ControllerOriginData>();
    auto set_handle = action_sets[current_action_set_index].handle;
    eastl::array<EControllerActionOrigin, STEAM_CONTROLLER_MAX_ORIGINS> types;
    SteamController()->GetDigitalActionOrigins(handle, set_handle, action_handle, types.data());
    return eastl::vector<ControllerOriginData>(types.begin(), types.end());
}
eastl::vector<sk::ControllerOriginData> sk::Controller::GetDigitalActionOrigins(const char *name) const {
    return GetDigitalActionOrigins(GetDigitalAction(name));
}

ControllerMotionData_t sk::Controller::GetMotionData() const {
    return SteamController()->GetMotionData(handle);
}

// utility

void sk::Controller::StopAnalogActionMomentum(uint64 action_handle) {
    SteamController()->StopAnalogActionMomentum(handle, action_handle);
}
void sk::Controller::StopAnalogActionMomentum(const char* name) {
    SteamController()->StopAnalogActionMomentum(handle, GetAnalogAction(name));
}

void sk::Controller::Pulse(ESteamControllerPad pad, unsigned short duration) {
    SteamController()->TriggerHapticPulse(handle, pad, duration);
}

void sk::Controller::RepeatedPulse(ESteamControllerPad pad, unsigned short ontime, unsigned short offtime, unsigned short repeat) {
    SteamController()->TriggerRepeatedHapticPulse(handle, pad, ontime, offtime, repeat, 0);
}

void sk::Controller::Vibration(unsigned short speed_left, unsigned short speed_right) {
    SteamController()->TriggerVibration(handle, speed_left, speed_right);
}

void sk::Controller::SetLED(uint8 r, uint8 g, uint8 b) {
    SteamController()->SetLEDColor(handle, r, g, b, k_ESteamControllerLEDFlag_SetColor);
}

void sk::Controller::ResetLED() {
    SteamController()->SetLEDColor(handle, 0, 0, 0, k_ESteamControllerLEDFlag_RestoreUserDefault);
}

bool sk::Controller::ShowBindingPanel() const {
    return SteamController()->ShowBindingPanel(handle);
}




