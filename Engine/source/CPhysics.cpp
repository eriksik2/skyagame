#include <skya/CPhysics.h>

#include <skya/CTransform.h>

sk::CPhysics::CPhysics()
    : Mass(80)
    , Drag(.1f) {}

void sk::CPhysics::OnUpdate(double dt) {

    this->template Get<sk::CTransform>()->GetPosition() += m_Velocity*float(dt);

    m_Velocity += m_Acceleration - Drag*m_Velocity;
    ResetAcceleration();
}

glm::vec3 sk::CPhysics::GetVelocity() const {
    return m_Velocity;
}

glm::vec3 sk::CPhysics::GetAcceleration() const {
    return m_Acceleration;
}

void sk::CPhysics::ResetAcceleration() {
    m_Acceleration = glm::vec3(0);
}


void sk::CPhysics::AddVelocity(glm::vec3 vel) {
    m_Velocity += vel;
}

void sk::CPhysics::AddAcceleration(glm::vec3 accel) {
    m_Acceleration += accel;
}

void sk::CPhysics::AddForce(glm::vec3 force) {
    m_Acceleration += force / Mass;
}

