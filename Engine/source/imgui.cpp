#include "skya/Profiling.h"
#include <skya/imgui/imgui.h>

#include <GL/gl3w.h>
#include <GLFW/glfw3.h>
#include <imgui.h>

using namespace sk;

imgui::imgui(GLFWwindow* window, ImGuiConfigFlags flags) {
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGui::StyleColorsDark();

    ImGuiStyle& style = ImGui::GetStyle();
    style.WindowRounding = 0.f;

    ImGuiIO& io = ImGui::GetIO();
    io.ConfigFlags |= flags;

    ImGui_ImplGlfw_InitForOpenGL(window, false); // don't install callbacks as theyre called by Window class
    ImGui_ImplOpenGL3_Init("#version 330");

    io.Fonts->Clear();
    io.Fonts->AddFontFromFileTTF("Content/Font/OpenSans-Regular.ttf", 16);
    io.Fonts->AddFontFromFileTTF("Content/Font/OpenSans-Light.ttf", 16);
    io.Fonts->AddFontFromFileTTF("Content/Font/OpenSans-Light.ttf", 32);
    io.Fonts->AddFontFromFileTTF("Content/Font/OpenSans-Regular.ttf", 11);
    io.Fonts->AddFontFromFileTTF("Content/Font/OpenSans-Bold.ttf", 11);
    io.Fonts->Build();

    io.DeltaTime = 1.f/60.f;
}

imgui::~imgui() {
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
}

bool sk::imgui::is_mouse_captured() const {
    ImGuiIO& io = ImGui::GetIO();
    return io.WantCaptureMouse;
}

bool sk::imgui::is_keyboard_captured() const {
    ImGuiIO& io = ImGui::GetIO();
    return io.WantCaptureKeyboard;
}

void sk::imgui::start_frame() const {
    SKYA_PROFILE_FUNCTION();
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    {   SKYA_PROFILE_SCOPE("ImGui::NewFrame()");
        ImGui::NewFrame();}
}

void imgui::render() const {
    {   SKYA_PROFILE_SCOPE("ImGui::Render()");
        ImGui::Render();
    }
    {   SKYA_PROFILE_SCOPE("ImGui_ImplOpenGL3_RenderDrawData()");
        auto* draw_data = ImGui::GetDrawData();
        ImGui_ImplOpenGL3_RenderDrawData(draw_data);
    }

    // Update and Render additional Platform Windows
    // (Platform functions may change the current OpenGL context, so we save/restore it to make it easier to paste this code elsewhere.
    //  For this specific demo app we could also call glfwMakeContextCurrent(window) directly)
    ImGuiIO& io = ImGui::GetIO();
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        GLFWwindow* backup_current_context = glfwGetCurrentContext();
        ImGui::UpdatePlatformWindows();
        ImGui::RenderPlatformWindowsDefault();
        glfwMakeContextCurrent(backup_current_context);
    }
}
