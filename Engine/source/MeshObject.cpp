#include "skya/grafx/base/Program.h"
#include <skya/MeshObject.h>

#include <EASTL/vector.h>

template<> sk::Content<sk::MeshObject>::DefaultMethod sk::Content<sk::MeshObject>::Default = &sk::MeshObject::FromFile;
template<> sk::Content<sk::MeshObject>::ErrorMethod sk::Content<sk::MeshObject>::Error = &sk::MeshObject::LoadError;

sk::MeshObject::MeshObject()
    : VAO(sk::grafxbase::create_vertex_array())
    , Indices(sk::grafxbase::create_buffer<int>())
    , Verts(sk::grafxbase::create_buffer<Vertex>()) {
    sk::grafxbase::bind_vertex_index_buffer(VAO, Indices);
}

sk::MeshObject::MeshObject(eastl::span<const Vertex> verts, eastl::span<const int> indices) : MeshObject() {
    RewriteData(verts, indices);
}


sk::grafxbase::BufferHandle<sk::MeshObject::Vertex> sk::MeshObject::GetVertices() {
    return Verts;
}

sk::grafxbase::BufferHandle<int> sk::MeshObject::GetIndices() {
    return Indices;
}

void sk::MeshObject::RewriteData(eastl::span<const Vertex> verts, eastl::span<const int> indices) {
    IndiceLength = indices.size();
    sk::grafxbase::allocate_buffer(Verts, verts, sk::grafxbase::BufferUsageHint_ClientWrite | sk::grafxbase::BufferUsageHint_DynamicRead);
    sk::grafxbase::allocate_buffer(Indices, indices, sk::grafxbase::BufferUsageHint_ClientWrite | sk::grafxbase::BufferUsageHint_DynamicRead);
}

void sk::MeshObject::Draw(sk::grafxbase::ProgramHandle program, sk::grafxbase::FramebufferHandle fb) {
    sk::grafxbase::DrawCommandSettings settings;
    settings.culling = sk::grafxbase::FaceCullingMode_CCW;
    sk::grafxbase::draw_triangles(VAO, program, fb, settings);
}

void sk::MeshObject::PipePositionToAttribute(int attribute) {
    sk::grafxbase::VertexAttributeFormat format {
        sk::grafxbase::ShaderDataType_Float3,
        sizeof(Vertex),
        0
    };
    sk::grafxbase::bind_vertex_attribute_buffer(VAO, attribute, Verts, format);
}

void sk::MeshObject::PipeNormalToAttribute(int attribute) {
    sk::grafxbase::VertexAttributeFormat format {
        sk::grafxbase::ShaderDataType_Float3,
        sizeof(Vertex),
        sizeof(glm::vec3)
    };
    sk::grafxbase::bind_vertex_attribute_buffer(VAO, attribute, Verts, format);
}

void sk::MeshObject::PipeUVToAttribute(int attribute) {
    sk::grafxbase::VertexAttributeFormat format {
        sk::grafxbase::ShaderDataType_Float2,
        sizeof(Vertex),
        sizeof(glm::vec3)*2
    };
    sk::grafxbase::bind_vertex_attribute_buffer(VAO, attribute, Verts, format);
}

void sk::MeshObject::PipeColorToAttribute(int attribute) {
    sk::grafxbase::VertexAttributeFormat format {
        sk::grafxbase::ShaderDataType_Float3,
        sizeof(Vertex),
        sizeof(glm::vec3)*2 + sizeof(glm::vec2)
    };
    sk::grafxbase::bind_vertex_attribute_buffer(VAO, attribute, Verts, format);
}


#define TINYOBJLOADER_IMPLEMENTATION // define this in only *one* compilation unit
#include <tiny_obj_loader.h>

sk::MeshObject* sk::MeshObject::LoadError() {
    return GenPlane("", 1, 1, 2);
}

#include <glm/gtc/random.hpp>
sk::MeshObject* sk::MeshObject::FromFile(const char * path) {
    tinyobj::attrib_t vertices;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string err;

    if(!tinyobj::LoadObj(&vertices, &shapes, &materials, &err, path) || err.length() > 0)
    {
        puts(err.c_str());
        return nullptr;
    }

    eastl::vector<Vertex> myVertices;
    eastl::vector<int> myIndices;

    for(size_t i = 0; i < vertices.vertices.size(); i += 3)
    {
        Vertex vert(vertices.vertices[i], vertices.vertices[i + 1], vertices.vertices[i + 2]);

        vert.Color = glm::vec3(glm::gaussRand(glm::vec3(.5f), glm::vec3(.5f)));
        vert.UV = glm::vec2((float)i/vertices.vertices.size());

        myVertices.push_back(vert);
    }
    for(size_t i = 0; i < shapes[0].mesh.indices.size(); i++)
    {
        myIndices.push_back(shapes[0].mesh.indices[i].vertex_index);
    }
    eastl::span v(myVertices.begin(), myVertices.end());
    eastl::span i(myIndices.begin(), myIndices.end());
    return new MeshObject(v, i);
}

sk::MeshObject* sk::MeshObject::GenPlane(const char*, float xlen, float ylen, int side) {
    eastl::vector<Vertex> verts; verts.reserve(side*side);
    eastl::vector<int> indices; indices.reserve(side*side*6);
    int index = 0;
    int squares = side - 1;
    for(int x = 0; x < side; ++x)
        for(int y = 0; y < side; ++y)
        {
            Vertex v;
            v.Position = glm::vec3(((float)x/squares - .5f)*xlen*2, ((float)y/squares - .5f)*ylen*2, 0);
            v.UV = glm::vec2((float)x/squares, (float)y/squares);
            v.Normal = glm::vec3(0, 1, 0);
            verts.push_back(v);
            if(x < side-1 && y < side-1)
            {
                indices.push_back(index);
                indices.push_back(index + 1);
                indices.push_back(index + side);
                indices.push_back(index + side);
                indices.push_back(index + 1);
                indices.push_back(index + 1 + side);
            }
            ++index;
        }
    return new MeshObject(verts, indices);
}
