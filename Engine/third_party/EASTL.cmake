cmake_minimum_required(VERSION 3.9.1)

execute_process(COMMAND git submodule update --init EASTL WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}")
execute_process(COMMAND git submodule update --init test/packages/EABase WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/EASTL")


set(OLD_BUILD_SHARED_LIBS ${BUILD_SHARED_LIBS})
set(BUILD_SHARED_LIBS FALSE)

option(EASTL_BUILD_BENCHMARK OFF)
option(EASTL_BUILD_TESTS OFF)
add_subdirectory("EASTL")

set(BUILD_SHARED_LIBS ${OLD_BUILD_SHARED_LIBS})

#file(GLOB SOURCES "EASTL/source/*.cpp")
#set(INCLUDES "EASTL/include")
#add_library(EASTL STATIC ${SOURCES})
#target_include_directories(EASTL PUBLIC ${INCLUDES})
#target_compile_definitions(EASTL PRIVATE _CHAR16T)
#target_compile_definitions(EASTL PRIVATE _CRT_SECURE_NO_WARNINGS)
#target_compile_definitions(EASTL PRIVATE _SCL_SECURE_NO_WARNINGS)
#target_compile_definitions(EASTL PRIVATE EASTL_OPENSOURCE=1)
#target_compile_features(EASTL PUBLIC cxx_std_14)
#add_subdirectory("EASTL/test/packages/EABase")
#target_link_libraries(EASTL EABase)

