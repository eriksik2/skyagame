cmake_minimum_required(VERSION 3.9.1)

add_library(tiny_obj_loader INTERFACE)
target_include_directories(tiny_obj_loader INTERFACE ".")