
execute_process(COMMAND git submodule update --init fmt WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}")


set(GLM_QUIET ON)
set(BUILD_SHARED_LIBS OFF)

set(GLM_TEST_ENABLE OFF)

add_subdirectory("fmt")