file(GLOB CONFIGS "*.cmake")
foreach(CONFIG ${CONFIGS})
    include(${CONFIG})
endforeach()