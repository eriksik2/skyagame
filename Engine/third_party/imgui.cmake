cmake_minimum_required(VERSION 3.9.1)

execute_process(COMMAND git submodule update --init imgui WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}")


file(GLOB SOURCES "imgui/*.cpp")
set(INCLUDES "imgui")

add_library(imgui STATIC ${SOURCES})
target_include_directories(imgui PUBLIC ${INCLUDES})