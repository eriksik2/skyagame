
execute_process(COMMAND git submodule update --init gl3w WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}")

add_subdirectory("gl3w")

set_source_files_properties("${CMAKE_CURRENT_BINARY_DIR}/gl3w/src/gl3w.c" PROPERTIES GENERATED 1)
add_library(gl3w_fix)
target_link_libraries(gl3w_fix PRIVATE gl3w)
target_include_directories(gl3w_fix INTERFACE "${CMAKE_CURRENT_BINARY_DIR}/gl3w/include")
