
execute_process(COMMAND git submodule update --init glfw WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}")


set(GLFW_BUILD_EXAMPLES OFF)
set(GLFW_BUILD_TESTS OFF)
add_subdirectory("glfw")