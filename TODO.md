* skyalib
  * skyalib_render
    * high level rendering build on top of skyalib_gl
    * 
  * move ini_loader to skyalib_ini or something

* Engine
  * Complete MeshObject, also make it better
  * Rewrite all of skmath as something better

* Player
  * use FlatCylinder for player collision (when FlatCylinder behaves correctly)