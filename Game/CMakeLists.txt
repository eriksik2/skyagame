cmake_minimum_required(VERSION 3.9.1)

add_subdirectory("third_party")

file(GLOB HEADERS "include/*.h")
file(GLOB SOURCES "source/*.cpp")

add_executable(Game ${HEADERS} ${SOURCES})
target_include_directories(Game PUBLIC "include")
target_link_libraries(Game SkyaEngine FastNoiseSIMD Cpp-Taskflow nlohmann_json::nlohmann_json)

set_target_properties(Game PROPERTIES VS_DEBUGGER_WORKING_DIRECTORY "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}")
source_group(include FILES ${HEADERS})
source_group(source FILES ${SOURCES} ${INLINES})

file(COPY "Content" DESTINATION ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
