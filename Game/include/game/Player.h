#pragma once
#include <skya/Engine.h>
#include <game/Chunk.h>
#include <game/World.h>
class Player : public sk::Component {
public:

    float base_speed = 3.0f;
    float sprint_multiplier = 3.0f;
    float sprint_control = 0.1f;
    float jump_force = 15.5f;
    bool noclip_enabled = false;
        
    Player();

    void OnStart();
    void OnUpdate(double dt);

    void SetWorld(World* w);
        
    const BlockType* on_block = nullptr;
    const BlockType* in_block = nullptr;
private:
    void OnCursorPos(sk::CursorPosEventArgs e);
    void OnKeyEvent(sk::KeyEventArgs e);
    void OnMouseButton(sk::MouseButtonEventArgs e);

    glm::vec2 look = glm::vec2(0);
    glm::vec2 mouse_pos = glm::vec2(0);
        
    bool go_forward = false;
    bool go_back = false;
    bool go_left = false;
    bool go_right = false;
    bool go_up = false;
    bool go_down = false;
    bool run = false;
    bool grounded = false;

    World* world = nullptr;
};

