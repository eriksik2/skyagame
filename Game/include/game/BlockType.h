#pragma once
#include <skya/ResourceLoader.h>
#include <skya/grafx/Base.h>

#include <EASTL/string.h>

class BlockType {
public:

    BlockType();

    const int ID;
    eastl::string Name;
    bool Transparent = false; // if true, doesn't hide adjecent block faces
    bool Hollow = false; // if true, no culling
    int Hitpoints = 10; // if 0 == unbreakable

    bool CollisionEnabled = true;
    float Drag = 0.3f;
    float Bounce = 0.0f;

    float SpeedMultiplier = 1.0f;
    float Density = 0.3f;

    sk::grafxbase::Texture2DHandle Texture[6];

    bool HasNoFace() const;
    bool HasFace(int face) const;
    bool BlocksFace(int face) const;

    void imgui_info(int id);

    static BlockType* LoadError();
    static BlockType* FromFile(const char* path);
    static void WriteFile(BlockType* obj, const char* path);
private:
    static int id_counter;
    static int UniqueID() { return ++id_counter; }
};

bool operator==(const BlockType& lhs, const BlockType& rhs);
bool operator!=(const BlockType& lhs, const BlockType& rhs);



