#pragma once

#include <game/Block.h>
#include <game/WorldGen/WorldGen.h>

#include <skya/grafx/Base.h>
#include <skya/Component.h>

#include <EASTL/vector.h>

#include <shared_mutex>

class MineGame;
class BlockType;
struct Generator;
class Chunk;

class World : public sk::Component {
public:
    glm::ivec3 SetChunkSize;
    glm::ivec3 SetChunkCount;
    glm::ivec3 SetOffset;

    World();
    ~World();

    glm::ivec3 GetChunkCount() const;
    glm::ivec3 GetChunkSize() const;
    glm::ivec3 GetOffset() const;

    void OnEnd();
    void OnLoad();

    int m_world_type;
    void SetWorldType(int type);

    int time = 0;
    void OnUpdate(double dt);
    void OnRender();

    glm::ivec3 ChunkPos(glm::vec3 pos);

    // chunk coords
    // creates new chunk in the buffer at pos if there isn't one
    Chunk& GetChunk(glm::ivec3 pos, int new_stage = -1);
    // doesn't create new chunk if one isnt loaded at requested pos
    Chunk* GetLoadedChunk(glm::ivec3 pos);

    const eastl::vector<Chunk*>& EnumerateChunks();

    // world coords
    // creates new chunk in the buffer at pos if there isn't one
    BlockInfo GetBlock(glm::ivec3 pos, int new_stage = -1);
    // doesn't create new chunk if one isnt loaded at requested pos
    BlockInfo GetLoadedBlock(glm::ivec3 pos);

    void Reload();
    bool m_reloaded = false;

    Block AirBlock;
    BlockType* Air = nullptr;
    BlockType* Grass = nullptr;
    BlockType* Dirt = nullptr;
    BlockType* Stone = nullptr;
    BlockType* Glass = nullptr;
    BlockType* Water = nullptr;
    BlockType* Wood = nullptr;
    BlockType* Leaves = nullptr;
    BlockType* Sand = nullptr;
    BlockType* Ash = nullptr;
    BlockType* Snow = nullptr;
    BlockType* Ice = nullptr;

    const BlockType* in_block = nullptr;

    glm::vec3 sky_color;

    WorldGen m_generator;
    sk::grafxbase::ProgramHandle BlockShader;

private:
    int total_chunk_count() const;
    void clear_chunk_storage();
    void set_chunk_count(glm::ivec3);
    void set_chunk_size(glm::ivec3);

    eastl::vector<Chunk*> m_chunks;
    eastl::vector<Chunk*> chunk_buffer;
    std::shared_mutex* m_chunk_buffer_mutex = new std::shared_mutex();


    glm::ivec3 m_chunk_size;
    glm::ivec3 m_chunk_count;
    glm::ivec3 m_offset;
};

