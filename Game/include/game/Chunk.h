#pragma once
#include <skya/Engine.h>
#include <game/Block.h>
#include <game/WorldGen/WorldGenData.h>

#include <skya/grafx/Base.h>

#include <EASTL/utility.h>
#include <EASTL/vector.h>

class MineGame;
class World;

class Chunk : public WorldGenData {
    friend Block;
    friend World;
public:
    eastl::vector<Block> m_blocks;
    World& m_world;
    glm::ivec3 m_position; // chunk pos. Multiply by size to get block pos

    Chunk(World* w, glm::ivec3 pos);

    void OnRender(sk::grafxbase::FramebufferHandle fb, bool render_transparent);

    World& get_world() const;

    glm::ivec3 world_position() const;
    glm::ivec3 chunk_position() const;
    glm::ivec3 size() const;
    // all get block functions in Chunk are relative to chunk position
    BlockInfo GetLoadedBlock(int x, int y, int z);
    BlockInfo GetBlock(int x, int y, int z);
    BlockInfo GetBlockFast(int x, int y, int z);


    void ReloadMesh();
    bool NeedsReload() const;

    bool b_NeedUpdate = false;
private:
    glm::ivec3 m_size;

    struct ChunkMesh {
        const BlockType& Type;
        sk::MeshObject Mesh;
        ChunkMesh(const BlockType& type,
                  int p, int n, int uv);
    };
    eastl::vector<ChunkMesh> Meshes;
};
