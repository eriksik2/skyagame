#pragma once

#include <skya/Engine.h>
#include <game/World.h>
#include <game/Player.h>
#include <skya/Component.h>

#include <skya/grafx/Base.h>

#include <skya/Controller.h>

class MineGame : public sk::Game {
    sk::ComponentManager manager;
public:
    MineGame()
        : manager(*this)
        , player(manager)
        , world(manager)
        , cow(manager)
        , pot(manager) {}

    void fbcallback(sk::FramebufferSizeEventArgs arg);
    void keycallback(sk::KeyEventArgs arg);
    void mousebuttoncallback(sk::MouseButtonEventArgs arg);

    bool focused() const;

    void PreLoad() override;
    void PreStart() override;
    void PreRender() override;
    void PostRender() override;

    void menu();

    glm::vec3 fog_color = glm::vec3(0);
    float fog_dist = 0;
    glm::mat4 VPMatrix = glm::mat4(0);

    sk::Entity player;
    sk::Entity world;
    sk::Entity cow;
    sk::Entity pot;

    //sk::Controller controller;
    sk::grafxbase::FramebufferHandle fb1;
private:
    bool is_focused = false;


    sk::grafxbase::Texture2DHandle diffuse;
    sk::grafxbase::Texture2DHandle normal;
    sk::grafxbase::Texture2DHandle depth;

    sk::grafxbase::ProgramHandle draw_texture_shader;
    sk::grafxbase::ProgramHandle final_shader;
    sk::MeshObject* plane;

    bool show_debug_framebuffers = false;
};

int entry();

//#ifdef _WIN32
//#include <windows.h>
//int APIENTRY WinMain(HINSTANCE, HINSTANCE, char*, int);
//#endif
int main();
