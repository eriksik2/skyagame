#pragma once

#include <skya/Handle.h>

#include <EASTL/string.h>

class Chunk;
class BlockType;

/* WorldGenLayer
*/
class WorldGenLayer {
    eastl::string m_name;
public:
    WorldGenLayer(eastl::string_view name);
    eastl::string_view get_name() const;

    virtual ~WorldGenLayer() = default;
    virtual void apply(Chunk&) const = 0;
};

class Noise;

class NoiseLayer : public WorldGenLayer {
    sk::handle<Noise> m_noise;
    sk::handle<BlockType> m_blocktype;
public:
    NoiseLayer(sk::handle<Noise>, sk::handle<BlockType>);
    virtual void apply(Chunk&) const override;
};


class SurfaceLayer : public WorldGenLayer {
    BlockType* stone;
    BlockType* air;
    BlockType* top;
    BlockType* bottom;
    BlockType* shore;
    BlockType* water;
    int water_level;
    int shore_level;
public:
    SurfaceLayer();
    virtual void apply(Chunk&) const override;
};

class TreeLayer : public WorldGenLayer {
    const BlockType* air;
    const BlockType* grass;
    const BlockType* wood;
public:
    TreeLayer();
    virtual void apply(Chunk&) const override;
};