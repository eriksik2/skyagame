#pragma once

#include <skya/Handle.h>

#include <EASTL/vector.h>

class Chunk;
class WorldGenLayer;

/* WorldGen
*/
class WorldGen {
    eastl::vector<sk::handle<WorldGenLayer>> m_layers;
public:

    void register_layer(sk::handle<WorldGenLayer>);
    void unregister_layer(sk::handle<WorldGenLayer>);
    void clear();

    void generate_next_layer(Chunk&) const;
    void generate_to_layer(Chunk&, int layer) const;
    void generate(Chunk&) const;
};