#pragma once

#include <mutex>

class WorldGenData {
    friend class WorldGenLayer;
    friend class WorldGen;
    enum class GenState { NONE , IN_PROGRESS , DONE };

    int m_next_layer_to_generate = 0;
    GenState m_generation_state = GenState::NONE;

    std::mutex m_generation_mutex;
public:
    bool is_generated() const { return m_generation_state == GenState::DONE; }
    bool current_generation_layer() const { return m_next_layer_to_generate - 1; }
};