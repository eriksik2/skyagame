#pragma once
#include <glm/glm.hpp>

class Chunk;
class BlockType;

struct Block {
    enum Side { TOP, BOTTOM, LEFT, RIGHT, FRONT, BACK };

    const BlockType* m_type = nullptr;
};


class BlockInfo {

    Chunk* m_chunk = nullptr;
    Block* m_block = nullptr;

    glm::ivec3 m_position = glm::ivec3(0);

public:
    BlockInfo() {}
    BlockInfo(Chunk* ch, Block* bl, glm::ivec3 p);
    
    bool valid() const;
    
    void set_type(const BlockType* type);
    const BlockType& get_type() const;

    glm::ivec3 local_position() const;
    glm::ivec3 world_position() const;
    Chunk& chunk() const;
};
