#pragma once
#include <stddef.h>

class FastNoiseSIMD;

class NoiseSet {
    friend class Noise;

    int m_sizex = 0;
    int m_sizey = 0;
    int m_sizez = 0;
    float* m_data = nullptr;
    float* m_index = nullptr;

    NoiseSet(float* data, int sizex, int sizey, int sizez)
        : m_sizex(sizex)
        , m_sizey(sizey)
        , m_sizez(sizez)
        , m_data(data)
        , m_index(data) {}
public:
    NoiseSet() {}
    ~NoiseSet();

    NoiseSet(const NoiseSet&) = delete;
    NoiseSet& operator=(const NoiseSet&) = delete;

    NoiseSet(NoiseSet&& move);
    NoiseSet& operator=(NoiseSet&& move);

    int size() const { return m_sizex*m_sizey*m_sizez; }
    float* begin() const { return m_data; }
    float* end() const { return m_data + size(); }

    float operator[](size_t index) const { return m_data[index]; }
    float operator()(int x, int y, int z) const;

    float next();
    void rewind() { m_index = m_data; }
};

class Noise {
    FastNoiseSIMD* noise;
public:
    enum NoiseType { Value, ValueFractal, Perlin, PerlinFractal, Simplex, SimplexFractal, WhiteNoise, Cellular, Cubic, CubicFractal };
    enum FractalType { FBM, Billow, RigidMulti };
    enum PerturbType { None, Gradient, GradientFractal, Normalise, Gradient_Normalise, GradientFractal_Normalise };

    enum CellularDistanceFunction { Euclidean, Manhattan, Natural };
    enum CellularReturnType { CellValue, Distance, Distance2, Distance2Add, Distance2Sub, Distance2Mul, Distance2Div, NoiseLookup, Distance2Cave };

    Noise();
    Noise(int seed);
    ~Noise();

    Noise(const Noise& from);
    Noise(Noise&& from);
    Noise& operator=(const Noise& from);
    Noise& operator=(Noise&& from);

    int GetSeed() const;

    void SetSeed(int seed);
    void SetFrequency(float frequency);
    void SetNoiseType(NoiseType noiseType);
    void SetAxisScales(float xScale, float yScale, float zScale);

    void SetFractalOctaves(int octaves);
    void SetFractalLacunarity(float lacunarity);
    void SetFractalGain(float gain);
    void SetFractalType(FractalType fractalType);

    void SetCellularReturnType(CellularReturnType cellularReturnType);
    void SetCellularDistanceFunction(CellularDistanceFunction cellularDistanceFunction);
    void SetCellularNoiseLookupType(NoiseType cellularNoiseLookupType);
    void SetCellularNoiseLookupFrequency(float cellularNoiseLookupFrequency);
    void SetCellularDistance2Indicies(int cellularDistanceIndex0, int cellularDistanceIndex1);
    void SetCellularJitter(float cellularJitter);

    void SetPerturbType(PerturbType perturbType);
    void SetPerturbAmp(float perturbAmp);
    void SetPerturbFrequency(float perturbFrequency);
    void SetPerturbFractalOctaves(int perturbOctaves);
    void SetPerturbFractalLacunarity(float perturbLacunarity);
    void SetPerturbFractalGain(float perturbGain);
    void SetPerturbNormaliseLength(float perturbNormaliseLength);

    NoiseSet GetNoiseSet(int xStart, int yStart, int zStart, int xSize, int ySize, int zSize, float scaleModifier = 1.0f);
};
