#include <game/Block.h>
#include <game/Chunk.h>
#include <game/World.h>

BlockInfo::BlockInfo(Chunk* ch, Block* bl, glm::ivec3 p)
    : m_chunk(ch)
    , m_block(bl)
    , m_position(p) {}

bool BlockInfo::valid() const {
    return m_chunk && m_block;
}

void BlockInfo::set_type(const BlockType * type) {
    if(!m_chunk || !m_block) return;
    if(m_block->m_type == type) return;
    m_block->m_type = type;

    auto size = m_chunk->size();
    Chunk* neigbor = nullptr;
    if(m_position.x <= 0) neigbor = m_chunk->get_world().GetLoadedChunk(m_chunk->chunk_position() + glm::ivec3{ -1, 0, 0 });
    else if(m_position.x >= size.x - 1) neigbor = m_chunk->get_world().GetLoadedChunk(m_chunk->chunk_position() + glm::ivec3{ 1, 0, 0 });
    if(neigbor) neigbor->b_NeedUpdate = true;

    neigbor = nullptr;
    if(m_position.y <= 0) neigbor = m_chunk->get_world().GetLoadedChunk(m_chunk->chunk_position() + glm::ivec3{ 0, -1, 0 });
    else if(m_position.y >= size.y - 1) neigbor = m_chunk->get_world().GetLoadedChunk(m_chunk->chunk_position() + glm::ivec3{ 0, 1, 0 });
    if(neigbor) neigbor->b_NeedUpdate = true;

    neigbor = nullptr;
    if(m_position.z <= 0) neigbor = m_chunk->get_world().GetLoadedChunk(m_chunk->chunk_position() + glm::ivec3{ 0, 0, -1 });
    else if(m_position.z >= size.z - 1) neigbor = m_chunk->get_world().GetLoadedChunk(m_chunk->chunk_position() + glm::ivec3{ 0, 0, 1 });
    if(neigbor) neigbor->b_NeedUpdate = true;

    m_chunk->b_NeedUpdate = true;
}

const BlockType& BlockInfo::get_type() const {
    if(!m_block || !m_block->m_type){
        if(!m_chunk) return *sk::Content<BlockType>::GetLoader().LoadError();
        return *m_chunk->get_world().Air;
    }
    return *m_block->m_type;
}

glm::ivec3 BlockInfo::local_position() const {
    return m_position;
}

glm::ivec3 BlockInfo::world_position() const {
    if(!m_chunk || !m_block) return m_position;
    return m_position + m_chunk->world_position();
}

Chunk & BlockInfo::chunk() const {
    return *m_chunk;
}
