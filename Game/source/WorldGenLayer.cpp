#include <game/WorldGen/WorldGenLayer.h>

#include <game/Noise.h>

#include <game/Chunk.h>
#include <game/BlockType.h>

WorldGenLayer::WorldGenLayer(eastl::string_view name) : m_name(name) {}

eastl::string_view WorldGenLayer::get_name() const {
    return m_name;
}


NoiseLayer::NoiseLayer(sk::handle<Noise> noise, sk::handle<BlockType> blocktype)
    : WorldGenLayer("NoiseLayer")
    , m_noise(noise)
    , m_blocktype(blocktype) {}

void NoiseLayer::apply(Chunk& chunk) const {
    auto chunkpos = chunk.world_position();
    auto chunksize = chunk.size();
    auto noise = m_noise->GetNoiseSet(chunkpos.x, chunkpos.y, chunkpos.z, chunksize.x, chunksize.y, chunksize.z);


    for(int y = 0; y < chunksize.y; ++y) {
        auto treshold = (float)(chunkpos.y + y) / 100;
        if(chunkpos.y + y < 0) treshold *= 2;

        for(int x = 0; x < chunksize.x; ++x)
        for(int z = 0; z < chunksize.z; ++z) {
            
            if(noise(x, y, z) > treshold)
                chunk.GetBlockFast(x, y, z).set_type(m_blocktype.get());
        }
    }
}

SurfaceLayer::SurfaceLayer()
    : WorldGenLayer("SurfaceLayer")
    , stone(sk::Content<BlockType>::Load("Content/Blocks/stone.block"))
    , air(sk::Content<BlockType>::Load("Content/Blocks/air.block"))
    , top(sk::Content<BlockType>::Load("Content/Blocks/grass.block"))
    , bottom(sk::Content<BlockType>::Load("Content/Blocks/dirt.block"))
    , shore(sk::Content<BlockType>::Load("Content/Blocks/sand.block"))
    , water(sk::Content<BlockType>::Load("Content/Blocks/water.block"))
    , water_level(0)
    , shore_level(2) {}


void SurfaceLayer::apply(Chunk& chunk) const {
    auto chunksize = chunk.size();
    auto chunkpos = chunk.world_position();
    //return;
    for(int x = 0; x < chunksize.x; ++x)
    for(int y = 0; y < chunksize.y; ++y)
    for(int z = 0; z < chunksize.z; ++z) {

        auto here = chunk.GetBlockFast(x, y, z);

        if(here.get_type() == *air && y + chunkpos.y <= water_level) {
            here.set_type(water);
            continue;
        }
        if(here.get_type() != *stone) continue;

        auto above = chunk.GetBlock(x, y + 1, z);

        if(above.get_type() != *air
        && above.get_type() != *water) continue;

        auto* ltop = top;
        auto* lbot = bottom;
        if(y + chunkpos.y <= shore_level){
            ltop = lbot = shore;
        }

        here.set_type(ltop);
        for(int i = 1; i <= 3; ++i){
            auto here = chunk.GetBlock(x, y - i, z);
            if(here.get_type() == *air) break;
            here.set_type(lbot);
        }
    }
}

TreeLayer::TreeLayer()
    : WorldGenLayer("TreeLayer")
    , air(sk::Content<BlockType>::Load("Content/Blocks/air.block"))
    , grass(sk::Content<BlockType>::Load("Content/Blocks/grass.block"))
    , wood(sk::Content<BlockType>::Load("Content/Blocks/wood.block")) {}


void TreeLayer::apply(Chunk& chunk) const {
    auto chunkpos = chunk.world_position();
    auto chunksize = chunk.size();
    //auto noise = sk::Content<Noise>::Load("Content/Noise/jungle.noise");
    //noise.GetNoiseSet(chunkpos.x, chunkpos.y, chunkpos.z, 1, 1, 1)

    for(int x = 0; x < chunksize.x; x += 5)
    for(int y = 0; y < chunksize.y; y += 1)
    for(int z = 0; z < chunksize.z; z += 5) {
        auto here = chunk.GetBlockFast(x, y, z);
        if(here.get_type() != *air) continue;
        auto below = chunk.GetBlock(x, y - 1, z);
        if(below.get_type() != *grass) continue;
        //below.set_type(dirt);
        for(int i = 0; i < 6; ++i){
            auto here = chunk.GetBlock(x, y + i, z);
            if(here.get_type() != *air) break;
            here.set_type(wood);
        }
    }
}


// tree and firtree
/*

struct GenTrees : GenStage {
    NoiseSet noiseset;
    NoiseSet branchset;
    const BlockType* air;
    const BlockType* grass;
    const BlockType* wood;
    const BlockType* leaves;

    float treshold = .08f;

    GenTrees()
        : air(get_type("air"))
        , grass(get_type("grass"))
        , wood(get_type("wood"))
        , leaves(get_type("leaves")) {}

    void setup(int sx, int sy, int sz) override {
        Noise noise(0);
        noise.SetNoiseType(Noise::WhiteNoise);
        noiseset = noise.GetNoiseSet(chunk_x, chunk_y, chunk_z, sx, sy, sz);
        branchset = noise.GetNoiseSet(chunk_x, chunk_y+5, chunk_z, sx, 1, 1);

    }
    void apply(int x, int y, int z) override {
        if(get({ x, y, z }).get_type() != *grass) return;
        if(get({ x, y+1, z }).get_type() != *air) return;
        if(get({ x+1, y+1, z }).get_type() != *air) return;
        if(get({ x-1, y+1, z }).get_type() != *air) return;
        if(get({ x, y+1, z+1 }).get_type() != *air) return;
        if(get({ x, y+1, z-1 }).get_type() != *air) return;
        if(noiseset(x, y, z) > treshold*2 - 1) return;

        branch(glm::ivec3(x, y, z), glm::vec3(0, 1, 0), 3);
    }

    void branch(glm::vec3 pos, glm::vec3 dir, float length, float lw = 0) {
        auto num_branch = 2;

        for(int i = 0; i<length; ++i)
        {
            pos += dir;
            auto bl = get({ int(round(pos.x)), int(round(pos.y)), int(round(pos.z)) });
            auto&& t = bl.get_type();
            if(t != *air && t != *leaves && t != *wood) return;
            bl.set_type(wood);

            if(lw)
                for(int x = int(pos.x-lw); x<=pos.x+lw; ++x)
                    for(int y = int(pos.y-lw); y<=pos.y+lw; ++y)
                        for(int z = int(pos.z-lw); z<=pos.z+lw; ++z)
                        {
                            glm::vec3 lpos(x, y, z);
                            auto bl = get({ x, y, z });
                            if(bl.get_type() != *air) continue;
                            auto dist2 = glm::distance2(lpos, pos);
                            if(dist2 > lw*lw) continue;
                            bl.set_type(leaves);
                        }
        }
        length *= .65f;
        if(length>=1)
        {
            for(int i = 0; i < num_branch; ++i)
            {
                auto rdir = glm::rotate(dir, branchset.next()/1.5f, glm::vec3(1, 0, 0));
                rdir = glm::rotate(rdir, branchset.next()/1.5f, glm::vec3(0, 0, 1));
                branch(pos, rdir, length, lw?lw+.2f:2);
            }
        }
    }
};

struct GenFirTrees : GenStage {
    NoiseSet noiseset;
    const BlockType* air;
    const BlockType* snow;
    const BlockType* wood;
    const BlockType* leaves;

    float treshold = .08f;

    int max_height = 13;
    int min_height = 6;

    GenFirTrees()
        : air(get_type("air"))
        , snow(get_type("snow"))
        , wood(get_type("wood"))
        , leaves(get_type("leaves")) {}

    void setup(int sx, int sy, int sz) override {
        Noise noise(0);
        noise.SetNoiseType(Noise::WhiteNoise);
        noiseset = noise.GetNoiseSet(chunk_x, chunk_y, chunk_z, sx, sy, sz);
    }
    void apply(int x, int y, int z) override {
        if(get({ x, y, z }).get_type() != *snow) return;
        if(get({ x, y+1, z }).get_type() != *air) return;
        if(get({ x+1, y+1, z }).get_type() != *air) return;
        if(get({ x-1, y+1, z }).get_type() != *air) return;
        if(get({ x, y+1, z+1 }).get_type() != *air) return;
        if(get({ x, y+1, z-1 }).get_type() != *air) return;
        if(noiseset(x, y, z) > treshold*2 - 1) return;
        int height = int((noiseset(0, y, 0)*.5+.5)*(max_height - min_height))+min_height;
        if(height%2) ++height;
        const int width = 5;

        if(get({ x, y+height, z }).get_type() != *air) return;

        int leafheight = height+2;
        for(int y1 = 2; y1 <= leafheight; ++y1)
        {
            auto w = width - y1/((leafheight+1)/4);
            if(w < 2) w += 1;
            if(y1%2) w += 1;
            if(w < 0) w = 0;
            if(y1 <= 2) w -= 1;
            for(int x1 = x-w; x1 <= x+w; ++x1)
                for(int z1 = z-w; z1 <= z+w; ++z1)
                {
                    if(abs(x1 - x)+abs(z1 - z) >= w) { continue; }
                    get({ x1, y+y1, z1 }).set_type(leaves);
                }
        }
        line({ x, y+1, z }, { x, y+height, z }, wood);
    }
};

*/