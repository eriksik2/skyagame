#include <game/BlockType.h>

#include <filesystem>

template<> sk::Content<BlockType>::DefaultMethod sk::Content<BlockType>::Default = &BlockType::FromFile;
template<> sk::Content<BlockType>::ErrorMethod sk::Content<BlockType>::Error = &BlockType::LoadError;

int BlockType::id_counter = 0;

BlockType::BlockType() : ID(UniqueID()) {
    for(int i = 0; i < 6; ++i)
        Texture[i] = {};
}

bool BlockType::HasNoFace() const {
    return !Texture[0]
        && !Texture[1]
        && !Texture[2]
        && !Texture[3]
        && !Texture[4]
        && !Texture[5];
}

bool BlockType::HasFace(int face) const {
    return (bool)Texture[face];
}

bool BlockType::BlocksFace(int face) const {
    return Texture[face] && !(Transparent || Hollow);
}


BlockType * BlockType::LoadError() {
    auto* err = new BlockType();
    for(int i = 0; i < 6; ++i)
        err->Texture[i] = *sk::Content<sk::grafxbase::Texture2DHandle>::GetLoader().LoadError();
    return err;
}

#include <nlohmann/json.hpp>
#include <EASTL/string.h>
#include <EASTL/string_view.h>
#include <string>
#include <iostream>
#include <fstream>

template<class Tj, class Tv>
bool optional(Tj& json, Tv& value){
    if(json.is_null()) return false;
    json.get_to(value);
    return true;
}

BlockType * BlockType::FromFile(const char * path) {
    auto* block_type = new BlockType;

    std::ifstream input(path);
    using json = nlohmann::json;

    json file2;
    input >> file2;

    if(!file2.is_object()){
        std::cout << "Error loading block: " << path;
        delete block_type;
        return nullptr;
    }
    std::string name;
    optional(file2["name"], name);
    if(name != "") block_type->Name = eastl::string_view{name.data()};
    optional(file2["transparent"], block_type->Transparent);
    optional(file2["hollow"], block_type->Hollow);
    optional(file2["hitpoints"], block_type->Hitpoints);

    if(auto& col = file2["collision"]; !col.is_null()){
        optional(col["enabled"], block_type->CollisionEnabled);
        optional(col["drag"], block_type->Drag);
        optional(col["bounce"], block_type->Bounce);
        optional(col["speed"], block_type->SpeedMultiplier);
        optional(col["density"], block_type->Density);
    }

    if(auto& tex = file2["texture"]; !tex.is_null()){
        eastl::array<std::string, 6> texpath;
        if(tex.is_string()){
            for(auto& str : texpath) tex.get_to(str);
        }
        else if(tex.is_object()){
            for(auto& str : texpath) tex["rest"].get_to(str);
            optional(tex["top"], texpath[0]);
            optional(tex["bottom"], texpath[1]);
            optional(tex["left"], texpath[2]);
            optional(tex["right"], texpath[3]);
            optional(tex["front"], texpath[4]);
            optional(tex["back"], texpath[5]);
        }
        std::filesystem::path p{std::string(path)};
        for(int i = 0; i < 6; ++i) {
            std::filesystem::path r{texpath[i]};
            auto fin = std::filesystem::canonical(p.remove_filename()/r);
            block_type->Texture[i] = *sk::Content<sk::grafxbase::Texture2DHandle>::Load(fin.string().c_str());
        }
    }
    return block_type;
}

void BlockType::WriteFile(BlockType* obj, const char * path) {
    /*ini_file file;

    file.add_pair("name", obj->Name.c_str());
    file.add_pair("transparent", obj->Transparent ? "true" : "false");

    file.add_section("texture");
    if(obj->Texture[0]) file.add_pair("top", sk::Content<sk::TextureObject>::View(obj->Texture[0]).Path.string().c_str());
    if(obj->Texture[1]) file.add_pair("bottom", sk::Content<sk::TextureObject>::View(obj->Texture[1]).Path.string().c_str());
    if(obj->Texture[2]) file.add_pair("left", sk::Content<sk::TextureObject>::View(obj->Texture[2]).Path.string().c_str());
    if(obj->Texture[3]) file.add_pair("rigth", sk::Content<sk::TextureObject>::View(obj->Texture[3]).Path.string().c_str());
    if(obj->Texture[4]) file.add_pair("front", sk::Content<sk::TextureObject>::View(obj->Texture[4]).Path.string().c_str());
    if(obj->Texture[5]) file.add_pair("back", sk::Content<sk::TextureObject>::View(obj->Texture[5]).Path.string().c_str());

    file.write(path);*/
}

#include <imgui.h>
void BlockType::imgui_info(int id) {
    using namespace ImGui;
    PushID(ID + (id<<4));
    BeginChild("Blocktype Info", ImVec2(0, 300), true);
    Text("Blocktype Info");

    Spacing();

    Text("ID: %d", ID);
    Text("Name: %s", Name.c_str());

    Checkbox("Transparent", &Transparent);
    Checkbox("Hollow", &Hollow);
    InputInt("Hollow", &Hitpoints);
    //Text("Transparent: %s", Transparent ? "true" : "false");
    //Text("Hollow: %s", Hollow ? "true" : "false");

    Spacing();

    Checkbox("Collision", &CollisionEnabled);
    Text("Collision: %s", CollisionEnabled ? "true" : "false");
    SliderFloat("Drag", &Drag, 0.f, 1.f);
    SliderFloat("Bounce", &Bounce, 0.f, 1.f);
    SliderFloat("Density", &Density, 0.f, 1.f);
    SliderFloat("SpeedMultiplier", &SpeedMultiplier, 0.f, 2.f);
    //Text("Drag: %f", Drag);
    //Text("Bounce: %f", Bounce);

    //Spacing();

    //Text("Textures");
    //int i;
    //const char* names[6] = {"Top", "Bottom", "Left", "Right", "Front", "Back"};
    //ListBox("Textures", &i, names, 6, 6);

    EndChild();
    PopID();
}

bool operator==(const BlockType& lhs, const BlockType& rhs) {
    return lhs.ID == rhs.ID;
}

bool operator!=(const BlockType& lhs, const BlockType& rhs) {
    return lhs.ID != rhs.ID;
}



