#include "skya/Profiling.h"
#include <game/World.h>
#include <game/main.h>
#include <game/Chunk.h>
#include <game/Noise.h>

#include <game/WorldGen/WorldGenLayer.h>

#include <skya/LoadUtil.h>

#include <taskflow/taskflow.hpp>

#include <algorithm>
#include <random>
#include <math.h>

tf::Executor ParExecutor;
std::ranlux24_base tmp_rng;

World::World()
    : SetChunkSize(8)
    , SetChunkCount(9, 5, 9)
    , SetOffset(0)
    , sky_color(153, 153, 242)
    , m_chunk_size(0)
    , m_chunk_count(0)
    , m_offset(0) {}

World::~World() {
    Reload();
}

glm::ivec3 World::GetChunkCount() const {
    return m_chunk_count;
}

glm::ivec3 World::GetChunkSize() const {
    return m_chunk_size;
}

glm::ivec3 World::GetOffset() const {
    return m_offset;
}

void World::OnEnd() {
    //delete BlockShader;
}

void World::OnLoad() {
    SKYA_PROFILE_FUNCTION();

    BlockShader = sk::load_shader_program("Content/Shader/block.vert", "Content/Shader/block.frag");

    for(int i = 0; i < sk::grafxbase::program_uniform_count(BlockShader); ++i){
        auto uniform = sk::grafxbase::program_uniform_info(BlockShader, i);
        
        if(uniform.name == "textureTop") sk::grafxbase::set_uniform_sampler(BlockShader, uniform.location, 0);
        else if(uniform.name == "textureBot") sk::grafxbase::set_uniform_sampler(BlockShader, uniform.location, 1);
        else if(uniform.name == "textureLeft") sk::grafxbase::set_uniform_sampler(BlockShader, uniform.location, 2);
        else if(uniform.name == "textureRight") sk::grafxbase::set_uniform_sampler(BlockShader, uniform.location, 3);
        else if(uniform.name == "textureFront") sk::grafxbase::set_uniform_sampler(BlockShader, uniform.location, 4);
        else if(uniform.name == "textureBack") sk::grafxbase::set_uniform_sampler(BlockShader, uniform.location, 5);
    }

    Air = sk::Content<BlockType>::Load("Content/Blocks/air.block");
    AirBlock.m_type = Air;

    Grass = sk::Content<BlockType>::Load("Content/Blocks/grass.block");
    Dirt = sk::Content<BlockType>::Load("Content/Blocks/dirt.block");
    Stone = sk::Content<BlockType>::Load("Content/Blocks/stone.block");
    Glass = sk::Content<BlockType>::Load("Content/Blocks/glass.block");
    Water = sk::Content<BlockType>::Load("Content/Blocks/water.block");
    Wood = sk::Content<BlockType>::Load("Content/Blocks/wood.block");
    Leaves = sk::Content<BlockType>::Load("Content/Blocks/leaves.block");
    Sand = sk::Content<BlockType>::Load("Content/Blocks/sand.block");
    Ash = sk::Content<BlockType>::Load("Content/Blocks/spire_ash.block");
    Snow = sk::Content<BlockType>::Load("Content/Blocks/snow.block");
    Ice = sk::Content<BlockType>::Load("Content/Blocks/ice.block");

    enum world_type { NORMAL, XENO, WINTER };
    SetWorldType(WINTER);
}

void World::SetWorldType(int type) {

    if(m_world_type == type) return;
    m_world_type = type;
    const auto* noise = sk::Content<Noise>::Load("Content/Noise/jungle.noise");
    m_generator.clear();

    auto n = eastl::make_shared<NoiseLayer>(
        eastl::make_shared<Noise>(*noise),
        eastl::shared_ptr<BlockType>(Stone));
    m_generator.register_layer(n);
    //m_generator.register_layer(eastl::make_shared<SurfaceLayer>());
    //m_generator.register_layer(eastl::make_shared<TreeLayer>());

    enum world_type { NORMAL, XENO, WINTER };
    switch(type) {
        case NORMAL:
        {
            sky_color = glm::vec3(153, 153, 248);
            /*world_gen->add<GenBase>(*noise, Stone);
            world_gen->add<GenSurface>();
            world_gen->add<GenStructure>();
            world_gen->add<GenTrees>();*/
            break;
        }
        case XENO:
        {
            sky_color = glm::vec3(50, 53, 58);
            /*auto* xeno = world_gen->add<GenBase>(*noise, Ash);
            xeno->noise.SetAxisScales(1, 2, 1);
            xeno->noise.SetPerturbAmp(5);*/
            break;
        }
        case WINTER:
        {
            sky_color = glm::vec3(166, 191, 234);
            /*world_gen->add<GenBase>(*noise, Stone);
            auto* surface = world_gen->add<GenSurface>();
            surface->water = Ice;
            surface->top = Snow;
            surface->top_count = 4;
            surface->bot_count = 2;
            surface->water_level = 3;
            world_gen->add<GenFirTrees>();*/
            break;
        }
    }
    Reload();
}

int index(glm::ivec3 size, glm::ivec3 ind){
    return ind.x + size.x*ind.y + size.x*size.y*ind.z;
}

void World::OnUpdate(double dt) {
    SKYA_PROFILE_FUNCTION();

    set_chunk_size(SetChunkSize);
    set_chunk_count(SetChunkCount);

    eastl::vector<Chunk*> gen_parallel[8];
    if(m_reloaded || SetOffset != m_offset) {
        auto diff = SetOffset - m_offset;
        m_offset = SetOffset;

        m_reloaded = false;


        eastl::vector<bool> remove;
        remove.resize(total_chunk_count(), true);

        eastl::vector<Chunk*> new_chunks;
        new_chunks.resize(total_chunk_count(), nullptr);

        for(int x = 0; x < m_chunk_count.x; ++x) for(int y = 0; y < m_chunk_count.y; ++y) for(int z = 0; z < m_chunk_count.z; ++z) {
            int cx = x + diff.x;
            int cy = y + diff.y;
            int cz = z + diff.z;

            Chunk* chunk;
            if(cx < 0 || cx >= m_chunk_count.x || cy < 0 || cy >= m_chunk_count.y || cz < 0 || cz >= m_chunk_count.z)
                chunk = nullptr;
            else {
                chunk = m_chunks[index(m_chunk_count, {cx, cy, cz})];
                remove[index(m_chunk_count, {cx, cy, cz})] = false;
            }
            new_chunks[index(m_chunk_count, {x, y, z})] = chunk;
        }
        for(int x = 0; x < m_chunk_count.x; ++x) for(int y = 0; y < m_chunk_count.y; ++y) for(int z = 0; z < m_chunk_count.z; ++z) {
            int i = index(m_chunk_count, {x, y, z});
            int par_index = (int[2][2][2]){
                {{0, 1},
                 {2, 3}},
                {{4, 5},
                 {6, 7}}
            }[abs(x+m_offset.x) % 2][abs(y+m_offset.y) % 2][abs(z+m_offset.z) % 2];
            if(!new_chunks[i]) {
                auto pos = glm::ivec3(x + m_offset.x - m_chunk_count.x/2, y + m_offset.y - m_chunk_count.y/2, z + m_offset.z - m_chunk_count.z/2);
                for(auto*& ch : chunk_buffer) // ref to ptr so im able to remove it
                    if(ch->chunk_position() == pos) {
                        new_chunks[i] = ch;
                        chunk_buffer.erase(&ch);
                        break;
                    }
                if(!new_chunks[i]) {
                    if(remove[i] && m_chunks[i]) {
                        new_chunks[i] = m_chunks[i];
                        new_chunks[i]->~Chunk();
                        new(new_chunks[i]) Chunk(this, pos);
                        remove[i] = false;
                    }
                    else new_chunks[i] = new Chunk(this, pos);
                }
                gen_parallel[par_index].push_back(new_chunks[i]);//generate.push_back(new_chunks[x][y][z]);
            }
            if(remove[i] && m_chunks[i]) {
                delete (m_chunks[i]);
                for(auto*& ch : gen_parallel[par_index])
                    if(ch == m_chunks[i]) gen_parallel[par_index].erase(&ch);//generate.erase(&ch);
            }
            m_chunks[i] = new_chunks[i];
        }
        // regularly clean chunk_buffer. there may be a better way to handle this
        for(int i = 0; i < chunk_buffer.size(); ++i) {
            while(!chunk_buffer[i]) chunk_buffer.erase(&chunk_buffer[i]);

            if(glm::length(glm::vec3(chunk_buffer[i]->chunk_position() - m_offset)) > m_chunk_count.x/2 + 5) {
                delete chunk_buffer[i];
                chunk_buffer.erase(&chunk_buffer[i]);
                --i;
            }
        }
    }


    tf::Task last;
    {
        SKYA_PROFILE_SCOPE("World Generation");
        tf::Taskflow gen_flow;
        last = gen_flow.placeholder();
        for(int i = 0; i < 8; ++i){
            auto [B, A] = gen_flow.parallel_for(gen_parallel[i].begin(), gen_parallel[i].end(), [this](Chunk* ch){
                //auto* surface = gen.add<GenSurface>();
                //surface->top = block;
                //if(block == stone) surface->top = surface->air;
                this->m_generator.generate(*ch);
            });
            B.succeed(last);
            last = A;
        }
        ParExecutor.run(gen_flow);
        ParExecutor.wait_for_all();
    }

    time = (time+1) % 4;
    if(time != 0) return;

    // gather chunks in checkerboard pattern
    eastl::vector<Chunk*> all_chunks[8];
    for(int i = 0; i < 8; ++i) all_chunks[i].reserve(m_chunk_count.x*m_chunk_count.y*m_chunk_count.z/8);
    for(int x = 0; x < m_chunk_count.x; ++x) for(int y = 0; y < m_chunk_count.y; ++y) for(int z = 0; z < m_chunk_count.z; ++z){
        int par_index = (int[2][2][2]){
            {{0, 1}, {2, 3}}, {{4, 5}, {6, 7}}
        }[abs(x+m_offset.x) % 2][abs(y+m_offset.y) % 2][abs(z+m_offset.z) % 2];
        all_chunks[par_index].push_back(m_chunks[index(m_chunk_count, {x, y, z})]);
    }

    auto* waterblock = this->Ash;
    auto* fallingblock = this->Sand;
    auto* air = this->Air;
    
    // build taskflow that updates chunks in parallel
    {
        SKYA_PROFILE_SCOPE("Parallel Update");
        tf::Taskflow update_flow;
        last = update_flow.placeholder();
        for(int i = 0; i < 8; ++i){
            auto [B, A] = update_flow.parallel_for(all_chunks[i].begin(), all_chunks[i].end(), [waterblock, fallingblock, air](Chunk* ch){
                
                auto size = ch->size();
                for(int y = 0; y < size.y; ++y) for(int x_ = 0; x_ < size.x; ++x_) {
                    int x = y % 2 == 0 ? x_ : size.x - x_ - 1;
                    for(int z_ = 0; z_ < size.z; ++z_){
                        int z = x % 2 == 0 ? z_ : size.z - z_ - 1;
                        auto here = ch->GetBlockFast(x, y, z);
                        if(&here.get_type() != fallingblock) continue;

                        eastl::array<glm::ivec3, 9> check = {{ {x, y-1, z},
                            {x-1, y-1, z-1}, {x-1, y-1, z}, {x-1, y-1, z+1},
                            {x, y-1, z-1},                  {x, y-1, z+1},
                            {x+1, y-1, z-1}, {x+1, y-1, z}, {x+1, y-1, z+1}
                        }};
                        std::shuffle(check.begin() + 1, check.end(), tmp_rng);
                        for(int i = 0; i < check.size() - 6; ++i){
                            auto pos = check[i];
                            auto there = ch->GetLoadedBlock(pos.x, pos.y, pos.z);
                            if(!there.valid()) break;
                            if(&there.get_type() == air){
                                here.set_type(air);
                                there.set_type(fallingblock);
                                break;
                            }
                        }
                    }
                }
            });
            B.succeed(last);
            last = A;
        }

        ParExecutor.run(update_flow);
        ParExecutor.wait_for_all();
    }
}

void World::OnRender() {
    SKYA_PROFILE_FUNCTION();

    auto& game = static_cast<MineGame&>(GetGame());
    if(in_block == Water) {
        game.fog_dist = 1.f + 0.8f;
        game.fog_color = glm::vec3(35, 67, 119)*(1/255.f);
    }
    else {
        game.fog_dist = 1.f + 0.97f;
        game.fog_color = sky_color*(1/255.f);
    }

    for(auto* c : m_chunks){
        if(!c) continue;
        if(c->NeedsReload()) c->ReloadMesh();
        c->OnRender(game.fb1, false);
    }
    for(auto* c : m_chunks){
        if(!c) continue;
        c->OnRender(game.fb1, true);
    }
    /*
    for(int x = 0; x < m_chunk_count.x; ++x) for(int y = 0; y < m_chunk_count.y; ++y) for(int z = 0; z < m_chunk_count.z; ++z) {
        auto* c = Chunks[x][y][z];
        if(!c) continue;
        //if(c->generation_stage < world_gen->stages.size() - 1) continue;
        if(c->NeedsReload()) c->ReloadMesh();
        c->OnRender(game.fb1, false);
    }
    for(int x = 0; x < m_chunk_count.x; ++x) for(int y = 0; y < m_chunk_count.y; ++y) for(int z = 0; z < m_chunk_count.z; ++z) {
        auto* c = Chunks[x][y][z];
        if(!c) continue;
        c->OnRender(game.fb1, true);
    }
    */

    //if(false) // debug stuff
    {
        using namespace ImGui;

        float winy = 20;

        Begin("Game Data", 0, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_AlwaysAutoResize);

        Text("Frametime: %f", GetIO().DeltaTime);

        ImGui::SetWindowPos({ GetIO().DisplaySize.x - GetWindowWidth() - 20, winy });
        winy += GetWindowHeight() + 10;
        End();

        Begin("World Data", 0, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_AlwaysAutoResize);

        Text("Buffered Chunks: %d", chunk_buffer.size());

        ImGui::SetWindowPos({ GetIO().DisplaySize.x - GetWindowWidth() - 20, winy });
        winy += GetWindowHeight() + 10;
        End();
    }
}

glm::ivec3 World::ChunkPos(glm::vec3 pos) {
    return glm::ivec3(glm::floor(pos/(glm::vec3)m_chunk_size));
}

Chunk& World::GetChunk(glm::ivec3 pos, int new_stage) {
    glm::ivec3 ind = pos - m_offset + m_chunk_count/2;
    for(int i = 0; i < 3; ++i)
    if(ind[i] < 0 || ind[i] >= m_chunk_count[i]) {
        {std::shared_lock l(*m_chunk_buffer_mutex);
            for(auto* ch : chunk_buffer)
            if(ch->chunk_position() == pos) {
                m_generator.generate_to_layer(*ch, new_stage);
                return *ch;
            }
        }
        auto* ch = new Chunk(this, pos);
        m_generator.generate_to_layer(*ch, new_stage);
        m_chunk_buffer_mutex->lock();
        chunk_buffer.push_back(ch);
        m_chunk_buffer_mutex->unlock();
        return *ch;
    }
    auto* ch = m_chunks[index(m_chunk_count, ind)];
    m_generator.generate_to_layer(*ch, new_stage);
    return *ch;
}

Chunk* World::GetLoadedChunk(glm::ivec3 pos) {
    pos = pos - m_offset + m_chunk_count/2;
    for(int i = 0; i < 3; ++i) {
        if(pos[i] < 0 || pos[i] >= m_chunk_count[i]) return nullptr;
    }
    return m_chunks[index(m_chunk_count, pos)];
}

const eastl::vector<Chunk*>& World::EnumerateChunks(){
    return m_chunks;
}



BlockInfo World::GetBlock(glm::ivec3 pos, int new_stage) {
    auto&& chunk = GetChunk(ChunkPos(pos), new_stage);
    auto bl_pos = pos - chunk.world_position();
    return chunk.GetBlockFast(bl_pos.x, bl_pos.y, bl_pos.z);
}

BlockInfo World::GetLoadedBlock(glm::ivec3 pos) {
    auto* chunk = GetLoadedChunk(ChunkPos(pos));
    if(!chunk) return BlockInfo(nullptr, &AirBlock, glm::ivec3(0));
    auto bl_pos = pos - chunk->world_position();
    return chunk->GetBlockFast(bl_pos.x, bl_pos.y, bl_pos.z);
}

void World::Reload() {
    clear_chunk_storage();
    m_chunks.resize(total_chunk_count(), nullptr);
    m_reloaded = true;
}

int World::total_chunk_count() const {
    return m_chunk_count.x*m_chunk_count.y*m_chunk_count.z;
}

void World::clear_chunk_storage() {
    for(auto* ch : m_chunks) if(ch) delete ch;
    m_chunks.clear();

    for(auto* ch : chunk_buffer) if(ch) delete ch;
    chunk_buffer.clear();
}

void World::set_chunk_count(glm::ivec3 count) {
    if(count == m_chunk_count) return;
    m_chunk_count = count;
    Reload();
}
void World::set_chunk_size(glm::ivec3 size) {
    if(size == m_chunk_size) return;
    m_chunk_size = size;
    Reload();
}