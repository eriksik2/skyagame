#include <game/main.h>
#include <game/World.h>
#include <game/Player.h>
#include <game/BlockType.h>

#include <skya/Profiling.h>

#include <glm/gtc/matrix_transform.hpp>
#include <imgui.h>

#include <GL/gl3w.h>

#include <random>
#include <math.h>

void MineGame::PreLoad() {
    SKYA_PROFILE_FUNCTION();

    final_shader = sk::load_shader_program("Content/Shader/combine.vert", "Content/Shader/combine.frag");
    draw_texture_shader = sk::load_shader_program("Content/Shader/draw_tex.vert", "Content/Shader/draw_tex.frag");

    plane = sk::MeshObject::GenPlane("", 1, 1, 2);
    sk::grafxbase::enable_vertex_attribute(plane->VAO, 0);
    plane->PipePositionToAttribute(0);


    fb1 = sk::grafxbase::create_framebuffer();
    sk::grafxbase::set_framebuffer_blend(fb1, sk::grafxbase::BlendFactor_NewAlpha, sk::grafxbase::BlendFactor_NewAlphaInverted);
    sk::grafxbase::set_framebuffer_blend(sk::grafxbase::DefaultFramebuffer, sk::grafxbase::BlendFactor_NewAlpha, sk::grafxbase::BlendFactor_NewAlphaInverted);
    sk::grafxbase::create_buffer<int>();


    depth = sk::grafxbase::create_texture_2D();
    sk::grafxbase::set_texture_minmag_filter(depth, sk::grafxbase::TextureScaleType_Nearest);
    sk::grafxbase::build_texture_mipmaps(depth);

    normal = sk::grafxbase::create_texture_2D();
    sk::grafxbase::set_texture_minmag_filter(normal, sk::grafxbase::TextureScaleType_Nearest);
    sk::grafxbase::build_texture_mipmaps(normal);

    diffuse = sk::grafxbase::create_texture_2D();
    sk::grafxbase::set_texture_minmag_filter(diffuse, sk::grafxbase::TextureScaleType_Nearest);
    sk::grafxbase::build_texture_mipmaps(diffuse);


    for(int i = 0; i < sk::grafxbase::program_uniform_count(final_shader); ++i){
        auto uniform = sk::grafxbase::program_uniform_info(final_shader, i);
            
        if(uniform.name == "diffuse_map") sk::grafxbase::set_uniform_texture(final_shader, uniform.location, diffuse);
        else if(uniform.name == "depth_map") sk::grafxbase::set_uniform_texture(final_shader, uniform.location, depth);
        else if(uniform.name == "normal_map") sk::grafxbase::set_uniform_texture(final_shader, uniform.location, normal);
    }

    for(int i = 0; i < sk::grafxbase::program_uniform_count(draw_texture_shader); ++i){
        auto uniform = sk::grafxbase::program_uniform_info(draw_texture_shader, i);
            
        if(uniform.name == "texture_map") sk::grafxbase::set_uniform_sampler(draw_texture_shader, uniform.location, 0);
    }

    // steam controller
    //controller.Init();

    window.OnFramebufferSizeCallback += sk::DELEGATE(&MineGame::fbcallback, this);
    window.OnKeyCallback += sk::DELEGATE(&MineGame::keycallback, this);
    window.OnMouseButtonCallback += sk::DELEGATE(&MineGame::mousebuttoncallback, this);
}

void MineGame::fbcallback(sk::FramebufferSizeEventArgs arg) {
    SKYA_PROFILE_FUNCTION();

    sk::grafxbase::allocate_texture(depth, sk::grafxbase::TextureFormat_Depth, {arg.width, arg.height});
    sk::grafxbase::bind_framebuffer_depth_texture(fb1, depth);

    sk::grafxbase::allocate_texture(diffuse, sk::grafxbase::TextureFormat_RGB16, {arg.width, arg.height});
    sk::grafxbase::bind_framebuffer_texture(fb1, 0, diffuse);

    sk::grafxbase::TextureFormat norm {
        sk::grafxbase::TextureFormatType_Float, 3,
        sk::grafxbase::Bitdepth_32
    };
    sk::grafxbase::allocate_texture(normal, norm, {arg.width, arg.height});
    sk::grafxbase::bind_framebuffer_texture(fb1, 1, normal);

    for(int i = 0; i < sk::grafxbase::program_uniform_count(draw_texture_shader); ++i){
        auto uniform = sk::grafxbase::program_uniform_info(draw_texture_shader, i);
            
        if(uniform.name == "windowSize") sk::grafxbase::set_uniform_float2(draw_texture_shader, uniform.location, {arg.width, arg.height});
    }
}

void MineGame::keycallback(sk::KeyEventArgs arg) {
    if(arg.key != sk::Keyboard::Key::ESCAPE) return;
    window.SetCursorMode(sk::CursorMode_Normal);
    is_focused = false;
}

void MineGame::mousebuttoncallback(sk::MouseButtonEventArgs arg) {
    if(arg.action != sk::Mouse::Action::PRESS || arg.button != sk::Mouse::Button::LEFT) return;
    is_focused = true;
    window.SetCursorMode(sk::CursorMode_Disabled);
}

bool MineGame::focused() const {
    return is_focused;
}

void MineGame::PreStart() {
    SKYA_PROFILE_FUNCTION();

    world.AddComponent<World>();

    player.AddComponent<sk::CTransform>();

    Player& plr = *player.AddComponent<Player>();
    plr.SetWorld(&world.template Get<World>().value());

    auto cam = player.AddComponent<sk::CCamera>();
    cam->VerticalFov = 1.5f;
    cam->NearZ = 0.1f;
    default_camera = cam;

    player.AddComponent<sk::CPhysics>();

    

    sk::CTransform& ptr = *pot.AddComponent<sk::CTransform>();
    ptr.GetPosition() += sk::CTransform::WorldForward * 5.0f;

    sk::CMesh& pmesh = *pot.AddComponent<sk::CMesh>();
    pmesh.SetMesh(sk::Content<sk::MeshObject>::Load("Content/Mesh/teapot.obj"));
    pmesh.SetTexture(*sk::Content<sk::grafxbase::Texture2DHandle>::Load("Content/Texture/grass/top.bmp"));
    pmesh.SetFramebuffer(fb1);

    sk::CTransform& ctr = *cow.AddComponent<sk::CTransform>();
    ctr.GetPosition() += sk::CTransform::WorldLeft * 10.0f;
    
    sk::CMesh& cowmesh = *cow.AddComponent<sk::CMesh>();
    cowmesh.SetTexture(diffuse);
}


void MineGame::PreRender() {
    SKYA_PROFILE_FUNCTION();

    menu();
    auto view = player.template Get<sk::CTransform>()->GetViewMatrix();
    auto proj = player.template Get<sk::CCamera>()->GetProjectionMatrix();
    VPMatrix = proj * view;

    {
        SKYA_PROFILE_SCOPE("Clear Framebuffers");
        static glm::vec4 sky = {.6f, .6f, .95f, 1};
        ImGui::ColorEdit4("Sky color", (float*)&sky);
        int sl[] = {0};
        sk::grafxbase::clear_framebuffer(fb1, sky, sl);
        sk::grafxbase::clear_framebuffer_depth(fb1);
    }
}
void MineGame::PostRender() {
    SKYA_PROFILE_FUNCTION();

    sk::grafxbase::clear_framebuffer(sk::grafxbase::DefaultFramebuffer, {.0f, .0f, .0f, 1.f});
    sk::grafxbase::clear_framebuffer_depth(sk::grafxbase::DefaultFramebuffer);

    int size_loc, pos_loc;
    for(int i = 0; i < sk::grafxbase::program_uniform_count(draw_texture_shader); ++i){
        auto uniform = sk::grafxbase::program_uniform_info(draw_texture_shader, i);
        if(uniform.name == "textureSize") size_loc = uniform.location;
        if(uniform.name == "texturePos") pos_loc = uniform.location;
    }

    auto size = this->window.GetFramebufferSize();
    if(show_debug_framebuffers) {
        sk::grafxbase::Texture2DHandle texture[] = { diffuse, normal, depth };

        float between_dist = .01f;
        float border_dist = .121f;

        float rx, rw = (1  - border_dist*2)/3.f - between_dist;
        for(int i = 0; i < 3; ++i) {
            rx = border_dist + ((1 - border_dist*2)/3.f + between_dist/2)*i;

            sk::grafxbase::set_uniform_float2(draw_texture_shader, size_loc, {rw*size.x, rw*size.y});
            sk::grafxbase::set_uniform_float2(draw_texture_shader, pos_loc, {rx*size.x, 0});

            sk::grafxbase::texture_unit(texture[i], 0);
            sk::grafxbase::draw_triangles(plane->VAO, draw_texture_shader);
        }
        sk::grafxbase::set_uniform_float2(draw_texture_shader, size_loc, {(1 - rw)*size.x, size.y - rw*size.y});
        sk::grafxbase::set_uniform_float2(draw_texture_shader, pos_loc, {(rw*.5)*size.x, rw*size.y});

        sk::grafxbase::texture_unit(diffuse, 0);
        sk::grafxbase::draw_triangles(plane->VAO, draw_texture_shader);
    }else{
        sk::grafxbase::texture_unit(diffuse, 0);
        sk::grafxbase::texture_unit(depth, 1);
        sk::grafxbase::texture_unit(normal, 2);

        auto& cam = *player.template Get<sk::CCamera>();
        auto& plr = *player.template Get<sk::CTransform>();

        for(int i = 0; i < sk::grafxbase::program_uniform_count(final_shader); ++i){
            auto uniform = sk::grafxbase::program_uniform_info(final_shader, i);
            
            if(uniform.name == "proj_mtx") sk::grafxbase::set_uniform_float44(final_shader, uniform.location, cam.GetProjectionMatrix());
            else if(uniform.name == "view_mtx") sk::grafxbase::set_uniform_float44(final_shader, uniform.location, plr.GetViewMatrix());
            else if(uniform.name == "pA") sk::grafxbase::set_uniform_float1(final_shader, uniform.location, cam.FarZ / (cam.FarZ - cam.NearZ));
            else if(uniform.name == "pB") sk::grafxbase::set_uniform_float1(final_shader, uniform.location, (-cam.FarZ * cam.NearZ) / (cam.FarZ - cam.NearZ));
            else if(uniform.name == "fog_color") sk::grafxbase::set_uniform_float3(final_shader, uniform.location, fog_color);
            else if(uniform.name == "fog_dist") sk::grafxbase::set_uniform_float1(final_shader, uniform.location, fog_dist);
        }
        sk::grafxbase::draw_triangles(plane->VAO, final_shader);
    }
}

#include <imgui_internal.h>

void MineGame::menu() {
    SKYA_PROFILE_FUNCTION();

    static thread_local char filter[256] = { 0 };
    static thread_local glm::bvec3 world_follow_player = glm::bvec3(true, true, true);
    static thread_local int chunk_per_frame = 1;

    static bool demo_window_open = false;
    if(demo_window_open) ImGui::ShowDemoWindow(&demo_window_open);

    ImGui::SetNextWindowPos(ImVec2(20, 20), ImGuiCond_FirstUseEver);
    ImGui::Begin("Settings", 0, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_AlwaysAutoResize);

    ImGui::Text("Build type:" BUILD_TYPE);
    ImGui::Text("Compiler:" COMPILER_ID);
    ImGui::Checkbox("Debug view", &show_debug_framebuffers);

    if(ImGui::BeginMenu("ImGui")){
        if(ImGui::Button("Debug Item Picker")) ImGui::DebugStartItemPicker();
        if(ImGui::Button(demo_window_open ? "Close demo window###toggle_demo_window" : "Open demo window###toggle_demo_window")) demo_window_open = !demo_window_open;
        ImGui::EndMenu();
    }

    // Player Settings
    if(ImGui::BeginMenu("Player")) {
        auto&& plr = *player.template Get<Player>();
        auto pos = plr.template Get<sk::CTransform>()->GetPosition();
        ImGui::Text("Plr Pos: %d, %d, %d", int(pos.x), int(pos.y), int(pos.z));

        ImGui::SliderFloat("Ground Speed", &plr.base_speed, 1.0f, 100.0f);
        ImGui::SliderFloat("Sprint Multiplier", &plr.sprint_multiplier, 1.0f, 5.0f);
        ImGui::SliderFloat("Sprint Control", &plr.sprint_control, 0.01f, 1.0f, "%.3f", 2);
        ImGui::SliderFloat("Jump Force", &plr.jump_force, 10, 400, "%.3f", 2);
        ImGui::Checkbox("Noclip", &plr.noclip_enabled);

        ImGui::Text("Touching Blocks:");
        if(plr.in_block) const_cast<BlockType*>(plr.in_block)->imgui_info(0);
        if(plr.on_block) const_cast<BlockType*>(plr.on_block)->imgui_info(1);

        ImGui::EndMenu();
    }

    auto&& w = world.Get<World>();
    if(w){
        auto plroffset = w->ChunkPos(player.template Get<sk::CTransform>()->GetPosition());
        if(world_follow_player.x) w->SetOffset.x = plroffset.x;
        if(world_follow_player.y) w->SetOffset.y = plroffset.y;
        if(world_follow_player.z) w->SetOffset.z = plroffset.z;
    }
    
    // World Settings
    if(ImGui::BeginMenu("World")) {
        if(w){
            ImGui::Text("World Settings");
            ImGui::Separator();

            ImGui::Text("World Type");
            ImGui::Indent();
            if(ImGui::Button("Normal")) w->SetWorldType(0);
            ImGui::SameLine();
            if(ImGui::Button("Xeno")) w->SetWorldType(1);
            ImGui::SameLine();
            if(ImGui::Button("Winter")) w->SetWorldType(2);
            ImGui::Unindent();

            // # of chunks to generate in each direction
            ImGui::Text("Chunk Count");
            ImGui::Indent();
            ImGui::SliderInt("X##dist", &w->SetChunkCount.x, 1, 100);
            ImGui::SliderInt("Y##dist", &w->SetChunkCount.y, 1, 100);
            ImGui::SliderInt("Z##dist", &w->SetChunkCount.z, 1, 100);

            ImGui::Unindent();
            ImGui::Separator();
            // # of blocks in each chunk in each direction
            ImGui::Text("Chunk size");
            ImGui::Indent();
            ImGui::SliderInt("X##size", &w->SetChunkSize.x, 1, 256);
            ImGui::SliderInt("Y##size", &w->SetChunkSize.y, 1, 256);
            ImGui::SliderInt("Z##size", &w->SetChunkSize.z, 1, 256);

            ImGui::Unindent();
            ImGui::Separator();
            // World Generation Offset == Player Position
            ImGui::Text("Positioning");
            ImGui::Indent();
            ImGui::Text("Use Player Position:"); ImGui::SameLine();
            ImGui::Checkbox("X##check", &world_follow_player.x); ImGui::SameLine();
            ImGui::Checkbox("Y##check", &world_follow_player.y); ImGui::SameLine();
            ImGui::Checkbox("Z##check", &world_follow_player.z);

            if(!world_follow_player.x || !world_follow_player.y || !world_follow_player.z) ImGui::Text("Set Position");
            if(!world_follow_player.x) ImGui::DragInt("X##pos", &w->SetOffset.x, 0.1f);
            if(!world_follow_player.y) ImGui::DragInt("Y##pos", &w->SetOffset.y, 0.1f);
            if(!world_follow_player.z) ImGui::DragInt("Z##pos", &w->SetOffset.z, 0.1f);

            ImGui::Unindent();
            ImGui::Separator();
            ImGui::Text("Performance");
            ImGui::Indent();

            ImGui::Unindent();
            ImGui::Separator();
            // Reload world
            if(ImGui::Button("Reload All")) w->Reload();
        }
        else {
            ImGui::TextUnformatted("World does not exist.");
        }

        ImGui::EndMenu();
    }

    if(ImGui::BeginMenu("Resources")) {
        ImGui::InputText("Filter", filter, 256, ImGuiInputTextFlags_AutoSelectAll);
        ImGui::Columns(2);
        ImGui::Separator();
        ImGui::Text("Name"); ImGui::NextColumn();
        ImGui::Text("Time loaded"); ImGui::NextColumn();
        ImGui::Separator();
        for(auto& tex : sk::Content<sk::grafxbase::Texture2DHandle>::ViewAll()) {
            auto name = tex.Path.string();
            if(!strstr(name.c_str(), filter)) continue;
            ImGui::PushID(&tex);
            auto* time = localtime(&tex.Time);
            ImGui::Image(sk::grafxbase::get_backend_handle(*tex.Data), ImVec2(12, 12)); ImGui::SameLine();
            ImGui::Text("%s", name.c_str()); ImGui::NextColumn();
            ImGui::Text("%d:%d:%d", time->tm_hour, time->tm_min, time->tm_sec); ImGui::NextColumn();
            ImGui::PopID();
        }
        for(auto& tex : sk::Content<BlockType>::ViewAll()) {
            auto name = tex.Path.string();
            if(!strstr(name.c_str(), filter)) continue;
            ImGui::PushID(&tex);
            auto* time = localtime(&tex.Time);
            ImGui::Text("%s", name.c_str()); ImGui::NextColumn();
            ImGui::Text("%d:%d:%d", time->tm_hour, time->tm_min, time->tm_sec); ImGui::NextColumn();
            ImGui::PopID();
        }
        for(auto& tex : sk::Content<sk::MeshObject>::ViewAll()) {
            auto name = tex.Path.string();
            if(!strstr(name.c_str(), filter)) continue;
            ImGui::PushID(&tex);
            auto* time = localtime(&tex.Time);
            ImGui::Text("%s", name.c_str()); ImGui::NextColumn();
            ImGui::Text("%d:%d:%d", time->tm_hour, time->tm_min, time->tm_sec); ImGui::NextColumn();
            ImGui::PopID();
        }
        ImGui::Columns(1);

        ImGui::EndMenu();
    }
    ImGui::End();
}
int entry() {
#if defined(_MSC_VER) && defined(DEBUG)
    _CrtSetDbgFlag(_CRTDBG_CHECK_ALWAYS_DF);
#endif

    {
        auto game = MineGame::Init<MineGame>(737960);
        game.Start();
    }
    /*int i;
    std::cin >> i;*/
    return 0;
}

//#ifdef _WIN32
//int APIENTRY WinMain(HINSTANCE, HINSTANCE, char*, int) { return entry(); }
//#endif
int main() { return entry(); }
