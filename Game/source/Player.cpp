#include "skya/Profiling.h"
#include <game/Player.h>

#include <game/main.h>
#include <game/Block.h>
#include <game/BlockType.h>

#define _USE_MATH_DEFINES
#include <math.h>

Player::Player() {}


void Player::OnStart() {
    this->GetGame().window.OnMouseButtonCallback += sk::DELEGATE(&Player::OnMouseButton, this);
    this->GetGame().window.OnKeyCallback += sk::DELEGATE(&Player::OnKeyEvent, this);
    this->GetGame().window.OnCursorPosCallback += sk::DELEGATE(&Player::OnCursorPos, this);
}

void Player::OnUpdate(double dt) {
    SKYA_PROFILE_FUNCTION();

    auto&& phys = *this->template Get<sk::CPhysics>();
    auto&& transform = *this->template Get<sk::CTransform>();
    auto&& pos = transform.GetPosition();

    transform.GetRotation() = glm::angleAxis(look.x, transform.WorldUp);
    transform.GetRotation() *= glm::angleAxis(-look.y, transform.Left());
    // Dont calculate physics if chunk isnt loaded
    if(!noclip_enabled) if(auto* chunk = world->GetLoadedChunk(world->ChunkPos(pos));
    !chunk || !chunk->is_generated()) return;

    sprint_control = 0.1f;
    float air_control = 0.05f;
    float current_speed;

    on_block = nullptr;
    if(!noclip_enabled) {
        glm::vec3 offset(0, -.5f, 0);
        sk::Cylinder plr(pos+offset, .5f, .5f);

        while((in_block = &world->GetLoadedBlock(glm::ivec3(glm::round(plr.Origin))).get_type())->CollisionEnabled)
            plr.Origin = (pos += glm::vec3(0, 1, 0))+offset; // move the player up until not inside a block

        auto size = glm::ivec3(glm::floor(glm::vec3(plr.Radius, plr.Radius+plr.HalfHeight, plr.Radius)*2.f));
        for(int x = -size.x; x <= size.x; ++x) for(int y = -size.y; y <= size.y; ++y) for(int z = -size.z; z <= size.z; ++z) {
            if((glm::abs(x) == size.x) + (glm::abs(y) == size.y) + (glm::abs(z) == size.z) >= 2) continue; // maybe
            auto bl = world->GetLoadedBlock(glm::ivec3(x, y, z) + glm::ivec3(glm::floor(plr.Origin)));
            auto&& bltype = bl.get_type();
            if(!bltype.CollisionEnabled) continue;
            sk::AABB blab(bl.world_position(), bl.world_position()+1);
            auto normal = glm::roundEven(plr.Normal((blab.Min + blab.Max)/2.0f));
            if(auto dist = sk::Distance(blab, plr); dist <= 0) {
                if(auto force = glm::abs(normal)*phys.GetVelocity(); glm::roundEven(glm::normalize(force)) == normal) {
                    phys.AddVelocity(-force*(glm::length2(force) <= 48.0f ? 1.0f : 1.0f + bltype.Bounce));
                    phys.AddAcceleration(-glm::abs(normal)*phys.GetAcceleration());
                }
                plr.Origin = (pos += normal*dist)+offset;
            }
            if(!on_block
            && sk::Intersection(blab, sk::Cylinder(plr.Origin - glm::vec3{ 0, plr.HalfHeight+.1f, 0 }, plr.Radius-.05f, .1f))) on_block = &bltype;
        }
        air_control = in_block->Density;

        grounded = on_block != nullptr;
        if(grounded) { // on ground
            auto weight = .25f;
            phys.Drag = glm::vec3(on_block->Drag*(1-weight) + in_block->Drag*weight);
            current_speed = base_speed*(on_block->SpeedMultiplier*(on_block->Drag+0.1f));
        }
        else { // not on ground
            phys.Drag = glm::vec3(in_block->Drag);
            current_speed = base_speed*(in_block->SpeedMultiplier*(in_block->Drag+0.1f))*(air_control+0.1f);
            sprint_control = sprint_control*(1-.06f) + air_control*.06f;
            
            if(&world->GetLoadedBlock(glm::ivec3(glm::round(plr.Origin + glm::vec3(0, .5, 0)))).get_type() != in_block
            && go_up) phys.AddVelocity({ 0, jump_force/10, 0 });
        }
        
        world->in_block = in_block;
    }
    else { // noclip is enabled
        grounded = false;
        current_speed = base_speed;
        sprint_control = 1.0f;
        air_control = 1.0f;
        phys.Drag = glm::vec3(0.3f);
    }
    auto strafespeed = current_speed;
    if(run) {
        current_speed *= sprint_multiplier;
        auto divider = (sprint_multiplier/2)*(1 - sprint_control);
        if(divider > 1.0f) strafespeed /= divider;
    }

    auto&& left = transform.Left();
    auto fwd_accel = glm::vec3(-left.z, 0, left.x)*(1-air_control) + transform.Forward()*air_control;
    if(go_up||go_down) fwd_accel = glm::vec3(-left.z, 0, left.x); // up, down movement overrides forward, back movement

    if(go_forward) phys.AddAcceleration(fwd_accel*current_speed);
    if(go_back) phys.AddAcceleration(-fwd_accel*current_speed);

    if(go_left) phys.AddAcceleration(left*strafespeed);
    if(go_right) phys.AddAcceleration(-left*strafespeed);
    if(go_up) phys.AddAcceleration(glm::vec3(0, 1, 0)*current_speed*air_control);
    if(go_down) phys.AddAcceleration(glm::vec3(0, -1, 0)*current_speed*air_control);

    phys.AddAcceleration({ 0, -(1 - air_control), 0 });
}

void Player::SetWorld(World* w) {
    world = w;
}

void Player::OnCursorPos(sk::CursorPosEventArgs e) {
    if(static_cast<MineGame&>(GetGame()).focused()) {
        auto lookspeed = .0075f;
        auto sprint_clamp = sprint_control/10;
        if(run) {
            look.x += std::clamp((float(e.X) - mouse_pos.x)*lookspeed, -sprint_clamp, sprint_clamp);
            look.y += std::clamp((float(e.Y) - mouse_pos.y)*lookspeed, -sprint_clamp, sprint_clamp);
        }
        else {
            look.x += (float(e.X) - mouse_pos.x)*lookspeed;
            look.y += (float(e.Y) - mouse_pos.y)*lookspeed;
        }
        auto max = float(M_PI_2) - .001f;
        look.y = std::clamp(look.y, -max, max);
    }
    mouse_pos.x = float(e.X);
    mouse_pos.y = float(e.Y);
}

void Player::OnKeyEvent(sk::KeyEventArgs e) {
    auto&& phys = *this->template Get<sk::CPhysics>();
    switch(e.action) {
        case(sk::Keyboard::PRESS): switch(e.key) {
            case(sk::Keyboard::SPACE):
            {
                if(grounded) {
                    phys.Drag = glm::vec3(in_block->Drag);
                    phys.AddVelocity({ 0, jump_force, 0 });
                }
                go_up = true;
            } break;
            case(sk::Keyboard::LEFT_CONTROL): go_down = true; break;
            case(sk::Keyboard::UP):
            case(sk::Keyboard::W): go_forward = true; break;
            case(sk::Keyboard::DOWN):
            case(sk::Keyboard::S): go_back = true; if(!noclip_enabled) run = false; break;
            case(sk::Keyboard::LEFT):
            case(sk::Keyboard::A): go_left = true; break;
            case(sk::Keyboard::RIGHT):
            case(sk::Keyboard::D): go_right = true; break;
            case(sk::Keyboard::LEFT_SHIFT): if(go_forward || noclip_enabled) run = true; break;

            case(sk::Keyboard::V): noclip_enabled = !noclip_enabled; break;
            default: break;
        } break;
        case(sk::Keyboard::RELEASE): switch(e.key) {
            case(sk::Keyboard::SPACE): go_up = false; break;
            case(sk::Keyboard::LEFT_CONTROL): go_down = false; break;
            case(sk::Keyboard::UP):
            case(sk::Keyboard::W): go_forward = false; if(!noclip_enabled) run = false; break;
            case(sk::Keyboard::DOWN):
            case(sk::Keyboard::S): go_back = false; break;
            case(sk::Keyboard::LEFT):
            case(sk::Keyboard::A): go_left = false; break;
            case(sk::Keyboard::RIGHT):
            case(sk::Keyboard::D): go_right = false; break;
            case(sk::Keyboard::LEFT_SHIFT): if(noclip_enabled) run = false; break;
            default: break;
        } break;
        default: break;
    }
}

void Player::OnMouseButton(sk::MouseButtonEventArgs e) {
    SKYA_PROFILE_FUNCTION();

    if(e.action != sk::Mouse::PRESS || e.button != sk::Mouse::LEFT) return;
    auto&& transform = *this->template Get<sk::CTransform>();
    auto origin = transform.GetPosition();
    auto dir = transform.Forward();

    Chunk* nearchunk = nullptr;
    glm::ivec3 nearest(-1);
    float nearestDist;
    for(auto* c : world->EnumerateChunks()) {
        if(!c) continue;
        auto& chunk = *c;
        auto ch_size = chunk.size();
        auto ch_worldpos = chunk.world_position();
        auto bounds = sk::AABB(ch_worldpos, ch_worldpos + ch_size);

        if(!sk::Intersection(sk::Ray(origin, dir), bounds)) continue;
        for(int x = 0; x < ch_size.x; ++x)
            for(int y = 0; y < ch_size.y; ++y)
                for(int z = 0; z < ch_size.z; ++z) {
                    auto* type = &chunk.GetBlockFast(x, y, z).get_type();
                    if(*type == *world->Air) continue;
                    if(type->Hitpoints == 0 && !type->CollisionEnabled) continue;
                    auto min = (glm::vec3)ch_worldpos + glm::vec3(x, y, z);
                    auto max = (glm::vec3)ch_worldpos + glm::vec3(x+1, y+1, z+1);
                    if(((min - origin)*glm::inverse(transform.GetRotation())).z <= 0) continue;
                    auto intersect = sk::Intersection(sk::Ray(origin, dir), sk::AABB(min, max));
                    if(!intersect) continue;
                    auto dist = sk::Distance2(min, origin);
                    if(!nearchunk || dist < nearestDist) {
                        nearchunk = &chunk;
                        nearest = glm::ivec3(x, y, z);
                        nearestDist = dist;
                    }
                }
    }
    if(nearchunk) {
        auto&& block = nearchunk->GetLoadedBlock(nearest.x, nearest.y, nearest.z);
        if(block.get_type().Hitpoints)
            block.set_type(world->Air);
    }
}



















