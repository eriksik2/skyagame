#include <game/Noise.h>

#include <skya/ResourceLoader.h>

#include <nlohmann/json.hpp>
#include <EASTL/string.h>
#include <EASTL/string_view.h>
#include <string>

#include <FastNoiseSIMD.h>

#include <time.h>
#include <assert.h>

#include <iostream>
#include <fstream>

#define STRENUM(enum, enumval) if(str == #enumval) return enum::enumval
Noise::NoiseType stoNoiseType(eastl::string_view str) {
    STRENUM(Noise::NoiseType, Value);
    STRENUM(Noise::NoiseType, ValueFractal);
    STRENUM(Noise::NoiseType, Perlin);
    STRENUM(Noise::NoiseType, PerlinFractal);
    STRENUM(Noise::NoiseType, Simplex);
    STRENUM(Noise::NoiseType, SimplexFractal);
    STRENUM(Noise::NoiseType, WhiteNoise);
    STRENUM(Noise::NoiseType, Cellular);
    STRENUM(Noise::NoiseType, Cubic);
    STRENUM(Noise::NoiseType, CubicFractal);
    return (Noise::NoiseType)0;
}
Noise::FractalType stoFractalType(eastl::string_view str) {
    STRENUM(Noise::FractalType, FBM);
    STRENUM(Noise::FractalType, Billow);
    STRENUM(Noise::FractalType, RigidMulti);
    return (Noise::FractalType)0;
}
Noise::PerturbType stoPerturbType(eastl::string_view str) {
    STRENUM(Noise::PerturbType, None);
    STRENUM(Noise::PerturbType, Gradient);
    STRENUM(Noise::PerturbType, GradientFractal);
    STRENUM(Noise::PerturbType, Normalise);
    STRENUM(Noise::PerturbType, Gradient_Normalise);
    STRENUM(Noise::PerturbType, GradientFractal_Normalise);
    return (Noise::PerturbType)0;
}
Noise::CellularReturnType stoCellularReturnType(eastl::string_view str) {
    STRENUM(Noise::CellularReturnType, CellValue);
    STRENUM(Noise::CellularReturnType, Distance);
    STRENUM(Noise::CellularReturnType, Distance2);
    STRENUM(Noise::CellularReturnType, Distance2Add);
    STRENUM(Noise::CellularReturnType, Distance2Sub);
    STRENUM(Noise::CellularReturnType, Distance2Mul);
    STRENUM(Noise::CellularReturnType, Distance2Div);
    STRENUM(Noise::CellularReturnType, NoiseLookup);
    STRENUM(Noise::CellularReturnType, Distance2Cave);
    return (Noise::CellularReturnType)0;
}
Noise::CellularDistanceFunction stoCellularDistanceFunction(eastl::string_view str) {
    STRENUM(Noise::CellularDistanceFunction, Euclidean);
    STRENUM(Noise::CellularDistanceFunction, Manhattan);
    STRENUM(Noise::CellularDistanceFunction, Natural);
    return (Noise::CellularDistanceFunction)0;
}
#undef STRENUM

Noise* NoiseFromFile(const char* path) {
    auto& noise = *(new Noise());
    int indice1 = 0, indice2 = 1;
    
    std::ifstream input(path);
    using json = nlohmann::json;

    json file1;
    input >> file1;

    if(!file1.is_object()){
        std::cout << "Error loading noise: " << path;
        delete &noise;
        return nullptr;
    }
    
    if(file1["seed"].is_number()) noise.SetSeed(file1["seed"]);
    if(file1["frequency"].is_number()) noise.SetFrequency(file1["frequency"]);
    if(file1["type"].is_string()) noise.SetNoiseType(stoNoiseType(file1["type"].get<std::string>().data()));
    
    if(auto& fractal = file1["fractal"]; !fractal.is_null()){
        if(fractal["octaves"].is_number()) noise.SetFractalOctaves(fractal["octaves"].get<int>());
        if(fractal["lacunarity"].is_number()) noise.SetFractalLacunarity(fractal["lacunarity"].get<float>());
        if(fractal["gain"].is_number()) noise.SetFractalGain(fractal["gain"].get<float>());
        if(fractal["type"].is_string()) noise.SetFractalType(stoFractalType(fractal["type"].get<std::string>().data()));
    }
    
    if(auto& perturb = file1["perturb"]; !perturb.is_null()){
        if(perturb["amplitude"].is_number()) noise.SetPerturbAmp(perturb["amplitude"].get<float>());
        if(perturb["frequency"].is_number()) noise.SetPerturbFrequency(perturb["frequency"].get<float>());
        if(perturb["normalize"].is_number()) noise.SetPerturbNormaliseLength(perturb["normalize"].get<float>());
        if(perturb["type"].is_string()) noise.SetPerturbType(stoPerturbType(perturb["type"].get<std::string>().data()));
        if(auto&& fractal = perturb["fractal"]; !fractal.is_null()){
            if(fractal["octaves"].is_number()) noise.SetPerturbFractalOctaves(fractal["octaves"].get<int>());
            if(fractal["lacunarity"].is_number()) noise.SetPerturbFractalLacunarity(fractal["lacunarity"].get<float>());
            if(fractal["gain"].is_number()) noise.SetPerturbFractalGain(fractal["gain"].get<float>());
        }
    }
    
    if(auto&& cellular = file1["cellular"]; !cellular.is_null()){
        if(cellular["return_type"].is_string()) noise.SetCellularReturnType(stoCellularReturnType(cellular["return_type"].get<std::string>().data()));
        if(cellular["distance_function"].is_string()) noise.SetCellularDistanceFunction(stoCellularDistanceFunction(cellular["distance_function"].get<std::string>().data()));
        if(cellular["lookup"].is_string()) noise.SetCellularNoiseLookupType(stoNoiseType(cellular["lookup"].get<std::string>().data()));
        if(cellular["indice1"].is_number()) indice1 = cellular["indice1"].get<int>();
        if(cellular["indice2"].is_number()) indice2 = cellular["indice2"].get<int>();
        if(cellular["lookup_frequency"].is_number()) noise.SetCellularNoiseLookupFrequency(cellular["lookup_frequency"].get<float>());
        if(cellular["jitter"].is_number()) noise.SetCellularJitter(cellular["jitter"].get<float>());
    }
    noise.SetCellularDistance2Indicies(indice1, indice2);
    return &noise;
}

Noise* ErrorNoise() {
    auto& noise = *(new Noise());
    noise.SetFrequency(0);
    return &noise;
}

template<> sk::Content<Noise>::DefaultMethod sk::Content<Noise>::Default = NoiseFromFile;
template<> sk::Content<Noise>::ErrorMethod sk::Content<Noise>::Error = ErrorNoise;

// Loading Functions ^
// NoiseSet          v

NoiseSet::~NoiseSet() {
    FastNoiseSIMD::FreeNoiseSet(m_data);
}

NoiseSet::NoiseSet(NoiseSet && move)
    : m_sizex(move.m_sizex)
    , m_sizey(move.m_sizey)
    , m_sizez(move.m_sizez)
    , m_data(move.m_data)
    , m_index(move.m_index) {
    move.m_data = nullptr;
}

NoiseSet & NoiseSet::operator=(NoiseSet && move) {
    if(m_data) FastNoiseSIMD::FreeNoiseSet(m_data);
    m_sizex = move.m_sizex;
    m_sizey = move.m_sizey;
    m_sizez = move.m_sizez;
    m_data = move.m_data;
    m_index = move.m_index;
    move.m_data = nullptr;
    return *this;
}

float NoiseSet::operator()(int x, int y, int z) const {
    assert(0 <= x && 0 <= y && 0 <= z);
    assert(m_sizex > x && m_sizey > y && m_sizez > z);
    return m_data[z + m_sizez*y + m_sizez*m_sizey*x];
}

float NoiseSet::next() {
    if(m_index == end())
        return *(m_index = m_data);
    return *(m_index++);
}

// NoiseSet          ^
// Noise             v

Noise::Noise(int seed) : noise(FastNoiseSIMD::NewFastNoiseSIMD(seed)) {}
Noise::Noise() : noise(FastNoiseSIMD::NewFastNoiseSIMD(int(time(0)))) {}

Noise::~Noise() {
    delete noise;
}

Noise::Noise(const Noise& from) : Noise() {
    *noise = *from.noise;
}

Noise::Noise(Noise&& from) {
    noise = from.noise;
    from.noise = nullptr;
}

Noise& Noise::operator=(const Noise& from) {
    if(noise == from.noise) return *this;
    *noise = *from.noise;
    return *this;
}

Noise& Noise::operator=(Noise&& from) {
    if(noise == from.noise) return *this;
    noise = from.noise;
    from.noise = nullptr;
    return *this;
}

int Noise::GetSeed() const {
    return noise->GetSeed();
}

void Noise::SetSeed(int seed) {
    noise->SetSeed(seed);
}

void Noise::SetFrequency(float frequency) {
    noise->SetFrequency(frequency);
}

void Noise::SetNoiseType(Noise::NoiseType noiseType) {
    noise->SetNoiseType((FastNoiseSIMD::NoiseType)noiseType);
}

void Noise::SetAxisScales(float xScale, float yScale, float zScale) {
    noise->SetAxisScales(xScale, yScale, zScale);
}

void Noise::SetFractalOctaves(int octaves) {
    noise->SetFractalOctaves(octaves);
}

void Noise::SetFractalLacunarity(float lacunarity) {
    noise->SetFractalLacunarity(lacunarity);
}

void Noise::SetFractalGain(float gain) {
    noise->SetFractalGain(gain);
}

void Noise::SetFractalType(Noise::FractalType fractalType) {
    noise->SetFractalType((FastNoiseSIMD::FractalType)fractalType);
}

void Noise::SetCellularReturnType(Noise::CellularReturnType cellularReturnType) {
    noise->SetCellularReturnType((FastNoiseSIMD::CellularReturnType) cellularReturnType);
}

void Noise::SetCellularDistanceFunction(Noise::CellularDistanceFunction cellularDistanceFunction) {
    noise->SetCellularDistanceFunction((FastNoiseSIMD::CellularDistanceFunction) cellularDistanceFunction);
}

void Noise::SetCellularNoiseLookupType(Noise::NoiseType cellularNoiseLookupType) {
    noise->SetCellularNoiseLookupType((FastNoiseSIMD::NoiseType) cellularNoiseLookupType);
}

void Noise::SetCellularNoiseLookupFrequency(float cellularNoiseLookupFrequency) {
    noise->SetCellularNoiseLookupFrequency(cellularNoiseLookupFrequency);
}

void Noise::SetCellularDistance2Indicies(int cellularDistanceIndex0, int cellularDistanceIndex1) {
    noise->SetCellularDistance2Indicies(cellularDistanceIndex0, cellularDistanceIndex1);
}

void Noise::SetCellularJitter(float cellularJitter) {
    noise->SetCellularJitter(cellularJitter);
}

void Noise::SetPerturbType(Noise::PerturbType perturbType) {
    noise->SetPerturbType((FastNoiseSIMD::PerturbType) perturbType);
}

void Noise::SetPerturbAmp(float perturbAmp) {
    noise->SetPerturbAmp(perturbAmp);
}

void Noise::SetPerturbFrequency(float perturbFrequency) {
    noise->SetPerturbFrequency(perturbFrequency);
}

void Noise::SetPerturbFractalOctaves(int perturbOctaves) {
    noise->SetPerturbFractalOctaves(perturbOctaves);
}

void Noise::SetPerturbFractalLacunarity(float perturbLacunarity) {
    noise->SetPerturbFractalLacunarity(perturbLacunarity);
}

void Noise::SetPerturbFractalGain(float perturbGain) {
    noise->SetPerturbFractalGain(perturbGain);
}

void Noise::SetPerturbNormaliseLength(float perturbNormaliseLength) {
    noise->SetPerturbNormaliseLength(perturbNormaliseLength);
}

NoiseSet Noise::GetNoiseSet(int xStart, int yStart, int zStart, int xSize, int ySize, int zSize, float scaleModifier) {
    float* data = noise->GetNoiseSet(xStart, yStart, zStart, xSize, ySize, zSize, scaleModifier);
    return NoiseSet(data, xSize, ySize, zSize);
}


