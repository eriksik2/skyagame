#include "skya/Profiling.h"
#include <game/Chunk.h>
#include <game/BlockType.h>
#include <game/World.h>
#include <game/main.h>

#include <GL/gl3w.h>

#include <FastNoiseSIMD.h>
#include <glm/gtc/matrix_transform.hpp>
#define _USE_MATH_DEFINES
#include <cmath>

Chunk::Chunk(World* w, glm::ivec3 pos) : m_world(*w), m_position(pos), m_size(m_world.GetChunkSize()) {
    m_blocks.resize(m_size.x*m_size.y*m_size.z);
}

World& Chunk::get_world() const {
    return m_world;
}

glm::ivec3 Chunk::world_position() const {
    return size()*m_position;
}
glm::ivec3 Chunk::chunk_position() const {
    return m_position;
}
glm::ivec3 Chunk::size() const {
    return m_world.GetChunkSize();
}

void Chunk::OnRender(sk::grafxbase::FramebufferHandle fb, bool render_transparent) {
    SKYA_PROFILE_FUNCTION();

    auto worldpos = glm::translate(glm::mat4(1), (glm::vec3)world_position());
    auto& game = static_cast<MineGame&>(m_world.GetGame());
    for(auto& p : Meshes)
    {
        auto&& Type = p.Type;
        if(Type.Transparent && !render_transparent) continue;
        if(!Type.Transparent && render_transparent) continue;
        auto& Mesh = p.Mesh;


        for(int i = 0; i < 6; ++i)
        {
            if(!Type.Texture[i]) continue;
            sk::grafxbase::texture_unit(Type.Texture[i], i);
        }

        for(int i = 0; i < sk::grafxbase::program_uniform_count(m_world.BlockShader); ++i){
            auto uniform = sk::grafxbase::program_uniform_info(m_world.BlockShader, i);
            
            if(uniform.name == "modelview") sk::grafxbase::set_uniform_float44(m_world.BlockShader, uniform.location, game.VPMatrix * worldpos);
        }
        sk::grafxbase::DrawCommandSettings set;
        if(!Type.Transparent && !Type.Hollow) set.culling = sk::grafxbase::FaceCullingMode_CCW;
        sk::grafxbase::draw_triangles(Mesh.VAO, m_world.BlockShader, fb, set);
    }
}

BlockInfo Chunk::GetLoadedBlock(int x, int y, int z) {
    auto Size = size();
    if(x < 0 || x >= Size.x || y < 0 || y >= Size.y || z < 0 || z >= Size.z)
        return m_world.GetLoadedBlock(glm::ivec3(x, y, z) + world_position());
    return GetBlockFast(x, y, z);
}
BlockInfo Chunk::GetBlock(int x, int y, int z) {
    auto Size = size();
    if(x < 0 || x >= Size.x || y < 0 || y >= Size.y || z < 0 || z >= Size.z)
        return m_world.GetBlock(glm::ivec3(x, y, z) + world_position(), current_generation_layer() - 1);
    return GetBlockFast(x, y, z);
}

int index(glm::ivec3 size, glm::ivec3 ind);

BlockInfo Chunk::GetBlockFast(int x, int y, int z) {
    auto&& block = m_blocks[index(m_size, {x, y, z})];
    if(!block.m_type) block.m_type = m_world.Air;
    return BlockInfo(this, &block, { x, y, z });
}

void Chunk::ReloadMesh() {
    SKYA_PROFILE_FUNCTION();

    auto Size = size();

    struct MeshParts {
        BlockType* Type = nullptr;
        eastl::vector<sk::MeshObject::Vertex> Verts;
        eastl::vector<int> Indices;
    };

    int max_id = 0;
    for(auto& bl : m_blocks)
        if(bl.m_type && bl.m_type->ID > max_id) max_id = bl.m_type->ID;
    max_id += 1;
    eastl::vector<MeshParts> meshTypes;
    meshTypes.resize(max_id);
    MeshParts* current;

    auto getVert = [&current](int x, int y, int z, int tu, int tv, glm::ivec3 normal) -> int {
        sk::MeshObject::Vertex vert((float)x, (float)y, (float)z);
        vert.UV = glm::vec2(tu, tv);
        vert.Normal = glm::vec3(normal);

        auto& verts = current->Verts;
        verts.push_back(vert);
        return verts.size() - 1;
    };

    for(int x = 0; x < Size.x; x++) for(int y = 0; y < Size.y; y++) for(int z = 0; z < Size.z; z++)
    {
        auto&& block = m_blocks[index(Size, {x, y, z})];
        auto* type = (BlockType*)block.m_type;
        if(!type) continue;
        if(type->HasNoFace()) continue;
        if(*type == *m_world.Air) continue;

        int type_id = type->ID;

        current = meshTypes.begin() + type_id;
        if(!current->Type)
        {
            current->Type = type;
            current->Indices.reserve(100);
            current->Verts.reserve(100);
        }

        auto& indices = current->Indices;
        for(int i = 0; i < 6; ++i)
        {
            if(!type->HasFace(i)) continue;
            int x1 = x, x2 = x+1,
                y1 = y, y2 = y+1,
                z1 = z, z2 = z+1;
            glm::ivec3 norm;
            switch(i)
            {
                case Block::TOP:
                    y1 = y2;
                    norm = glm::ivec3(0, 1, 0);
                    break;
                case Block::BOTTOM:
                    y2 = y1;
                    norm = glm::ivec3(0, -1, 0);
                    break;
                case Block::LEFT:
                    x1 = x2;
                    norm = glm::ivec3(1, 0, 0);
                    break;
                case Block::RIGHT:
                    x2 = x1;
                    norm = glm::ivec3(-1, 0, 0);
                    break;
                case Block::FRONT:
                    z1 = z2;
                    norm = glm::ivec3(0, 0, 1);
                    break;
                case Block::BACK:
                    z2 = z1;
                    norm = glm::ivec3(0, 0, -1);
            }

            auto other_loc = glm::ivec3(x, y, z) + norm;
            auto block2 = GetLoadedBlock(other_loc.x, other_loc.y, other_loc.z);
            if(!block2.valid()) continue;
            if(block2.get_type() == *type) continue;
            if(block2.get_type().BlocksFace((i%2==0)?i+1:i-1)) continue;

            int x1a = x1, x2a = x2;
            if(i<=2) std::swap(x1a, x2a);

            int indice[4];
            indice[0] = getVert(x2, y1, z1, 0, 0, norm);
            indice[1] = getVert(x2a, y2, z1, 0, 1, norm);
            indice[2] = getVert(x1, y2, z2, 1, 1, norm);
            indice[3] = getVert(x1a, y1, z2, 1, 0, norm);
            if(i%2!=0) std::swap(indice[1], indice[3]);
            indices.insert(indices.end(), {indice[0], indice[1], indice[2], indice[2], indice[3], indice[0]});
        }
    }

    // should get once and store in World class
    int pos_attr = -1, normal_attr = -1, uv_attr = -1;
    for(int i = 0; i < sk::grafxbase::program_attribute_count(m_world.BlockShader); ++i){
        auto attr = sk::grafxbase::program_attribute_info(m_world.BlockShader, i);
        
        if(attr.name == "vPosition") pos_attr = attr.location;
        else if(attr.name == "vNormal") normal_attr = attr.location;
        else if(attr.name == "texcoord") uv_attr = attr.location;
    }
    Meshes.clear();
    for(auto& local : meshTypes)
    {
        if(!local.Type || local.Indices.size() <= 0) continue;
        Meshes.emplace_back(*local.Type, pos_attr, normal_attr, uv_attr);
        auto& real = Meshes.back();
        real.Mesh.RewriteData(local.Verts, local.Indices);
    }
    b_NeedUpdate = false;
}

bool Chunk::NeedsReload() const {
    return b_NeedUpdate;
}

Chunk::ChunkMesh::ChunkMesh(const BlockType& type, int p, int n, int uv) : Type(type) {
    if(p!=-1)
    {
        sk::grafxbase::enable_vertex_attribute(Mesh.VAO, p);
        Mesh.PipePositionToAttribute(p);
    }
    if(n!=-1)
    {
        sk::grafxbase::enable_vertex_attribute(Mesh.VAO, n);
        Mesh.PipeNormalToAttribute(n);
    }
    if(uv!=-1)
    {
        sk::grafxbase::enable_vertex_attribute(Mesh.VAO, uv);
        Mesh.PipeUVToAttribute(uv);
    }
}

