#include <game/WorldGen/WorldGen.h>
#include <game/WorldGen/WorldGenLayer.h>

#include <game/Chunk.h>

void WorldGen::register_layer(sk::handle<WorldGenLayer> ptr) {
    m_layers.push_back(ptr);
}

void WorldGen::unregister_layer(sk::handle<WorldGenLayer> ptr) {
    m_layers.erase_first(ptr);
}

void WorldGen::clear() {
    m_layers.clear();
}

void WorldGen::generate_next_layer(Chunk& chunk) const {
    if(chunk.m_generation_state == WorldGenData::GenState::DONE)
        return;

    chunk.m_generation_state = WorldGenData::GenState::IN_PROGRESS;
    auto& layer = *m_layers[chunk.m_next_layer_to_generate++];

    chunk.m_generation_mutex.lock();
    layer.apply(chunk);
    chunk.m_generation_mutex.unlock();

    if(chunk.m_next_layer_to_generate >= m_layers.size())
        chunk.m_generation_state = WorldGenData::GenState::DONE;
}

void WorldGen::generate_to_layer(Chunk& chunk, int layer) const {
    while(chunk.m_next_layer_to_generate <= layer)
        generate_next_layer(chunk);
}

void WorldGen::generate(Chunk& chunk) const {
    generate_to_layer(chunk, m_layers.size() - 1);
}