
execute_process(COMMAND git submodule update --init Cpp-Taskflow WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}")


set(TF_BUILD_EXAMPLES OFF)
set(TF_BUILD_TESTS OFF)
set(TF_BUILD_BENCHMARKS OFF)
add_subdirectory("Cpp-Taskflow")