#version 330

uniform mat4 view_mtx;
uniform mat4 proj_mtx;
uniform float pA;
uniform float pB;

uniform sampler2D depth_map;
uniform sampler2D diffuse_map;
uniform sampler2D normal_map;

uniform vec3 fog_color;
uniform float fog_dist;

in vec2 f_uv;

out vec4 output_color;

float lin_depth(vec2 vTexCoord) {
    return pB/(texture(depth_map, vTexCoord).x - pA);
}

void main() {
    float d = lin_depth(f_uv)*(1 - fog_dist);
    d = max(min(d, 1.0f), 0.0f);
    output_color = texture(diffuse_map, f_uv);
    output_color.xyz = (1 - d)*output_color.xyz + d*fog_color;
}
