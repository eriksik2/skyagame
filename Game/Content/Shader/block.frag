#version 410


uniform sampler2D textureTop;
uniform sampler2D textureBot;
uniform sampler2D textureLeft;
uniform sampler2D textureRight;
uniform sampler2D textureFront;
uniform sampler2D textureBack;

uniform sampler2D maintexture;

in float f_brightness;
in vec3 f_normal;
in vec2 f_texcoord;

layout(location = 0) out vec4 outputColor;
layout(location = 1) out vec3 outputNormal;


void
main()
{
    vec4 tex;
    
    vec3 abnorm = abs(f_normal);
    float maxn = max(max(abnorm.x, abnorm.y), abnorm.z);
    int maxid = 0;
    for(int i = 0; i < 3; ++i){
        if(abnorm[i] == maxn) maxid = i;
    }
    
    if(f_normal[maxid] < 0) maxid = maxid*2 + 1;
    else maxid *= 2;
    
    switch(maxid) {
        case 0: tex = texture(textureLeft, f_texcoord); break;
        case 1: tex = texture(textureRight, f_texcoord); break;
            
        case 2: tex = texture(textureTop, f_texcoord); break;
        case 3: tex = texture(textureBot, f_texcoord); break;
            
        case 4: tex = texture(textureFront, f_texcoord); break;
        case 5: tex = texture(textureBack, f_texcoord); break;
        default: break;
    }

    tex.xyz *= f_brightness;

    if(tex.a <= 0.1)
        discard;
    outputColor = tex;
    outputNormal = f_normal;
}
