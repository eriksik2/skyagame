#version 410


uniform mat4 modelview;

in vec3 vPosition;
in vec3 vNormal;
in vec2 texcoord;

out float f_brightness;
out vec3 f_normal;
out vec2 f_texcoord;

void
main()
{
    float brightnessRange = 6;
    float brightnessLevel = 5;
    f_brightness = vNormal.y/brightnessRange + brightnessLevel/brightnessRange;
    gl_Position = modelview * vec4(vPosition, 1.0);
    f_texcoord = vec2(texcoord.x, 1.0 - texcoord.y);
    f_normal = vNormal;
}
