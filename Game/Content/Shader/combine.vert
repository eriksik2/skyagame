#version 330

layout(location = 0) in vec3 pos;

out vec2 f_uv;

void main() {
    gl_Position = vec4(pos, 1.0);
    f_uv = pos.xy*.5+.5;
}