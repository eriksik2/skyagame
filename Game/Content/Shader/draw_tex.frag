#version 330

uniform sampler2D texture_map;
in vec2 f_uv;

out vec4 output_color;

void main() {
    output_color = texture(texture_map, f_uv);
}
