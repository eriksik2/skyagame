#version 330

uniform vec2 windowSize;
uniform vec2 textureSize;
uniform vec2 texturePos; // [0,windowSize]

layout(location = 0) in vec3 pos; // [-1, 1]
out vec2 f_uv;

void main() {
    vec2 cpos = pos.xy*.5+.5; // [0, 1]
    f_uv = cpos;
    cpos = cpos*textureSize/windowSize;
    gl_Position = vec4((texturePos/windowSize + cpos)*2 - 1, 0.0, 1.0); 
}
